  function resetPass()
  {
    var pass        = $("#reset").find("input[id='password']").val();
    var pass_ulang  = $("#reset").find("input[id='password_ulang']").val();
    var password    = calcMD5($("#reset").find("input[name='password']").val());
    
    if(pass !== pass_ulang && pass !== "")
    {
      toastr.error('Password tidak sama.', 'Error Alert', {timeOut: 5000});
    }
    else {
      var form_data = new FormData();
      form_data.append('password', password);
      $.ajax({
        dataType: 'json',
        type:'POST',
        url: 'pengaturan-user/ubah-pass',
        cache: false,
        contentType: false,
        processData: false,
        data:form_data,
        success : function (data, status)
        {
          if(data.status != 'error')
          {
            $("#reset").find("input[name='password']").val('');
            $("#reset").find("input[id='password_ulang']").val('');
            toastr.success(data.msg, 'Success Alert', {timeOut: 5000});
            toastr.success('Item Created Successfully.', 'Success Alert', {timeOut: 5000});
          }
          else{
            toastr.error(data.msg, 'Error Alert', {timeOut: 5000});
          }
        }
      })
    }
  }