$(document).ready(function(){
  manageData();
  function manageData() {
    getBerita();
    getBerita1();
    getNganjukSlide1();
    getNganjukSlide2();
    getPengumuman();
    getAgenda();
    getgaleri1();
    getgaleri2();
    getgaleri3();
  }
});

function getNganjukSlide1()
{
    $.ajax({
    type:'POST',
    dataType: 'json',
    url: 'home/getnganjukkab',
    data:{
      start:0
    },
    success: function ( data) {
      console.log(data);
      for(var i = 0; i<=3; i++)
      {
        $judul = data.data[i].judul.replace(/([\/\,\!\\\^\$\{\}\[\]\(\)\.\*\+\?\|\<\>\-\&])/g, ' ');
        $beautyurl =$judul.replace(/\s+/g,'-'); 
        $('.nganjuk-slide1').html($('.nganjuk-slide1').html()+"<div class='row gbr-berita' style='margin-bottom:5%'><div class='col-sm-3 float-left' style='width:150px; height:100px;'><img class='img-fluid' src='"+data.data[i].imgpath+"' alt='Sample image' style='width:100%; height:100%; box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 80px 0 rgba(0, 0, 0, 0.19);'></div><div class='col-sm-9 judul-berita'><h6><a href='https://www.nganjukkab.go.id/beranda/berita/detail-berita/"+data.data[i].idberita+"/"+$beautyurl+"' target='_blank'><h4 class='font-weight-bold dark-grey-text'>"+data.data[i].judul.substr(0,45)+"...</a></h6><i class='fa fa-calendar' style='color: #f8c300;'></i>&nbsp;"+data.data[i].tanggal+" |&nbsp; <i class='fa fa-user' style='color: #f8c300;'></i>&nbsp;"+data.data[i].nama+"-&nbspnganjukkab | <a href='https://www.nganjukkab.go.id/beranda/berita/detail-berita/"+data.data[i].idberita+"/"+$beautyurl+"' style='text-decoration:none;' target=_blank> <i class='fa fa-newspaper-o' style='color:#f8c300;'></i>&nbsp;Detail Berita</a></div> </div><hr>");
      }
    },
    error: function ( data ) {
      console.log('error');
    }
  });
}

function getNganjukSlide2()
{
    $.ajax({
    type:'POST',
    dataType: 'json',
    url: 'home/getnganjukkab',
    data:{
      start:0
    },
    success: function ( data) {
      for(var i = 4; i<data.data.length; i++)
      {
        $judul = data.data[i].judul.replace(/([\/\,\!\\\^\$\{\}\[\]\(\)\.\*\+\?\|\<\>\-\&])/g, ' ');
        $beautyurl =$judul.replace(/\s+/g,'-'); 
        $('.nganjuk-slide2').html($('.nganjuk-slide2').html()+"<div class='row gbr-berita' style='margin-bottom:5%'><div class='col-sm-3 float-left' style='width:150px; height:100px;'><img class='img-fluid' src='"+data.data[i].imgpath+"' alt='Sample image' style='width:100%; height:100%; box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 80px 0 rgba(0, 0, 0, 0.19);'></div><div class='col-sm-9 judul-berita'><h6><a href='https://www.nganjukkab.go.id/beranda/berita/detail-berita/"+data.data[i].idberita+"/"+$beautyurl+"' target='_blank'><h4 class='font-weight-bold dark-grey-text'>"+data.data[i].judul.substr(0,45)+"...</a></h6><i class='fa fa-calendar' style='color: #f8c300;'></i>&nbsp;"+data.data[i].tanggal+" |&nbsp; <i class='fa fa-user' style='color: #f8c300;'></i>&nbsp;"+data.data[i].nama+"-&nbspnganjukkab | <a href='https://www.nganjukkab.go.id/beranda/berita/detail-berita/"+data.data[i].idberita+"/"+$beautyurl+"' style='text-decoration:none;' target=_blank> <i class='fa fa-newspaper-o' style='color:#f8c300;'></i>&nbsp;Detail Berita</a></div> </div><hr>");
      }
    },
    error: function ( data ) {
      console.log('error');
    }
  });
}

function getAgenda()
{
  $.ajax({
        dataType: 'json',
        type:'POST',
        url: 'agenda/semua-agenda',
    }).done(function(data){
        var rows = '';
        if(data.data.length==0)
        {
          rows = rows + '<tr>';
          rows = rows + '<td colspan=2>Tidak Ada Agenda Kegiatan</td>';
          rows = rows + '<tr>';
        }
        else
        {
          for(var i=0;i<data.data.length;i++)
          {
            rows = rows + '<tr>';
            rows = rows + '<td>'+data.data[i].tanggal+'</td>';
            rows = rows + '<td>'+data.data[i].judul.substr(0, 50)+'</td>';
            rows = rows + '  <td><button type="button" class="btn btn-qibul btn-sm" onclick="detailAgenda('+data.data[i].id+')">Detail</button></td></tr>';
          }
        }
        $("#isiagenda").html(rows);
    });
}

function detailAgenda($id)
{
  $.ajax({
        dataType: 'json',
        type:'POST',
        url: 'agenda/semua-agenda/'+$id,
    }).done(function(data){
      $("#exampleModal #judul").text(data.data[0].judul);
      $("#exampleModal #tanggal").text(data.data[0].tanggal);
      $("#exampleModal #bagian").text("Bagian Terkait : "+data.data[0].namabagian);
      $("#exampleModal #download-file").attr("href",data.data[0].filepath);
        $('#exampleModal').modal('show');
    });
}

function getPengumuman()
{
  $.ajax({
        dataType: 'json',
        type:'POST',
        url: 'pengumuman/semua-pengumuman',
    }).done(function(data){
        var rows = '';
        if(data.data.length==0)
        {
          rows = rows + '<tr>';
          rows = rows + '<td colspan=2>Tidak Ada Agenda Kegiatan</td>';
          rows = rows + '<tr>';
        }
        else
        {
          for(var i=0;i<data.data.length;i++)
          {
            rows = rows + '<tr>';
            rows = rows + '<td>'+data.data[i].created_at+'</td>';
            rows = rows + '<td>'+data.data[i].judul.substr(0, 50)+'</td>';
            rows = rows + '<td><button type="button" class="btn btn-qibul btn-sm" onclick="detailPengumuman('+data.data[i].id+')">Detail</button></td></tr>';
          }
        }
        $("#isipengumuman").html(rows);
    });
}

function detailPengumuman($id)
{
  $.ajax({
        dataType: 'json',
        type:'POST',
        url: 'pengumuman/semua-pengumuman/'+$id,
    }).done(function(data){
      $("#PengumumanModal #judul").text(data.data[0].judul);
      $("#PengumumanModal #download-file").attr("href",data.data[0].lampiran);
      $('#PengumumanModal').modal('show');
    });
}

function getBerita()
{
  var pathArray = window.location.pathname.split('/');
  var slug = pathArray[2];
  $.ajax({
        dataType: 'json',
        type:'POST',
        url: 'berita',
    }).done(function(data){
        if(data.data.length == 0)
        {
           $('.berita-dinsos').html($('.berita-dinsos').html()+"<h4>Belum Ada Berita<h4>");
        }
        else
        {
          for(var i=0; i<data.data.length; i++)
          {
            $('.berita-dinsos').html($('.berita-dinsos').html()+"<div class='row gbr-berita'> <div class='col-md-3 float-left' style='width:100%; height:80%; '><img class='img-fluid' src='"+data.data[i].imgpath+"' alt='Sample image'  style='width:100%; height:100%; box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 80px 0 rgba(0, 0, 0, 0.19);'></div><div class='col-md-9 judul-berita-dinsos'><h4>"+data.data[i].judul+"</h4> <i class='fa fa-calendar' style='color: #f8c300;'></i>&nbsp;"+data.data[i].tanggal+" |&nbsp;<i class='fa fa-user' style='color: #f8c300;'></i>&nbsp;"+data.data[i].nama+" | <a href='"+slug+"/berita/detail-berita/"+data.data[i].id+"' style='text-decoration:none;' ><i class='fa fa-newspaper-o' style='color:#f8c300;'></i>&nbsp;Detail Berita</a></div></div><hr> ");
          }
        }
    });
}

function getBerita1()
{
  var pathArray = window.location.pathname.split('/');
  var slug = pathArray[2];
  $.ajax({
        dataType: 'json',
        type:'POST',
        url: 'berita1',
    }).done(function(data){
        if(data.data.length == 0)
        {
           $('.berita-dinsos1').html($('.berita-dinsos1').html()+"<h4>Belum Ada Berita<h4>");
        }
        else
        {
          for(var i=0; i<data.data.length; i++)
          {
            $('.berita-dinsos1').html($('.berita-dinsos1').html()+"<div class='item' style='margin-left: 0px; margin-bottom: 25px;'><img src='"+data.data[i].imgpath+"'><div class='carousel-caption'><h4><a href='#' class='berita2' style='color: #f8c300 ;  font-family: Open Sans, cursive;'><b>"+data.data[i].judul+"</b></a></h4><div class='icon1'><i class='fa fa-calendar' style='color: #f8c300;'></i>&nbsp;"+data.data[i].tanggal+" |&nbsp;<i class='fa fa-user' style='color: #f8c300;'></i>&nbsp;"+data.data[i].nama+" | <a href='"+slug+"/berita/detail-berita/"+data.data[i].id+"' style='text-decoration:none; color:#fff;' ><i class='fa fa-newspaper-o' style='color:#f8c300;'></i>&nbsp;Detail Berita</a></div</div></div>");
          }
        }
    });
}

function getgaleri1()
{
  $.ajax({
        dataType: 'json',
        type:'POST',
        url: 'galeri',
        data:{
          start:0
        },
    }).done(function(data){
      console.log("galeri1"+data.data.length);
      if(data.data.length == 3)
      {
        for(var i = 0; i<3; i++)
        {
          
          $('.galeri1').html($('.galeri1').html()+" <div class='col-md-4'><div class='card item'><a  onclick=preview("+data.data[i].idDetail+")><img class='card-img-top' src='"+data.data[i].imgpath+"''> ini contoh produk</a><button type='button' class='btn btn-primary'>Primary</button></div></div> "); 
        }
      }
      else if(data.data.length == 2)
      {
        for(var i = 0; i<2; i++)
        {
         
          $('.galeri1').html($('.galeri1').html()+" <div class='col-md-4'><div class='card item'><a onclick=preview("+data.data[i].idDetail+")><img class='card-img-top' src='"+data.data[i].imgpath+"''></a></div></div> "); 
        }
        $('.galeri1').html($('.galeri1').html()+" <div class='col-md-4'><div class='card item'><img class='card-img-top' src='"+ BASE_URL +"upload_file/1.jpg'></div></div> ");
      }
      else if(data.data.length == 1)
      {
        for(var i = 0; i<1; i++)
        {
         
          $('.galeri1').html($('.galeri1').html()+" <div class='col-md-4'><div class='card item'><a onclick=preview("+data.data[i].idDetail+")><img class='card-img-top' src='"+data.data[i].imgpath+"''></a></div></div> "); 
        }
        $('.galeri1').html($('.galeri1').html()+" <div class='col-md-4'><div class='card item'><img class='card-img-top' src='"+ BASE_URL +"upload_file/1.jpg'></div></div> ");
        $('.galeri1').html($('.galeri1').html()+" <div class='col-md-4'><div class='card item'><img class='card-img-top' src='"+ BASE_URL +"upload_file/1.jpg'></div></div> ");
      }
      else
      {
        for(var i = 0; i<3; i++)
        {
         $('.galeri1').html($('.galeri1').html()+" <div class='col-md-4'><div class='card item'><img class='card-img-top' src='"+ BASE_URL +"upload_file/1.jpg'></div></div> ");
        }
      } 

    });
}

function getgaleri2()
{
  $.ajax({
        dataType: 'json',
        type:'POST',
        url: 'galeri',
        data:{
          start:3
        },
    }).done(function(data){
      console.log("galeri2"+data.data.length);
      if(data.data.length == 3)
      {
        for(var i = 0; i<3; i++)
        {
          
          $('.galeri2').html($('.galeri2').html()+" <div class='col-md-4'><div class='card item'><a onclick=preview("+data.data[i].idDetail+")><img class='card-img-top' src='"+data.data[i].imgpath+"''></a></div></div> "); 
        }
      }
      else if(data.data.length == 2)
      {
        for(var i = 0; i<2; i++)
        {
         
          $('.galeri2').html($('.galeri2').html()+" <div class='col-md-4'><div class='card item'><a onclick=preview("+data.data[i].idDetail+")><img class='card-img-top' src='"+data.data[i].imgpath+"''></a></div></div> "); 
        }
        $('.galeri2').html($('.galeri2').html()+" <div class='col-md-4'><div class='card item'><img class='card-img-top' src='"+ BASE_URL +"upload_file/1.jpg'></div></div> ");
      }
      else if(data.data.length == 1)
      {
        for(var i = 0; i<1; i++)
        {
         
          $('.galeri2').html($('.galeri2').html()+" <div class='col-md-4'><div class='card item'><a onclick=preview("+data.data[i].idDetail+")><img class='card-img-top' src='"+data.data[i].imgpath+"''></a></div></div> "); 
        }
        $('.galeri2').html($('.galeri2').html()+" <div class='col-md-4'><div class='card item'><img class='card-img-top' src='"+ BASE_URL +"upload_file/1.jpg'></div></div> ");
        $('.galeri2').html($('.galeri2').html()+" <div class='col-md-4'><div class='card item'><img class='card-img-top' src='"+ BASE_URL +"upload_file/1.jpg'></div></div> ");
      }
      else
      {
        for(var i = 0; i<3; i++)
        {
         $('.galeri2').html($('.galeri2').html()+" <div class='col-md-4'><div class='card item'><img class='card-img-top' src='"+ BASE_URL +"upload_file/1.jpg'></div></div> ");
        }
      } 
    });
}

function getgaleri3()
{
  $.ajax({
        dataType: 'json',
        type:'POST',
        url: 'galeri',
        data:{
          start:6
        },
    }).done(function(data){
      console.log("galeri3"+data.data.length);
      if(data.data.length == 3)
      {
        for(var i = 0; i<3; i++)
        {
          
          $('.galeri3').html($('.galeri3').html()+" <div class='col-md-4'><div class='card item'><a onclick=preview("+data.data[i].idDetail+")><img class='card-img-top' src='"+data.data[i].imgpath+"''></a></div></div> "); 
        }
      }
      else if(data.data.length == 2)
      {
        for(var i = 0; i<2; i++)
        {
         
          $('.galeri3').html($('.galeri3').html()+" <div class='col-md-4'><div class='card item'><a onclick=preview("+data.data[i].idDetail+")><img class='card-img-top' src='"+data.data[i].imgpath+"''></a></div></div> "); 
        }
        $('.galeri3').html($('.galeri3').html()+" <div class='col-md-4'><div class='card item'><img class='card-img-top' src='"+ BASE_URL +"upload_file/1.jpg'></div></div> ");
      }
      else if(data.data.length == 1)
      {
        for(var i = 0; i<1; i++)
        {
         
          $('.galeri3').html($('.galeri3').html()+" <div class='col-md-4'><div class='card item'><a onclick=preview("+data.data[i].idDetail+")><img class='card-img-top' src='"+data.data[i].imgpath+"''></a></div></div> "); 
        }
        $('.galeri3').html($('.galeri3').html()+" <div class='col-md-4'><div class='card item'><img class='card-img-top' src='"+ BASE_URL +"upload_file/1.jpg'></div></div> ");
        $('.galeri3').html($('.galeri3').html()+" <div class='col-md-4'><div class='card item'><img class='card-img-top' src='"+ BASE_URL +"upload_file/1.jpg'></div></div> ");
      }
      else
      {
        for(var i = 0; i<3; i++)
        {
         $('.galeri3').html($('.galeri3').html()+" <div class='col-md-4'><div class='card item'><img class='card-img-top' src='"+ BASE_URL +"upload_file/1.jpg'></div></div> ");
        }
      } 
    });
}

function preview(id)
{
  $.ajax({
    dataType: 'json',
    url: 'visitor/galeri/detail-galeri/'+id,
    success: function (data) {
      var $index = 0;
      updateGallery(0,data.data)
      $('#show-next-image').click(function(){
        if($index < data.data.length - 1) $index++;
        updateGallery($index,data.data);
      });
      $('#show-previous-image').click(function(){
        if($index > 0) $index--;
        updateGallery($index,data.data);
      });
      $('#exampleModal1').modal('show');
    },
    error: function ( data ) {
      console.log('error');
    }
  });
}

function updateGallery(selector, data) {
    var $sel = selector;
    $('#image-gallery-image').attr('src', data[$sel].imgpath);
    disableButtons(selector,data.length);
}

function disableButtons(counter_current,counter_max){
  $('#show-previous-image, #show-next-image').removeAttr("disabled");
  if(counter_current == counter_max-1){
      $('#show-next-image').attr("disabled", true);
  } else if (counter_current == 0){
      $('#show-previous-image').attr("disabled", true);
  }
}