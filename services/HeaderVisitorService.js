$(document).ready(function(){
  manageData();
  function manageData() {
   getHeader();
  }
});

function getHeader()
{
    $.ajax({
    type:'POST',
    dataType: 'json',
    url: 'header-image',
    data:{
      start:0
    },
    success: function ( data) {
      if(data.data.length == 0)
      {
      	$('.poster-indicators').html($('.poster-indicators').html()+"<li data-target='#carousel-example-2' data-slide-to='0' class='active'></li>");
      	$('.poster-inner').html($('.poster-inner').html()+" <div class='carousel-item active'><div class='view'><img class='d-block w-100' style='width: 100%;' src='upload_file/header/caro.jpg' alt='Italian Trulli'><div class='mask rgba-black-light'></div></div></div> ");
      }
      else
      {
      	for(var i = 0; i<data.data.length; i++)
      	{
        	if(i==0)
        	{
        		$('.poster-indicators').html($('.poster-indicators').html()+"<li data-target='#carousel-example-2' data-slide-to='"+i+"' class='active'></li>");
        		$('.poster-inner').html($('.poster-inner').html()+" <div class='carousel-item active'><div class='view'><img class='d-block w-100' style='width: 100%;' src='"+data.data[i].imgpath+"' alt='Italian Trulli'><div class='mask rgba-black-light'></div></div></div> ");
        	}
        	else
        	{
        		$('.poster-indicators').html($('.poster-indicators').html()+"<li data-target='#carousel-example-2' data-slide-to='"+i+"'></li>");
        		$('.poster-inner').html($('.poster-inner').html()+" <div class='carousel-item'><div class='view'><img class='d-block w-100' style='width: 100%;' src='"+data.data[i].imgpath+"' alt='Italian Trulli'><div class='mask rgba-black-light'></div></div></div> ");
        	}
      	}
      }
    
    },
    error: function ( data ) {
      console.log('error');
    }
  });
}