$(document).ready(function(){
  manageData();
  function manageData() {
    $.ajax({
      dataType: 'json',
      url: 'admin/dashboard',
      success: function ( data) {
        console.info(data);
        if(data.berita.length==0)
          $("#berita").text(0);
        else
          $("#berita").text(data.berita.length);
        
        if(data.agenda.length==0)
          $("#agenda").text(0);
        else
          $("#agenda").text(data.agenda.length);

         if(data.pengumuman.length==0)
          $("#pengumuman").text(0);
        else
          $("#pengumuman").text(data.pengumuman.length);

        if(data.galeri.length==0)
          $("#galeri").text(0);
        else
          $("#galeri").text(data.galeri.length);
      },
      error: function ( data ) {
        console.log('error');
      }
    });
  }
});