var table;

$(document).ready(function(){
manageData();
  function manageData() {
    table = $('#example2').DataTable({
        "processing": true,
        'serverSide': true,
        "paging": true,
        "lengthChange": false,
        "ordering": false,
        'ajax': {
          'type': 'POST',
          'url': url,
          dataSrc: function(data)
          {
            var return_data = new Array();
              for(var i=0;i< data.data.length; i++){
              return_data.push({
                'no'        : data.data[i].no,
                'judul'     : data.data[i].judul,
                'isi'       : data.data[i].isi.substr(0, 100),
                'action'    : data.data[i].action,
              })
            }
            return return_data;
          }
        },
           "columnDefs": [
        { 
            "targets": [ 0 ], //first column / numbering column
            "orderable": false, //set not orderable
        },
        ],
        "columns"    : [
          {'data': 'no'},
          {'data': 'judul'},
          {'data': 'isi'},
          {'data': 'action'}
        ]
    });
  }
});

function reload_table()
{
    table.ajax.reload(null,false); //reload datatable ajax 
}

function update(id)
{
   $.ajax({
    dataType: 'json',
    url: 'profil-sejarah/ambil-data-by-id/'+id,
    success: function (data) {
      CKEDITOR.instances.editor1.setData(data.data[0].isi);
      $("#update-modal").find("input[name='id']").val(data.data[0].id);
      $("#update-modal").find("form").attr("action",'profil-sejarah/edit-data/'+id);
      $('#update-modal').modal('show');
    },
    error: function ( data ) {
      console.log('error');
    }
  });
}

function update_data()
{
    var form_action = $("#update-modal").find("form").attr("action");
    var id          = $("#update-modal").find("input[name='id']").val();
    var isi         = CKEDITOR.instances.editor1.getData();

    var form_data = new FormData();
    form_data.append('isi', isi);
    form_data.append('id', id);
    $.ajax({
        dataType: 'json',
        type:'POST',
        url: form_action,
        cache: false,
        contentType: false,
        processData: false,
        data:form_data,
        success : function (data, status)
        {
          if(data.status != 'error')
          {
            $(".modal").modal('hide');
            reload_table();
            $("#update-modal").find("input[name='id']").val('');
            toastr.success(data.msg, 'Success Alert', {timeOut: 5000});
          }
          else{
            toastr.error(data.msg, 'Error Alert', {timeOut: 5000});
          }
        }
    })
}


function detail(id){
  $.ajax({
    dataType: 'json',
    url: 'profil-sejarah/ambil-data-by-id/'+id,
    success: function ( data) {
      $("#title h4").text(data.data[0].judul);
      $("#uisi").html(''+data.data[0].isi+'');
      $('#detail-modal').modal('show');
    },
    error: function ( data ) {
      console.log('error');
    }
  });
}

