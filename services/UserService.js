var table;

$(document).ready(function(){
manageData();
  function manageData() {
    table = $('#example2').DataTable({
        "processing": true,
        'serverSide': true,
        "paging": true,
        "lengthChange": false,
        "ordering": false,
        'ajax': {
          'type': 'POST',
          'url': url,
          dataSrc: function(data)
          {
            var return_data = new Array();
              for(var i=0;i< data.data.length; i++){
              return_data.push({
                'no'        : data.data[i].no,
                'username'     : data.data[i].username,
                'nama'   : data.data[i].nama,
                'status'    : data.data[i].isActive,
                'action'    : data.data[i].action,
              })
            }
            return return_data;
          }
        },
           "columnDefs": [
        { 
            "targets": [ 0 ], //first column / numbering column
            "orderable": false, //set not orderable
        },
        ],
        "columns"    : [
          {'data': 'no'},
          {'data': 'nama'},
          {'data': 'username'},
          {'data': 'status'},
          {'data': 'action'}
        ]
    });
  }
});

function reload_table()
{
    table.ajax.reload(null,false); //reload datatable ajax 
}

function insert()
{
    var form_action = $("#insert-modal").find("form").attr("action");
    var username = $("#insert-modal").find("input[name='username']").val();
    var password = calcMD5($("#insert-modal").find("input[name='password']").val());
    var nama = $("#insert-modal").find("input[name='nama']").val();
    var roleid = $("#insert-modal").find("select[name='roleid']").val();
    var isActive = 1;

    $.ajax({
        dataType: 'json',
        type:'POST',
        url: form_action,
        data:{
          username:username, 
          password:password,
          nama:nama,
          roleid:roleid,
          isActive:isActive
        }
    }).done(function(data){
        $(".modal").modal('hide');
        reload_table();
        toastr.success('Item Created Successfully.', 'Success Alert', {timeOut: 5000});
    });
}

function update(id)
{
   $.ajax({
    dataType: 'json',
    url: 'pengaturan-user/ambil-data-by-id/'+id,
    success: function ( data) {
      //console.log(data.data); //view the returned json in the console
      $("#update-modal").find("input[name='username']").val(data.data[0].username);
      $("#update-modal").find("input[name='nama']").val(data.data[0].nama);
      $("#update-modal").find("select[name='roleid']").val(data.data[0].roleid);
      $("#update-modal").find("input[name='password']").val(data.data[0].password);
      $("#update-modal").find("input[name='id']").val(data.data[0].id);
      $("#update-modal").find("input[name='isactive']").val(data.data[0].isActive);
      $("#update-modal").find("form").attr("action",'pengaturan-user/edit-data/'+id);
      $('#update-modal').modal('show');
    },
    error: function ( data ) {
      console.log('error');
    }
  });
}

function update_data()
{
  var form_action = $("#update-modal").find("form").attr("action");
    var username = $("#update-modal").find("input[name='username']").val();
    var password = $("#update-modal").find("input[name='password']").val();
    var nama = $("#update-modal").find("input[name='nama']").val();
    var roleid = $("#update-modal").find("select[name='roleid']").val();
    var isActive = $("#update-modal").find("input[name='isactive']").val();;
    
    $.ajax({
        dataType: 'json',
        type:'POST',
        url: form_action,
        data:{username:username, 
          password:password,
          nama:nama,
          roleid:roleid,
          isActive:isActive}
    }).done(function(data){
        $(".modal").modal('hide');
        reload_table();
        toastr.success('Item Updated Successfully.', 'Success Alert', {timeOut: 5000});
    });
}

function hapus(id)
{
  $.ajax({
    dataType: 'json',
    url: 'pengaturan-user/ambil-data-by-id/'+id,
    success: function ( data) {
      //console.log(data.data); //view the returned json in the console
      var nama = data.data[0].nama;
      var isactive = data.data[0].isActive;
      $.confirm({
        title : 'Hapus Data',
        content : 'Apakah Anda Yakin Untuk Menghapus Data : '+nama,
        buttons: {
        hapus: {
            btnClass: 'btn-blue',
            text:'Hapus',
            action: function(){
              $.ajax({
                dataType: 'json',
                type:'POST',
                url: 'pengaturan-user/edit-data/'+id,
                data:{
                  isActive:0
                }
              }).done(function(data){
                $(".modal").modal('hide');
                reload_table();
                toastr.success('Item Deleted Successfully.', 'Success Alert', {timeOut: 5000});
              });
            }
        },
        batal: {
            btnClass: 'btn-red any-other-class',
            text:'Batal'
          },
        }
      });   
    },
    error: function ( data ) {
      console.log('error');
    }
  });    
}

function activate(id){
  $.ajax({
    dataType: 'json',
    url: 'pengaturan-user/ambil-data-by-id/'+id,
    success: function (data) {
      var nama     = data.data[0].nama;
      var isactive = data.data[0].isActive;
      $.confirm({
        title : 'Aktifkan Data',
        content : 'Apakah Anda Yakin Untuk Mengaktifkan Kembali Data : '+nama,
        buttons: {
        hapus: {
            btnClass: 'btn-blue',
            text:'Aktifkan',
            action: function(){
              $.ajax({
                dataType: 'json',
                type:'POST',
                url: 'pengaturan-user/edit-data/'+id,
                data:{
                  id:id,
                  isActive:1
                }
              }).done(function(data){
                $(".modal").modal('hide');
                reload_table();
                toastr.success('Item Activated Successfully.', 'Success Alert', {timeOut: 5000});
              });
            }
        },
        batal: {
            btnClass: 'btn-red any-other-class',
            text:'Batal'
          },
        }
      });   
    },
    error: function ( data ) {
      console.log('error');
    }
  });    
}
