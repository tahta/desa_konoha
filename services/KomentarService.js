 var table;

 $(document).ready(function(){
    $('#btnSave').on('click',function(){
    var nama=$('#nama').val();
    var email=$('#email').val();
    var isi=$('#isi').val();
    
        $.ajax({
            type : "POST",
            url  : "http://localhost/desamargopatut/berita/tambah-komentar",
            dataType : "JSON",
            data : {nama:nama , email:email, isi:isi},
            success: function(data){
                console.log(nama);
                $('[name="nama"]').val("");
                $('[name="email"]').val("");
                $('[name="isi"]').val("");
                //$('#komentar').modal('hide');
                location.reload();
            }
        });
        return false;
    });


    manageData();
    function manageData() {
        table = $('#example2').DataTable({
            "processing": true,
            'serverSide': true,
            "paging": true,
            "lengthChange": false,
            "ordering": false,
            'ajax': {
              'type': 'POST',
              'url': url,
              dataSrc: function(data)
              {
                var return_data = new Array();
                  for(var i=0;i< data.data.length; i++){
                  return_data.push({
                    'no'        : data.data[i].no,
                    'judul'     : data.data[i].judul,
                    'nama_kom'  : data.data[i].nama_kom,
                    'email_kom' : data.data[i].email_kom,
                    'isi_kom'   : data.data[i].isi_kom,
                    'action'    : data.data[i].action,
                  })
                }
                return return_data;
              }
            },
               "columnDefs": [
            { 
                "targets": [ 0 ], //first column / numbering column
                "orderable": false, //set not orderable
            },
            ],
            "columns"    : [
              {'data': 'no'},
              {'data': 'judul'},
              {'data': 'nama_kom'},
              {'data': 'email_kom'},
              {'data': 'isi_kom'},
              {'data': 'action'}
            ]
        });
    }
});

function reload_table()
{
    table.ajax.reload(null,false); //reload datatable ajax 
}

function terima(id_kom){
  $.ajax({
    dataType: 'json',
    url: 'daftar-komentar/ambil-data-by-id/'+id_kom,
    success: function ( data) {
      //console.log(data.data); //view the returned json in the console
      var nama_kom = data.data[0].nama_kom;
      var isactive = data.data[0].isActive;
      $.confirm({
        title : 'Validasi Komentar',
        content : 'Apakah Anda Yakin Untuk Menampilkan Komentar : '+nama_kom,
        buttons: {
        hapus: {
            btnClass: 'btn-blue',
            text:'Validasi',
            action: function(){
              $.ajax({
                dataType: 'json',
                type:'POST',
                url: 'daftar-komentar/terima/'+id_kom,
                data:{
                  id_kom:id_kom,
                  isActive:1
                }
              }).done(function(data){
                $(".modal").modal('hide');
                reload_table();
                toastr.success('Item Validation Successfully.', 'Success Alert', {timeOut: 5000});
              });
            }
        },
        batal: {
            btnClass: 'btn-red any-other-class',
            text:'Batal'
          },
        }
      });   
    },
    error: function ( data ) {
      console.log('error');
    }
  });    
}

function tolak(id_kom){
  $.ajax({
    dataType: 'json',
    url: 'daftar-komentar/ambil-data-by-id/'+id_kom,
    success: function ( data) {
      //console.log(data.data); //view the returned json in the console
      var nama_kom = data.data[0].nama_kom;
      var isactive = data.data[0].isActive;
      $.confirm({
        title : 'Validasi Komentar',
        content : 'Apakah Anda Yakin Untuk Tidak Menampilkan Komentar : '+nama_kom,
        buttons: {
        hapus: {
            btnClass: 'btn-blue',
            text:'Validasi',
            action: function(){
              $.ajax({
                dataType: 'json',
                type:'POST',
                url: 'daftar-komentar/tolak/'+id_kom,
                data:{
                  id_kom:id_kom,
                  isActive:0
                }
              }).done(function(data){
                $(".modal").modal('hide');
                reload_table();
                toastr.success('Item Validation Successfully.', 'Success Alert', {timeOut: 5000});
              });
            }
        },
        batal: {
            btnClass: 'btn-red any-other-class',
            text:'Batal'
          },
        }
      });   
    },
    error: function ( data ) {
      console.log('error');
    }
  });    
}