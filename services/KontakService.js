var table;

$(document).ready(function(){
manageData();
  function manageData() {
    table = $('#example2').DataTable({
        "processing": true,
        'serverSide': true,
        "paging": true,
        "lengthChange": false,
        "ordering": false,
        'ajax': {
          'type': 'POST',
          'url': url,
          dataSrc: function(data)
          {
            var return_data = new Array();
              for(var i=0;i< data.data.length; i++){
              return_data.push({
                'no'              : data.data[i].no,
                'desa'            : data.data[i].desa,
                'alamat'          : data.data[i].alamat,
                'email'           : data.data[i].email,
                'no_telp'         : data.data[i].no_telp,
                'action'          : data.data[i].action,
              })
            }
            return return_data;
          }
        },
           "columnDefs": [
        { 
            "targets": [ 0 ], //first column / numbering column
            "orderable": false, //set not orderable
        },
        ],
        "columns"    : [
          {'data': 'no'},
          {'data': 'desa'},
          {'data': 'alamat'},
          {'data': 'email'},
          {'data': 'no_telp'},
          {'data': 'action'}
        ]
    });
  }
});

function reload_table()
{
    table.ajax.reload(null,false); //reload datatable ajax 
}

function update(id)
{
   $.ajax({
    dataType: 'json',
    url: 'kontak/ambil-data-by-id/'+id,
    success: function (data) {
      $("#update-modal").find("input[name='id']").val(data.data[0].id);
      $("#update-modal").find("input[name='desa']").val(data.data[0].desa);
      $("#update-modal").find("input[name='alamat']").val(data.data[0].alamat);
      $("#update-modal").find("input[name='email']").val(data.data[0].email);
      $("#update-modal").find("input[name='no_telp']").val(data.data[0].no_telp);
      $("#update-modal").find("form").attr("action",'kontak/edit-data/'+id);
      $('#update-modal').modal('show');
    },
    error: function ( data ) {
      console.log('error');
    }
  });
}

function update_data()
{
    var form_action      = $("#update-modal").find("form").attr("action");
    var id               = $("#update-modal").find("input[name='id']").val();
    var desa   = $("#update-modal").find("input[name='desa']").val();
    var alamat = $("#update-modal").find("input[name='alamat']").val();
    var email  = $("#update-modal").find("input[name='email']").val();
    var no_telp          = $("#update-modal").find("input[name='no_telp']").val();

    var form_data = new FormData();
    form_data.append('id', id);
    form_data.append('desa', desa);
    form_data.append('alamat', alamat);
    form_data.append('email', email);
    form_data.append('no_telp', no_telp);
    $.ajax({
        dataType: 'json',
        type:'POST',
        url: form_action,
        cache: false,
        contentType: false,
        processData: false,
        data:form_data,
        success : function (data, status)
        {
          if(data.status != 'error')
          {
            $(".modal").modal('hide');
            reload_table();
            $("#update-modal").find("input[name='id']").val('');
            $("#update-modal").find("input[name='desa']").val('');
            $("#update-modal").find("input[name='alamat']").val('');
            $("#update-modal").find("input[name='email']").val('');
            $("#update-modal").find("input[name='no_telp']").val('');
            toastr.success(data.msg, 'Success Alert', {timeOut: 5000});
          }
          else{
            toastr.error(data.msg, 'Error Alert', {timeOut: 5000});
          }
        }
    })
}


