-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 26, 2019 at 09:16 AM
-- Server version: 10.3.16-MariaDB
-- PHP Version: 7.3.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `desa`
--

-- --------------------------------------------------------

--
-- Table structure for table `agenda`
--

CREATE TABLE `agenda` (
  `id` int(11) NOT NULL,
  `tanggal` date NOT NULL,
  `judul` varchar(200) NOT NULL,
  `bagian` int(11) NOT NULL,
  `filepath` varchar(2000) NOT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `isActive` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `berita`
--

CREATE TABLE `berita` (
  `id` int(11) NOT NULL,
  `tanggal` date NOT NULL,
  `judul` varchar(100) NOT NULL,
  `isi` text NOT NULL,
  `imgpath` varchar(100) NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  `isActive` tinyint(1) NOT NULL,
  `id_kategori` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `berita`
--

INSERT INTO `berita` (`id`, `tanggal`, `judul`, `isi`, `imgpath`, `created_by`, `updated_by`, `created_at`, `updated_at`, `isActive`, `id_kategori`) VALUES
(1, '2019-08-03', 'Sumberkepuh PEMDES', '<p>asdsfdfdsfddfdfdtes</p>\n', 'http://localhost/desa/upload_file/news_image/cc7306871157f72dc7eb74f06fab9791.png', 6, 6, '2019-08-03', '2019-08-21', 1, 2),
(2, '2019-08-03', 'Ngadirejo Josss', '<p>Margo</p>\n', 'http://localhost/desa/upload_file/news_image/218f7d1bb1e15a50e9c3dce5e547018b.png', 5, 5, '2019-08-03', '2019-08-03', 1, 2),
(3, '2019-08-05', 'Sumberkepuh membangun1', '<p>adadadadad</p>\n', 'http://localhost/desa/upload_file/news_image/c9464835ccd914f1bc745d2538833efd.jpg', 6, 6, '2019-08-05', '2019-08-05', 1, 3),
(4, '2019-08-05', 'Sumberkepuh pemberdayaan1', '<p>pemberdayaan</p>\n', 'http://localhost/desa/upload_file/news_image/7114180edecde8c8bd62fcd3664433b3.jpg', 6, 6, '2019-08-05', '2019-08-05', 1, 4),
(5, '2019-08-05', 'Sumberkepuh membina', '<p>pembinaan bossssss</p>\n', 'http://localhost/desa/upload_file/news_image/2a833355cf9e4600ce24f388199ef04b.jpg', 6, 6, '2019-08-05', '2019-08-21', 1, 5),
(6, '2019-08-05', 'Ngadirejo Membangun', '<p>pembinaan bossssss</p>\r\n', 'http://localhost/desa/upload_file/news_image/2a833355cf9e4600ce24f388199ef04b.jpg', 5, 5, '2019-08-05', '2019-08-05', 1, 3),
(7, '2019-08-05', 'Ngadirejo pemberdayaan1', '<p>pemberdayaan</p>\r\n', 'http://localhost/desa/upload_file/news_image/7114180edecde8c8bd62fcd3664433b3.jpg', 5, 5, '2019-08-05', '2019-08-05', 1, 4),
(8, '2019-08-05', 'Ngadirejo membina', '<p>pembinaan bossssss</p>\r\n', 'http://localhost/desa/upload_file/news_image/2a833355cf9e4600ce24f388199ef04b.jpg', 5, 5, '2019-08-05', '2019-08-05', 1, 5);

-- --------------------------------------------------------

--
-- Table structure for table `data_desa`
--

CREATE TABLE `data_desa` (
  `id` int(11) NOT NULL,
  `namadesa` varchar(100) NOT NULL,
  `isActive` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `data_desa`
--

INSERT INTO `data_desa` (`id`, `namadesa`, `isActive`) VALUES
(1, 'Margopatut', 1),
(2, 'Bendolo', 1);

-- --------------------------------------------------------

--
-- Table structure for table `detail_desa`
--

CREATE TABLE `detail_desa` (
  `id` int(11) NOT NULL,
  `idDesa` int(11) DEFAULT NULL,
  `isi` text NOT NULL,
  `imgpath` text NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  `isActive` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `detail_galeri`
--

CREATE TABLE `detail_galeri` (
  `id` int(11) NOT NULL,
  `idgaleri` int(11) NOT NULL,
  `imgpath` varchar(100) NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL,
  `isActive` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `detail_galeri`
--

INSERT INTO `detail_galeri` (`id`, `idgaleri`, `imgpath`, `created_by`, `updated_by`, `created_at`, `updated_at`, `isActive`) VALUES
(1, 1, 'http://localhost/desa/upload_file/galeri/e66ea39f3f6841bfade09f5703310bf6.jpg', 5, 5, 2019, 2019, 1);

-- --------------------------------------------------------

--
-- Table structure for table `galeri`
--

CREATE TABLE `galeri` (
  `id` int(11) NOT NULL,
  `judul` varchar(100) NOT NULL,
  `tanggal` date NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  `isActive` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `galeri`
--

INSERT INTO `galeri` (`id`, `judul`, `tanggal`, `created_by`, `updated_by`, `created_at`, `updated_at`, `isActive`) VALUES
(1, 'kegiatan sumberkepuh', '2019-08-20', 5, 5, '2019-08-20', '2019-08-20', 1);

-- --------------------------------------------------------

--
-- Table structure for table `header_image`
--

CREATE TABLE `header_image` (
  `id` int(11) NOT NULL,
  `judul` varchar(100) NOT NULL,
  `imgpath` varchar(500) NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  `isActive` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `header_image`
--

INSERT INTO `header_image` (`id`, `judul`, `imgpath`, `created_by`, `updated_by`, `created_at`, `updated_at`, `isActive`) VALUES
(1, 'header1', 'http://localhost/desa/upload_file/header/f7b4b685d6c30db86101d23bbf41f958.jpg', 6, 6, '2019-08-14', '2019-08-14', 1),
(2, 'header2', 'http://localhost/desa/upload_file/header/798e2c36dafb7b03bfe70a493f6dac5d.jpg', 6, 6, '2019-08-14', '2019-08-14', 1),
(3, 'headersatu', 'http://localhost/desa/upload_file/header/2d838c2169b06b55ae2929db89bf834b.jpg', 5, 5, '2019-08-14', '2019-08-14', 1);

-- --------------------------------------------------------

--
-- Table structure for table `informasi`
--

CREATE TABLE `informasi` (
  `id` int(11) NOT NULL,
  `tanggal` date NOT NULL,
  `judul` varchar(150) NOT NULL,
  `isi` text NOT NULL,
  `filepath` text NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  `isActive` tinyint(1) NOT NULL,
  `status` enum('Transparansi Keuangan Desa','Data Penerima Bantuan','Prestasi Desa') DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `informasi`
--

INSERT INTO `informasi` (`id`, `tanggal`, `judul`, `isi`, `filepath`, `created_by`, `updated_by`, `created_at`, `updated_at`, `isActive`, `status`) VALUES
(1, '2019-07-26', 'Transparansi Ngadirejo', '<p>Transparansi Ngadirejo</p>\r\n', 'http://localhost/desamargopatut/upload_file/informasi/cfb7b37f43a1a81335ab9c0353bb4762.jpg', 5, 5, '2019-07-26', '2019-08-03', 1, 'Transparansi Keuangan Desa'),
(2, '2019-07-26', 'DPBD', '<p>DPBD Sumberkepuh</p>\r\n', 'http://localhost/desamargopatut/upload_file/informasi/cfb7b37f43a1a81335ab9c0353bb4762.jpg', 6, 6, '2019-07-26', '2019-08-03', 1, 'Data Penerima Bantuan'),
(3, '2019-07-26', 'Prestasi', '<p>Prestasi Sumberkepuh</p>\r\n', 'http://localhost/desamargopatut/upload_file/informasi/cfb7b37f43a1a81335ab9c0353bb4762.jpg', 6, 6, '2019-07-26', '2019-08-03', 1, 'Prestasi Desa'),
(4, '2019-08-05', 'Transparansi Sumberkepuh', '<p>Transparansi Sumberkepuh</p>\r\n', 'http://localhost/desa/upload_file/informasi/bf04c1506adb8a9bd2be8316dc4d8e10.JPG', 6, 6, '2019-08-05', '2019-08-05', 1, 'Transparansi Keuangan Desa'),
(5, '2019-08-05', 'Berprestasi Ngadirejo', '<p>Prestasi Ngadirejo</p>\r\n', 'http://localhost/desa/upload_file/informasi/4eb79174efd830def799fe1b5bc1204f.pdf', 5, 5, '2019-08-05', '2019-08-05', 1, 'Prestasi Desa'),
(6, '2019-07-26', 'DPBD Ngadirrrr', '<p>DPBD NGADIRRRREJOO</p>\r\n', 'http://localhost/desamargopatut/upload_file/informasi/cfb7b37f43a1a81335ab9c0353bb4762.jpg', 5, 5, '2019-07-26', '2019-08-03', 1, 'Data Penerima Bantuan');

-- --------------------------------------------------------

--
-- Table structure for table `kategori`
--

CREATE TABLE `kategori` (
  `id_kategori` int(50) NOT NULL,
  `nama_kategori` varchar(999) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kategori`
--

INSERT INTO `kategori` (`id_kategori`, `nama_kategori`) VALUES
(1, 'Berita Desa'),
(2, 'Pemerintahan Desa'),
(3, 'Pembangunan Desa'),
(4, 'Pemberdayaan Masyarakat'),
(5, 'Pembinaan Masyarakat'),
(6, 'Bumdes'),
(7, 'PKK'),
(8, 'Potensi Desa');

-- --------------------------------------------------------

--
-- Table structure for table `kategori_ppid`
--

CREATE TABLE `kategori_ppid` (
  `id_kategori` int(50) NOT NULL,
  `nama_kategori` varchar(999) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kategori_ppid`
--

INSERT INTO `kategori_ppid` (`id_kategori`, `nama_kategori`) VALUES
(1, 'PROFIL PEMERINTAH DESA'),
(2, 'INFORMASI PROGRAM DAN KEGIATAN'),
(3, 'INFORMASI PROGRAM MASUK DESA'),
(4, 'Program Pemerintah Pusat'),
(5, 'Program Pemerintah Provinsi'),
(6, 'Program Pemerintah Kabupaten'),
(7, 'Program Pihak Ketiga'),
(8, 'Rencana Pembangunan Jangka Menengah Desa'),
(9, 'Rencana Kerja Pemerintah Desa'),
(10, 'Daftar Usulan Rencana Kerja Pemerintah Daerah'),
(11, 'Anggaran Pendapatan dan Belanja Desa'),
(12, 'PERDES APB DESA'),
(13, 'Laporan Penyelenggaraan Pemerintah Desa\r\nAhli Tahun Anggaran'),
(14, 'Laporan Penyelenggaraan Pemerintah Desa\r\nAhli Masa Jabatan '),
(15, 'Laporan Kinerja Kepala Desa'),
(16, 'Laporan Pelaksanaan Tugas Perangkat Desa'),
(17, 'LRA APB Desa'),
(18, 'Laporan Realisasi Kegiatan'),
(19, 'PRODUK HUKUM DESA'),
(20, 'Peraturan Desa'),
(21, 'Peraturan Kepala Desa'),
(22, 'Peraturan Bersama Kepala Desa'),
(23, 'Keputusan Kepala Desa'),
(24, 'Tata Cara Mendapatkan Informasi PUBLIK DESA');

-- --------------------------------------------------------

--
-- Table structure for table `komentar`
--

CREATE TABLE `komentar` (
  `id_kom` bigint(255) NOT NULL,
  `id_berita` bigint(255) NOT NULL,
  `nama_kom` varchar(99) NOT NULL,
  `email_kom` varchar(99) NOT NULL,
  `kode_keamanan` varchar(99) NOT NULL,
  `isi_kom` text NOT NULL,
  `created_at` date NOT NULL,
  `isActive` int(99) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `komentar`
--

INSERT INTO `komentar` (`id_kom`, `id_berita`, `nama_kom`, `email_kom`, `kode_keamanan`, `isi_kom`, `created_at`, `isActive`) VALUES
(1, 1, 'dada', 'adada', '', 'adad', '2019-08-15', 1),
(2, 1, 'cac', 'sdadqaq', '', 'cac', '2019-08-15', 0),
(3, 1, '', '', '', '', '2019-08-15', 1),
(4, 1, 'nama3', 'bpkad@nganjukkab.go.id', '', 'hahaha', '2019-08-19', 1),
(5, 1, 'nama', 'bpkad@nganjukkab.go.id', '', 'hahaha asholole', '2019-08-19', 1),
(6, 4, 'Mamank', 'Mamank@gmaiol.vpom', '', 'Assalamualaikum mamank, jadi gini anda tau memeyx?', '2019-08-19', 0),
(7, 6, 'tes', 'tessss', '', 'ssss', '2019-08-20', 1);

-- --------------------------------------------------------

--
-- Table structure for table `kontak`
--

CREATE TABLE `kontak` (
  `id` int(11) NOT NULL,
  `nama_kecamatan` varchar(50) DEFAULT NULL,
  `alamat_kecamatan` text DEFAULT NULL,
  `email_kecamatan` varchar(100) DEFAULT NULL,
  `no_telp` varchar(50) DEFAULT NULL,
  `peta_kecamatan` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kontak`
--

INSERT INTO `kontak` (`id`, `nama_kecamatan`, `alamat_kecamatan`, `email_kecamatan`, `no_telp`, `peta_kecamatan`) VALUES
(1, 'Desa Margopatut', 'Sawahan, Kabupaten Nganjuk, Jawa Timur 64475', '', '(0358) 32 23 24', '<<iframe src=\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3953.6148484200744!2d111.7734850739709!3d-7.724406601228218!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e79adfba115ce35%3A0x6ddef61083d92041!2sKantor+Kecamatan+Sawahan!5e0!3m2!1sen!2sid!4v1563855079106!5m2!1sen!2sid\" width=\"100%\" height=\"100%\" frameborder=\"0\" style=\"border:0\" allowfullscreen></iframe>');

-- --------------------------------------------------------

--
-- Table structure for table `mst_bagian`
--

CREATE TABLE `mst_bagian` (
  `id` int(11) NOT NULL,
  `namabagian` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mst_bagian`
--

INSERT INTO `mst_bagian` (`id`, `namabagian`) VALUES
(1, 'Sekertariat');

-- --------------------------------------------------------

--
-- Table structure for table `pengumuman`
--

CREATE TABLE `pengumuman` (
  `id` int(11) NOT NULL,
  `judul` varchar(200) NOT NULL,
  `lampiran` varchar(1000) NOT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `isActive` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pengumuman`
--

INSERT INTO `pengumuman` (`id`, `judul`, `lampiran`, `created_at`, `updated_at`, `created_by`, `updated_by`, `isActive`) VALUES
(1, 'Peng Ngadirejo', 'http://localhost/kecsawahan/upload_file/pengumuman/2d878c7d720b90581c33b706454cc2c0.png', '2019-07-24', '2019-07-25', 5, 5, 1),
(2, 'Peng Sumberkepuh', 'http://localhost/desa/upload_file/pengumuman/5616a76be4f133d35f1e50e62dfb80b5.JPG', '2019-08-20', '2019-08-20', 6, 6, 1);

-- --------------------------------------------------------

--
-- Table structure for table `ppid`
--

CREATE TABLE `ppid` (
  `id` int(11) NOT NULL,
  `tanggal` date NOT NULL,
  `judul` varchar(150) NOT NULL,
  `isi` text NOT NULL,
  `filepath` text NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  `isActive` tinyint(1) NOT NULL,
  `id_kategori` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ppid`
--

INSERT INTO `ppid` (`id`, `tanggal`, `judul`, `isi`, `filepath`, `created_by`, `updated_by`, `created_at`, `updated_at`, `isActive`, `id_kategori`) VALUES
(1, '2019-08-23', 'Pemerintah Desa PDF', '<p>Pemdes</p>\n', 'http://localhost/desa/upload_file/ppid/418c8a3563d645b57587ec3708ac9b6d.pdf', 6, 6, '2019-08-23', '2019-08-26', 1, 1),
(2, '2019-08-23', 'Program dan Kegiatan', '<p>Program dan Kegiatan</p>\n', 'http://localhost/desa/upload_file/ppid/a43a98fc8b11b62ea07556e51acd1544.doc', 6, 6, '2019-08-23', '2019-08-26', 1, 2),
(3, '2019-08-23', 'Program Masuk Desa', '<p>Program Masuk Desa</p>\n', 'http://localhost/desa/upload_file/ppid/1ea51f4e25b20d933d0056889a27324e.pdf', 6, 6, '2019-08-23', '2019-08-26', 1, 3);

-- --------------------------------------------------------

--
-- Table structure for table `profil`
--

CREATE TABLE `profil` (
  `id` int(11) NOT NULL,
  `judul` varchar(100) DEFAULT NULL,
  `isi` text DEFAULT NULL,
  `filepath` text DEFAULT NULL,
  `status` enum('Pemerintah Desa','Sejarah','BPD','LPMD','Karang Taruna','PKK Desa','Kader Desa','RTRW') DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  `isActive` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `profil`
--

INSERT INTO `profil` (`id`, `judul`, `isi`, `filepath`, `status`, `created_by`, `updated_by`, `created_at`, `updated_at`, `isActive`) VALUES
(1, 'Pemerintah Desa Sumberkepuh', '<p>Pemdes&nbsp; SUMBERKEPUH GOKKK</p>\r\n', 'http://localhost/desa/upload_file/profil/50f8b6545fd66fdf8fdb6f57b2a7bf0f.jpg', 'Pemerintah Desa', 6, 6, '2019-07-09', '2019-08-06', 1),
(2, 'Sejarah', 'Sejarahipun Deso SUMBERKEPUUUHHHHHHHH', NULL, 'Sejarah', 6, 6, '2019-07-09', '2019-07-09', 1),
(3, 'Badan Permusyawaratan Desa', 'BPD Sumberkepuh', NULL, 'BPD', 6, 6, '2019-07-09', '2019-07-09', 1),
(4, 'LPMD', '<p>LPMD Sumberkepuh ENYOIII</p>\n', 'http://localhost/desa/upload_file/profil/1b309e33d7f9ae8d63ddd845736a535d.jpg', 'LPMD', 6, 6, '2019-07-09', '2019-08-07', 1),
(5, 'Karang Taruna', 'KARTAR Sumberkepuh', NULL, 'Karang Taruna', 6, 6, '2019-07-09', '2019-07-09', 1),
(6, 'PKK Desa', 'PKK Sumberkepuh', NULL, 'PKK Desa', 6, 6, '2019-07-09', '2019-07-09', 1),
(7, 'Kader Desa', '<p>Kader desa Sumberkepuh</p>\n', 'http://localhost/desa/upload_file/profil/dbbc79b9f0e267f71157b456b6b04666.png', 'Kader Desa', 6, 6, '2019-07-09', '2019-08-07', 1),
(8, 'RT-RW', 'RT-RW Sumberkepuh', NULL, 'RTRW', 6, 6, '2019-07-09', '2019-07-09', 1),
(11, 'Pemerintah Desa Ngadirejo', '<p>NGADIREJO ASOYYYY</p>\n', 'http://localhost/desa/upload_file/profil/72bb6d539bcb540f46426ff655d379b2.jpg', 'Pemerintah Desa', 5, 5, '2019-07-09', '2019-08-07', 1),
(12, 'Sejarah', 'Sejarahipun NGADIREJOOOOOO', NULL, 'Sejarah', 5, 5, '2019-07-09', '2019-07-09', 1);

-- --------------------------------------------------------

--
-- Table structure for table `sysrole`
--

CREATE TABLE `sysrole` (
  `id` int(11) NOT NULL,
  `nama` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sysrole`
--

INSERT INTO `sysrole` (`id`, `nama`) VALUES
(1, 'Super Admin'),
(2, 'Admin');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `roleid` int(11) NOT NULL,
  `isActive` tinyint(1) NOT NULL,
  `slug` varchar(50) NOT NULL,
  `desa` varchar(50) NOT NULL,
  `alamat` text NOT NULL,
  `email` varchar(100) NOT NULL,
  `no_telp` varchar(50) NOT NULL,
  `peta_desa` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `nama`, `roleid`, `isActive`, `slug`, `desa`, `alamat`, `email`, `no_telp`, `peta_desa`) VALUES
(1, 'rootadmin', 'cd92a26534dba48cd785cdcc0b3e6bd1', 'Root', 1, 1, '', '', '', '', '', ''),
(2, 'admin', '21232f297a57a5a743894a0e4a801fc3', 'Admin', 2, 1, '', '', '', '', '', ''),
(3, 'vesta123', '285c65aae106302ffba3ce3a21dab3f1', 'vesta', 2, 1, '', '', '', '', '', ''),
(4, 'adminopd', 'e5c1ec96ad72771de806d1e2809e83e6', 'Admin OPD', 1, 1, '', '', '', '', '', ''),
(5, 'ngadirejo', 'cd92a26534dba48cd785cdcc0b3e6bd1', 'Desa Ngadirejo', 2, 1, 'ngadirejo', 'Desa Ngadirejo', 'Ngadirejo, Tanjunganom, Nganjuk', 'margopatut@gmail.com', '(0358) 089 0000', '<<iframe src=\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3953.6148484200744!2d111.7734850739709!3d-7.724406601228218!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e79adfba115ce35%3A0x6ddef61083d92041!2sKantor+Kecamatan+Sawahan!5e0!3m2!1sen!2sid!4v1563855079106!5m2!1sen!2sid\" width=\"100%\" height=\"100%\" frameborder=\"0\" style=\"border:0\" allowfullscreen></iframe>'),
(6, 'sumberkepuh', 'cd92a26534dba48cd785cdcc0b3e6bd1', 'Desa Sumberkepuh', 2, 1, 'sumberkepuh', 'Desa Sumberkepuh', 'Sumberkepuh, Tanjunganom, Kab. Nganjuk', 'bendolo@gmail.com', '(0358) 98762736', '<<iframe src=\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3953.6148484200744!2d111.7734850739709!3d-7.724406601228218!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e79adfba115ce35%3A0x6ddef61083d92041!2sKantor+Kecamatan+Sawahan!5e0!3m2!1sen!2sid!4v1563855079106!5m2!1sen!2sid\" width=\"100%\" height=\"100%\" frameborder=\"0\" style=\"border:0\" allowfullscreen></iframe>');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `agenda`
--
ALTER TABLE `agenda`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `berita`
--
ALTER TABLE `berita`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `data_desa`
--
ALTER TABLE `data_desa`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `detail_desa`
--
ALTER TABLE `detail_desa`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `detail_galeri`
--
ALTER TABLE `detail_galeri`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `galeri`
--
ALTER TABLE `galeri`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `header_image`
--
ALTER TABLE `header_image`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `informasi`
--
ALTER TABLE `informasi`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kategori`
--
ALTER TABLE `kategori`
  ADD PRIMARY KEY (`id_kategori`);

--
-- Indexes for table `kategori_ppid`
--
ALTER TABLE `kategori_ppid`
  ADD PRIMARY KEY (`id_kategori`);

--
-- Indexes for table `komentar`
--
ALTER TABLE `komentar`
  ADD PRIMARY KEY (`id_kom`);

--
-- Indexes for table `kontak`
--
ALTER TABLE `kontak`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mst_bagian`
--
ALTER TABLE `mst_bagian`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pengumuman`
--
ALTER TABLE `pengumuman`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ppid`
--
ALTER TABLE `ppid`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `profil`
--
ALTER TABLE `profil`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sysrole`
--
ALTER TABLE `sysrole`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `agenda`
--
ALTER TABLE `agenda`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `berita`
--
ALTER TABLE `berita`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `data_desa`
--
ALTER TABLE `data_desa`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `detail_desa`
--
ALTER TABLE `detail_desa`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `detail_galeri`
--
ALTER TABLE `detail_galeri`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `galeri`
--
ALTER TABLE `galeri`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `header_image`
--
ALTER TABLE `header_image`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `informasi`
--
ALTER TABLE `informasi`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `kategori_ppid`
--
ALTER TABLE `kategori_ppid`
  MODIFY `id_kategori` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `komentar`
--
ALTER TABLE `komentar`
  MODIFY `id_kom` bigint(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `kontak`
--
ALTER TABLE `kontak`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `mst_bagian`
--
ALTER TABLE `mst_bagian`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `pengumuman`
--
ALTER TABLE `pengumuman`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `ppid`
--
ALTER TABLE `ppid`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `profil`
--
ALTER TABLE `profil`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `sysrole`
--
ALTER TABLE `sysrole`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
