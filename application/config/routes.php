<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/

$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
$route['auth/login'] = 'auth/Login/index';
$route['auth/validatelogin'] = 'auth/Auth/validatelogin';
$route['home/getnganjukkab']['post'] = 'welcome/getNewsNganjukkab';
$route['agenda/semua-agenda'] = 'welcome/getAgenda';
$route['agenda/semua-agenda/(:any)'] = 'welcome/getAgendaById/$1';
$route['pengumuman/semua-pengumuman'] = 'welcome/getPengumuman';
$route['pengumuman/semua-pengumuman/(:any)'] = 'welcome/getPengumumanById/$1';
$route['berita'] = 'welcome/get_news';
$route['berita1'] = 'welcome/get_news1';
$route['header-image'] = 'welcome/getHeaderImage';
$route['(:any)/beranda'] = 'welcome/index';
$route['(:any)/profil/(:any)'] = 'welcome/profil/$2';
$route['(:any)/berita/semua-berita'] = 'welcome/semua_berita';
$route['(:any)/berita/detail-berita/(:any)'] = 'welcome/detailBerita/$2';
$route['kelurahan/(:any)'] = 'welcome/kelurahan/$1';
$route['all-kelurahan'] = 'welcome/all_kelurahan';
$route['galeri/semua-galeri'] = 'welcome/semua_galeri';
$route['galeri/detail-galeri/(:any)'] = 'welcome/detail_galeri/$1';
$route['galeri'] = 'welcome/galeriimage';
$route['visitor/galeri/detail-galeri/(:any)'] = 'welcome/getDetailGallery/$1';

//route informasi
$route['(:any)/informasi/transparansi_desa'] = 'welcome/transparansi_desa';
$route['(:any)/informasi/detail_transparansi/(:any)'] = 'welcome/detailtransparansi/$2';
$route['(:any)/informasi/penerima_bantuan'] = 'welcome/datapenerimabantuan';
$route['(:any)/informasi/detail_bantuan/(:any)'] = 'welcome/detailpenerimabantuan/$2';
$route['(:any)/informasi/prestasi_desa'] = 'welcome/prestasidesa';
$route['(:any)/informasi/detail_prestasi/(:any)'] = 'welcome/detailprestasidesa/$2';

//admin routing
$route['admin'] = 'AdminController/index';
$route['admin/dashboard'] = 'AdminController/dashboardinfo';

//berita
$route['admin/berita'] = 'BeritaController/index';
$route['admin/berita/ambil-data'] = 'BeritaController/getData';
$route['admin/berita/insert-data']['post'] = 'BeritaController/insertData';
$route['admin/berita/ambil-data-by-id/(:any)'] = 'BeritaController/getById/$1';
$route['admin/berita/edit-data/(:any)']['post'] = 'BeritaController/editData/$1';
$route['admin/berita/remove/(:any)']['post'] = 'BeritaController/delete/$1';
$route['admin/berita/activate/(:any)']['post'] = 'BeritaController/activate/$1';
$route['admin/berita/remove_data/(:any)']['post'] = 'BeritaController/delete_data/$1';

//tambah-komentar
$route['(:any)/berita/tambah-komentar/(:num)']['post']='KomentarController/addKomen/$2';

//galeri
$route['admin/galeri'] = 'GaleriController/index';
$route['admin/galeri/ambil-data'] = 'GaleriController/getData';
$route['admin/galeri/insert-data']['post'] = 'GaleriController/insertData';
$route['admin/galeri/detail-galeri/(:any)'] = 'GaleriController/getDetailGallery/$1';
$route['admin/galeri/edit-data/(:any)'] = 'GaleriController/getGallery/$1';
$route['admin/galeri/update-data']['post'] = 'GaleriController/editGallery';
$route['admin/galeri/remove/(:any)']['post'] = 'GaleriController/delete/$1';
$route['admin/galeri/activate/(:any)']['post'] = 'GaleriController/activate/$1';

//pengaturan-user
$route['admin/pengaturan-user'] = 'UserController/index';
$route['admin/pengaturan-user/ambil-data'] = 'UserController/getData';
$route['admin/pengaturan-user/insert-data']['post'] = 'UserController/insertData';
$route['admin/pengaturan-user/ambil-data-by-id/(:any)'] = 'UserController/getById/$1';
$route['admin/pengaturan-user/edit-data/(:any)']['post'] = 'UserController/editData/$1';

//pemerintah_desa
$route['admin/pemerintah_desa'] = 'PemerintahDesaController/index';
$route['admin/pemerintah_desa/ambil-data'] = 'PemerintahDesaController/getData';
$route['admin/pemerintah_desa/insert-data']['post'] = 'PemerintahDesaController/insertData';
$route['admin/pemerintah_desa/ambil-data-by-id/(:any)'] = 'PemerintahDesaController/getById/$1';
$route['admin/pemerintah_desa/edit-data/(:any)']['post'] = 'PemerintahDesaController/editData/$1';

//profil-sejarah
$route['admin/profil-sejarah'] = 'ProfilSejarahController/index';
$route['admin/profil-sejarah/ambil-data'] = 'ProfilSejarahController/getData';
$route['admin/profil-sejarah/insert-data']['post'] = 'ProfilSejarahController/insertData';
$route['admin/profil-sejarah/ambil-data-by-id/(:any)'] = 'ProfilSejarahController/getById/$1';
$route['admin/profil-sejarah/edit-data/(:any)']['post'] = 'ProfilSejarahController/editData/$1';

//BPD
$route['admin/bpd'] = 'BpdController/index';
$route['admin/bpd/ambil-data'] = 'BpdController/getData';
$route['admin/bpd/insert-data']['post'] = 'BpdController/insertData';
$route['admin/bpd/ambil-data-by-id/(:any)'] = 'BpdController/getById/$1';
$route['admin/bpd/edit-data/(:any)']['post'] = 'BpdController/editData/$1';

//LPMD
$route['admin/lpmd'] = 'LPMDController/index';
$route['admin/lpmd/ambil-data'] = 'LPMDController/getData';
$route['admin/lpmd/insert-data']['post'] = 'LPMDController/insertData';
$route['admin/lpmd/ambil-data-by-id/(:any)'] = 'LPMDController/getById/$1';
$route['admin/lpmd/edit-data/(:any)']['post'] = 'LPMDController/editData/$1';

//Karang Taruna
$route['admin/kartar'] = 'KartarController/index';
$route['admin/kartar/ambil-data'] = 'KartarController/getData';
$route['admin/kartar/insert-data']['post'] = 'KartarController/insertData';
$route['admin/kartar/ambil-data-by-id/(:any)'] = 'KartarController/getById/$1';
$route['admin/kartar/edit-data/(:any)']['post'] = 'KartarController/editData/$1';

//PKK Desa
$route['admin/pkkdesa'] = 'PkkDesaController/index';
$route['admin/pkkdesa/ambil-data'] = 'PkkDesaController/getData';
$route['admin/pkkdesa/insert-data']['post'] = 'PkkDesaController/insertData';
$route['admin/pkkdesa/ambil-data-by-id/(:any)'] = 'PkkDesaController/getById/$1';
$route['admin/pkkdesa/edit-data/(:any)']['post'] = 'PkkDesaController/editData/$1';

//Kader Desa
$route['admin/kaderdesa'] = 'KaderDesaController/index';
$route['admin/kaderdesa/ambil-data'] = 'KaderDesaController/getData';
$route['admin/kaderdesa/insert-data']['post'] = 'KaderDesaController/insertData';
$route['admin/kaderdesa/ambil-data-by-id/(:any)'] = 'KaderDesaController/getById/$1';
$route['admin/kaderdesa/edit-data/(:any)']['post'] = 'KaderDesaController/editData/$1';

//RT RW
$route['admin/rtrw'] = 'RTRWController/index';
$route['admin/rtrw/ambil-data'] = 'RTRWController/getData';
$route['admin/rtrw/insert-data']['post'] = 'RTRWController/insertData';
$route['admin/rtrw/ambil-data-by-id/(:any)'] = 'RTRWController/getById/$1';
$route['admin/rtrw/edit-data/(:any)']['post'] = 'RTRWController/editData/$1';

//Kontak
$route['admin/kontak'] = 'KontakController/index';
$route['admin/kontak/ambil-data'] = 'KontakController/getData';
$route['admin/kontak/insert-data']['post'] = 'KontakController/insertData';
$route['admin/kontak/ambil-data-by-id/(:any)'] = 'KontakController/getById/$1';
$route['admin/kontak/edit-data/(:any)']['post'] = 'KontakController/editData/$1';

//transparansi_desa
$route['admin/transparansi_desa'] = 'TransparansiDesaController/index';
$route['admin/transparansi_desa/ambil-data'] = 'TransparansiDesaController/getData';
$route['admin/transparansi_desa/insert-data']['post'] = 'TransparansiDesaController/insertData';
$route['admin/transparansi_desa/ambil-data-by-id/(:any)'] = 'TransparansiDesaController/getById/$1';
$route['admin/transparansi_desa/edit-data/(:any)']['post'] = 'TransparansiDesaController/editData/$1';
$route['admin/transparansi_desa/remove/(:any)']['post'] = 'TransparansiDesaController/delete/$1';
$route['admin/transparansi_desa/activate/(:any)']['post'] = 'TransparansiDesaController/activate/$1';
$route['admin/transparansi_desa/remove_data/(:any)']['post'] = 'TransparansiDesaController/delete_data/$1';

//penerima_bantuan
$route['admin/penerima_bantuan'] = 'PenerimaBantuanController/index';
$route['admin/penerima_bantuan/ambil-data'] = 'PenerimaBantuanController/getData';
$route['admin/penerima_bantuan/insert-data']['post'] = 'PenerimaBantuanController/insertData';
$route['admin/penerima_bantuan/ambil-data-by-id/(:any)'] = 'PenerimaBantuanController/getById/$1';
$route['admin/penerima_bantuan/edit-data/(:any)']['post'] = 'PenerimaBantuanController/editData/$1';
$route['admin/penerima_bantuan/remove/(:any)']['post'] = 'PenerimaBantuanController/delete/$1';
$route['admin/penerima_bantuan/activate/(:any)']['post'] = 'PenerimaBantuanController/activate/$1';
$route['admin/penerima_bantuan/remove_data/(:any)']['post'] = 'PenerimaBantuanController/delete_data/$1';

//prestasi desa
$route['admin/prestasi_desa'] = 'PrestasiDesaController/index';
$route['admin/prestasi_desa/ambil-data'] = 'PrestasiDesaController/getData';
$route['admin/prestasi_desa/insert-data']['post'] = 'PrestasiDesaController/insertData';
$route['admin/prestasi_desa/ambil-data-by-id/(:any)'] = 'PrestasiDesaController/getById/$1';
$route['admin/prestasi_desa/edit-data/(:any)']['post'] = 'PrestasiDesaController/editData/$1';
$route['admin/prestasi_desa/remove/(:any)']['post'] = 'PrestasiDesaController/delete/$1';
$route['admin/prestasi_desa/activate/(:any)']['post'] = 'PrestasiDesaController/activate/$1';
$route['admin/prestasi_desa/remove_data/(:any)']['post'] = 'PrestasiDesaController/delete_data/$1';

//navbar
$route['(:any)/pemerintahan_desa'] = 'welcome/pemerintahan_desa';
$route['(:any)/pembangunan_desa'] = 'welcome/pembangunan_desa';
$route['(:any)/pemberdayaan_masyarakat'] = 'welcome/pemberdayaan_masyarakat';
$route['(:any)/pembinaan_masyarakat'] = 'welcome/pembinaan_masyarakat';
$route['(:any)/bum_desa'] = 'welcome/bum_desa';
$route['(:any)/pkk_desa'] = 'welcome/pkk_desa';
$route['(:any)/potensi_desa'] = 'welcome/potensi_desa';

//Header
$route['admin/gambar-header'] = 'HeaderController/index';
$route['admin/gambar-header/ambil-data'] = 'HeaderController/getData';
$route['admin/gambar-header/insert-data']['post'] = 'HeaderController/insertData';
$route['admin/gambar-header/ambil-data-by-id/(:any)'] = 'HeaderController/getById/$1';
$route['admin/gambar-header/edit-data/(:any)'] = 'HeaderController/editData/$1';
$route['admin/gambar-header/remove/(:any)']['post'] = 'HeaderController/delete/$1';


//Daftar Komentar
$route['admin/daftar-komentar'] = 'KomentarController/daftarKomentar';
$route['admin/daftar-komentar/ambil-data'] = 'KomentarController/getData';
$route['admin/daftar-komentar/ambil-data-by-id/(:any)'] = 'KomentarController/getById/$1';
$route['admin/daftar-komentar/terima/(:any)']['post'] = 'KomentarController/terima/$1';
$route['admin/daftar-komentar/tolak/(:any)']['post'] = 'KomentarController/tolak/$1';

//pengumuman
$route['admin/pengumuman'] = 'PengumumanController/index';
$route['admin/pengumuman/ambil-data'] = 'PengumumanController/getData';
$route['admin/pengumuman/insert-data']['post'] = 'PengumumanController/insertData';
$route['admin/pengumuman/ambil-data-by-id/(:any)'] = 'PengumumanController/getById/$1';
$route['admin/pengumuman/edit-data/(:any)'] = 'PengumumanController/editData/$1';
$route['admin/pengumuman/remove/(:any)']['post'] = 'PengumumanController/delete/$1';

//agenda
$route['admin/agenda'] = 'AgendaController/index';
$route['admin/agenda/ambil-data'] = 'AgendaController/getData';
$route['admin/agenda/insert-data']['post'] = 'AgendaController/insertData';
$route['admin/agenda/ambil-data-by-id/(:any)'] = 'AgendaController/getById/$1';
$route['admin/agenda/edit-data/(:any)'] = 'AgendaController/editData/$1';
$route['admin/agenda/remove/(:any)']['post'] = 'AgendaController/delete/$1';
$route['admin/agenda/activate/(:any)']['post'] = 'AgendaController/activate/$1';

//ppid_desa
$route['admin/ppid'] = 'PPIDController/index';
$route['admin/ppid/ambil-data'] = 'PPIDController/getData';
$route['admin/ppid/insert-data']['post'] = 'PPIDController/insertData';
$route['admin/ppid/ambil-data-by-id/(:any)'] = 'PPIDController/getById/$1';
$route['admin/ppid/edit-data/(:any)']['post'] = 'PPIDController/editData/$1';
$route['admin/ppid/remove/(:any)']['post'] = 'PPIDController/delete/$1';
$route['admin/ppid/activate/(:any)']['post'] = 'PPIDController/activate/$1';
$route['admin/ppid/remove_data/(:any)']['post'] = 'PPIDController/delete_data/$1';

//PPID Informasi Berkala
$route['(:any)/informasi/berkala'] = 'welcome/informasiberkala';
$route['(:any)/informasi/berkala/peraturandesa'] = 'welcome/peraturandesa';

$route['captcha/refresh'] = "welcome/refresh";

$route['(:any)'] = 'welcome';

//API ANALISA
$route['api/analisa/desa/getberita'] = 'Api/getBerita';
$route['api/analisa/desa/getppid'] = 'Api/getPPID';

//Reset Password
$route['admin/resetPassword']                       = 'ResetPasswordController/ubahPassword';
$route['admin/pengaturan-user/ubah-pass']['post']   = 'ResetPasswordController/updatePass';

//importcovid19
$route['admin/importcovid19']                        = 'Covid19Controller/index';
$route['admin/importcovid19/import']                 = 'Covid19Controller/import';
$route['admin/importcovid19/insert']                 = 'Covid19Controller/insert';
$route['admin/laporancovid19']                       = 'Covid19Controller/laporan';

//PPID MBUH PROFIL TA OPO SEMBARANGGG WESSS
$route['admin/profilppid'] = 'PPIDdesa/index';
$route['admin/profilppid/ambil-data'] = 'PPIDdesa/getData';
$route['admin/profilppid/insert-data']['post'] = 'PPIDdesa/insertData';
$route['admin/profilppid/ambil-data-by-id/(:any)'] = 'PPIDdesa/getById/$1';
$route['admin/profilppid/edit-data']['post'] = 'PPIDdesa/editData';
$route['admin/profilppid/remove/(:any)']['post'] = 'PPIDdesa/delete/$1';
$route['admin/profilppid/activate/(:any)']['post'] = 'PPIDdesa/activate/$1';
$route['admin/profilppid/remove_data/(:any)']['post'] = 'PPIDdesa/delete_data/$1';
$route['admin/profilppid/check_judul']= 'PPIDdesa/check_judul';

//INFORMASI SERTA MERTA WOEEE!!!
$route['admin/serta-merta'] = 'SertaMerta/index';
$route['admin/serta-merta/ambil-data'] = 'SertaMerta/getData';
$route['admin/serta-merta/insert-data']['post'] = 'SertaMerta/insertData';
$route['admin/serta-merta/ambil-data-by-id/(:any)'] = 'SertaMerta/getById/$1';
$route['admin/serta-merta/edit-data']['post'] = 'SertaMerta/editData';
$route['admin/serta-merta/remove/(:any)']['post'] = 'SertaMerta/delete/$1';
$route['admin/serta-merta/activate/(:any)']['post'] = 'SertaMerta/activate/$1';
$route['admin/serta-merta/remove_data/(:any)']['post'] = 'SertaMerta/delete_data/$1';
$route['admin/serta-merta/check_judul']= 'SertaMerta/check_judul';

//DIP ONLINE COOOOOKKKKKKKKKKKKKK
$route['admin/dip_online'] = 'DIPonline/index';
$route['admin/dip_online/ambil-data'] = 'DIPonline/getData';
$route['admin/dip_online/insert-data']['post'] = 'DIPonline/insertData';
$route['admin/dip_online/ambil-data-by-id/(:any)'] = 'DIPonline/getById/$1';
$route['admin/dip_online/edit-data']['post'] = 'DIPonline/editData';
$route['admin/dip_online/remove/(:any)']['post'] = 'DIPonline/delete/$1';
$route['admin/dip_online/activate/(:any)']['post'] = 'DIPonline/activate/$1';
$route['admin/dip_online/remove_data/(:any)']['post'] = 'DIPonline/delete_data/$1';

//routes SKPPID DINAMOR
$route['(:any)/informasi/sk_ppid'] = 'welcome/sk_ppid';
$route['admin/sk_ppid'] = 'SKController/index';
$route['admin/sk_ppid/ambil-data'] = 'SKController/getData';
$route['admin/sk_ppid/insert-data']['post'] = 'SKController/insertData';
$route['admin/sk_ppid/ambil-data-by-id/(:any)'] = 'SKController/getById/$1';
$route['admin/sk_ppid/edit-data/(:any)']['post'] = 'SKController/editData/$1';
$route['admin/sk_ppid/remove/(:any)']['post'] = 'SKController/delete/$1';
$route['admin/sk_ppid/activate/(:any)']['post'] = 'SKController/activate/$1';
$route['admin/sk_ppid/remove_data/(:any)']['post'] = 'SKController/delete_data/$1';
$route['(:any)/informasi/detail_sk_ppid/(:any)'] = 'welcome/detailsk_ppid/$2';

//routes Alur PPID MBOOT
$route['(:any)/informasi/alurpermohonan'] = 'welcome/alurpermohonan';
$route['(:any)/informasi/alurkeberatan'] = 'welcome/alurkeberatan';

///PPID HALAMAN VIEW!!!
$route['(:any)/ppidview/(:any)'] = 'PPIDView/profil_ppid/$2';
$route['(:any)/informasi/diponline'] = 'PPIDView/diponline';
$route['(:any)/informasi/sertamerta'] = 'PPIDView/sertamerta';