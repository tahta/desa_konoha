<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct()
	{
		parent::__construct();
		$this->load->library('session');
		$this->load->helper('captcha');
		$this->setSession();
	
	}

	private function setSession(){
		$slug = $this->uri->segment(1);
		if(isset($slug)){
			$this->load->database();
			$this->db->select('id, desa, alamat, email, no_telp, peta_desa, slug');
			$this->db->where('slug', $slug);
			$this->db->from('users');
			$query = $this->db->get()->result();
			if($query){
				$this->session->set_userdata('view_id', $query[0]->id);
				$this->session->set_userdata('view_desa', $query[0]->desa);
				$this->session->set_userdata('view_alamat', $query[0]->alamat);
				$this->session->set_userdata('view_email', $query[0]->email);
				$this->session->set_userdata('view_no_telp', $query[0]->no_telp);
				$this->session->set_userdata('view_peta_desa', $query[0]->peta_desa);
				$this->session->set_userdata('view_slug', $query[0]->slug);

			}
		}
	}

	public function index()
	{
		$slug = $this->uri->segment(1);
		if(isset($slug)){
			$this->load->database();
			$this->db->select('id, desa, alamat, email, no_telp, peta_desa, slug');
			$this->db->where('slug', $slug);
			$this->db->from('users');
			$query = $this->db->get()->result();
			if($query){
				$this->session->set_userdata('view_id', $query[0]->id);
				$this->session->set_userdata('view_desa', $query[0]->desa);
				$this->session->set_userdata('view_alamat', $query[0]->alamat);
				$this->session->set_userdata('view_email', $query[0]->email);
				$this->session->set_userdata('view_no_telp', $query[0]->no_telp);
				$this->session->set_userdata('view_peta_desa', $query[0]->peta_desa);
				$this->session->set_userdata('view_slug', $query[0]->slug);

			}
		}

		$this->db->select('komentar.id_berita, komentar.id_kom, komentar.nama_kom, komentar.email_kom, komentar.isi_kom, komentar.created_at, komentar.isActive, berita.created_by, users.slug');
		$this->db->join('berita','komentar.id_berita = berita.id','INNER');
		$this->db->join('users','berita.created_by = users.id','INNER');
		$this->db->where(array('komentar.isActive' => 1,'users.slug' => $slug));
		$this->db->order_by('komentar.created_at', 'desc');
		$this->db->limit(10,0);
		$komen = $this->db->get('komentar')->result();
		$data['komentar'] = $komen;
			
		$this->load->view('visitors/page/home',$data);
	}

	public function getNewsNganjukkab()
	{
		$nganjukkabdb = $this->load->database('nganjuk', TRUE);
		$nganjukkabdb->select('berita.id as idberita,berita.tanggal,berita.judul,berita.isi,berita.imgpath,berita.isActive,users.nama');
		$nganjukkabdb->from('berita');
		$nganjukkabdb->join('users','berita.created_by = users.id','INNER');
		$nganjukkabdb->where(array('berita.isActive' => 1));
		$nganjukkabdb->limit(8,0);
		$nganjukkabdb->order_by("berita.tanggal", "desc");
		$query = $nganjukkabdb->get();
		$data['data'] =$query->result();
		echo json_encode($data);
	}

	public function getAgenda()
	{
		$this->load->database();
		$this->db->select('agenda.id,agenda.tanggal,agenda.judul,agenda.bagian,agenda.filepath,agenda.isActive,mst_bagian.namabagian');
		$this->db->from('agenda');
		$this->db->join('mst_bagian','agenda.bagian = mst_bagian.id','INNER');
		$this->db->where(array('agenda.created_by' => $this->session->userdata('view_id')));
		$this->db->where(array('agenda.isActive' => 1));
		$this->db->limit(4);
		$this->db->order_by("agenda.id", "desc");
		$query = $this->db->get();
		$data['data'] =$query->result();
		echo json_encode($data);
	}

	public function getAgendaById($id)
	{
		$this->load->database();
		$this->db->select('agenda.id,agenda.tanggal,agenda.judul,agenda.bagian,agenda.isActive,agenda.filepath,mst_bagian.namabagian');
		$this->db->from('agenda');
		$this->db->join('mst_bagian','agenda.bagian = mst_bagian.id','INNER');
		$this->db->where(array('agenda.isActive' => 1, 'agenda.id' => $id));
		$query = $this->db->get();
		$data['data'] =$query->result();
		echo json_encode($data);	
	}

	public function getPengumuman()
	{
		$this->load->database();
		$this->db->select('*');
		$this->db->from('pengumuman');
		$this->db->where(array('pengumuman.created_by' => $this->session->userdata('view_id')));
		$this->db->where(array('pengumuman.isActive' => 1));
		$this->db->limit(4);
		$this->db->order_by("pengumuman.id", "desc");
		$query = $this->db->get();
		$data['data'] =$query->result();
		echo json_encode($data);
	}

	public function getPengumumanById($id)
	{
		$this->load->database();
		$this->db->select('*');
		$this->db->from('pengumuman');
		$this->db->where(array('pengumuman.isActive' => 1, 'pengumuman.id' => $id));
		$query = $this->db->get();
		$data['data'] =$query->result();
		echo json_encode($data);	
	}

	public function get_news()
	{
		$this->load->database();
        $this->db->select('berita.id,berita.tanggal,berita.judul,berita.isi,berita.imgpath,berita.isActive,users.nama, berita.created_by');
		$this->db->from('berita');
		$this->db->join('users','berita.created_by = users.id','INNER');
		$this->db->where(array('berita.isActive' => 1));
		$this->db->where(array('berita.created_by' => $this->session->userdata('view_id')));
		$this->db->limit(3,1);
        $this->db->order_by("berita.id", "desc");
       	$query = $this->db->get();
       	$data['data'] = $query->result();
       	echo json_encode($data);
	}

	public function get_news1()
	{
		$this->load->database();
        $this->db->select('berita.id,berita.tanggal,berita.judul,berita.isi,berita.imgpath,berita.isActive,users.nama');
		$this->db->from('berita');
		$this->db->join('users','berita.created_by = users.id','INNER');
		$this->db->where(array('berita.isActive' => 1));
		$this->db->where(array('berita.created_by' => $this->session->userdata('view_id')));
		$this->db->limit(1,0);
        $this->db->order_by("berita.id", "desc");
       	$query = $this->db->get();
       	$data['data'] = $query->result();
       	echo json_encode($data);
	}

	public function getHeaderImage()
	{
		$this->load->database();
        $this->db->select('*');
		$this->db->from('header_image');
		$this->db->where(array('header_image.isActive' => 1));
		$this->db->where(array('header_image.created_by' => $this->session->userdata('view_id')));
		$this->db->limit(3,0);
        $this->db->order_by("header_image.id", "desc");
       	$query = $this->db->get();
       	$data['data'] = $query->result();
       	echo json_encode($data);
	}

	public function profil($id){
		$this->load->database();
		$userid= $this->session->userdata('view_id');
		$slug = $this->uri->segment(1);
		$this->db->select('profil.id as id, profil.judul as judul, profil.isi as isi, profil.filepath as filepath, profil.status as status, profil.created_by as created_by');
		//$this->db->join('users','profil.created_by = users.id','INNER');
		$this->db->where(array('profil.id' => $id, 'profil.created_by' => $userid));
		$query = $this->db->get('profil')->row();

		if ($query->status == 'Sejarah') {
			$data['judul']    = $query->judul;
			$data['isi'] 	  = $query->isi;
			$data['status']   = $query->status;
			$this->load->view('visitors/page/profil',$data);
			
		} else {
			
			$data['judul']   = $query->judul;
			$data['isi']     = $query->isi;
			$data['filepath'] = $query->filepath;
			$data['status']  = $query->status;
			$this->load->view('visitors/page/profil',$data);
			
		}

	}

	private function delTempImg(){
        $ts = intval(substr(time(), 0, 7));
        $files = scandir('image_for_captcha/');
        
        foreach ($files as $key => $value) {
            if($value != '.' && $value != '..'){
                $filename = intval(substr($value, 0, 7));
                if( $filename < $ts) {
                    unlink('image_for_captcha/'.$value);
                }
            }
        }
	}
	
	public function refresh()
    {
        $config = array(
            'img_url' => base_url() . 'image_for_captcha/',
            'img_path' => 'image_for_captcha/',
            'img_height' => 45,
            'word_length' => 5,
            'img_width' => '100',
            'font_size' => 10
        );
        $captcha = create_captcha($config);
        $this->session->unset_userdata('valuecaptchaCode');
        $this->session->set_userdata('valuecaptchaCode', $captcha['word']);
        echo $captcha['image'];
    }


	public function detailBerita($id){
		$this->load->database();
		
		$this->db->select('berita.id,berita.tanggal,berita.judul,berita.isi,berita.imgpath,berita.isActive,users.nama');
		$this->db->where('berita.id',$id);
		$this->db->join('users','berita.created_by = users.id','INNER');
		$query = $this->db->get('berita')->row();

		$this->db->select('komentar.id_kom, komentar.id_berita, komentar.nama_kom, komentar.email_kom, komentar.isi_kom, komentar.created_at, komentar.isActive');
		$this->db->where('komentar.id_berita',$id);
		$this->db->where('komentar.isActive','1');
		$this->db->join('berita','komentar.id_berita = berita.id','INNER');
		$this->db->order_by('komentar.created_at', 'desc');
		$this->db->limit(5,0);
		$komen = $this->db->get('komentar')->result();
		
		$data['komentar'] = $komen;
		$data['id'] = $query->id;
		$data['imgpath'] = $query->imgpath;
		$data['judul']   = $query->judul;
		$data['author']  = $query->nama;
		$data['tanggal'] = $query->tanggal;
		$data['isi']     = $query->isi;

		// Cap
		// Delete Recent Image
        $this->delTempImg();

        if ($this->input->post('submit')) {
            $captcha_insert = $this->input->post('captcha');
            $contain_sess_captcha = $this->session->userdata('valuecaptchaCode');
            if ($captcha_insert === $contain_sess_captcha) {
                echo 'Success';
            } else {
                echo 'Failure';
            }
        }
        $config = array(
            'img_url' => base_url() . 'image_for_captcha/',
            'img_path' => 'image_for_captcha/',
            'img_height' => 45,
            'word_length' => 5,
            'img_width' => '100',
            'font_size' => 10
        );
        $captcha = create_captcha($config);
        $this->session->unset_userdata('valuecaptchaCode');
        $this->session->set_userdata('valuecaptchaCode', $captcha['word']);
        $data['captchaImg'] = $captcha['image'];
		$this->load->view('visitors/page/detail-berita',$data);
		
	}

	public function semua_berita()
	{
		$this->load->view('visitors/page/semua-berita');
	}

	public function kelurahan($id){
		$this->load->database();
		$this->db->select('detail_desa.id as id, detail_desa.isi as isi, detail_desa.idDesa as idDesa, data_desa.id as idDesa, data_desa.namadesa as namadesa');
		$this->db->where('detail_desa.idDesa',$id);
		$this->db->join('data_desa','detail_desa.idDesa = data_desa.id','INNER');
		$query = $this->db->get('detail_desa')->row();

		$data['namadesa'] = @$query->namadesa;
		$data['isi']      = @$query->isi;

		$this->load->view('visitors/page/kelurahan',$data);
	}

	public function all_kelurahan()
	{
		$this->load->view('visitors/page/semua-kelurahan');
	}

	public function semua_galeri()
	{
		$this->load->view('visitors/page/galeri');
	}

	public function detail_galeri($id){

		$data['id'] = $id;
		$this->load->view('visitors/page/detail-galeri',$data);
	}

	public function galeriimage()
	{
		$this->load->database();
		$this->db->select('detail_galeri.id as idDetail, detail_galeri.idgaleri, detail_galeri.imgpath, detail_galeri.isActive');
		$this->db->where(array('detail_galeri.created_by' => $this->session->userdata('view_id')));
		$this->db->where('detail_galeri.isActive', 1);
		$this->db->limit(3,$_POST['start']);
		$this->db->order_by("detail_galeri.id", "desc");
		$query = $this->db->get('detail_galeri');
       	$data['data'] = $query->result();
       	echo json_encode($data);
	}

	function getDetailGallery($id){
        $this->load->database();
        $this->db->select('*');
        $this->db->from('detail_galeri');
        $this->db->where(array(
            'detail_galeri.id'       => $id,
            'detail_galeri.isActive' => 1
        ));
        $q = $this->db->get();
        $data['data'] = $q->result();
        
        echo json_encode($data);
    }

    public function transparansi_desa()
	{
		$this->load->view('visitors/page/transparansi_desa');
	}

	public function detailtransparansi($id){
		$this->load->database();
		$this->db->select('*');
		$this->db->where('informasi.id',$id);
		$this->db->where('informasi.status','Transparansi Keuangan Desa');
		$this->db->where('informasi.isActive','1');
		$query = $this->db->get('informasi')->row();

		$data['judul']     = $query->judul;
		$data['isi']       = $query->isi;
		$data['filepath']  = $query->filepath;
		$this->load->view('visitors/page/detail_transparansi',$data);	
	}

	public function datapenerimabantuan()
	{
		$this->load->view('visitors/page/penerima_bantuan');
	}

	public function detailpenerimabantuan($id){
		$this->load->database();
		$this->db->select('*');
		$this->db->where('informasi.id',$id);
		$this->db->where('informasi.status','Data Penerima Bantuan');
		$this->db->where('informasi.isActive','1');
		$query = $this->db->get('informasi')->row();

		$data['judul']     = $query->judul;
		$data['isi']       = $query->isi;
		$data['filepath']  = $query->filepath;
		$this->load->view('visitors/page/detail_bantuan',$data);	
	}

	public function prestasidesa()
	{
		$this->load->view('visitors/page/prestasi_desa');
	}

	public function detailprestasidesa($id){
		$this->load->database();
		$this->db->select('*');
		$this->db->where('informasi.id',$id);
		$this->db->where('informasi.status','Prestasi Desa');
		$this->db->where('informasi.isActive','1');
		$query = $this->db->get('informasi')->row();

		$data['judul']     = $query->judul;
		$data['isi']       = $query->isi;
		$data['filepath']  = $query->filepath;
		$this->load->view('visitors/page/detail_prestasi',$data);	
	}

		public function sk_ppid()
	{
		$this->load->view('visitors/page/sk_ppid');
	}

	public function detailsk_ppid($id){
		$this->load->database();
		$this->db->select('*');
		$this->db->where('informasi.id',$id);
		$this->db->where('informasi.status','Surat Keputusan PPID');
		$this->db->where('informasi.isActive','1');
		$query = $this->db->get('informasi')->row();

		$data['judul']     = $query->judul;
		$data['isi']       = $query->isi;
		$data['filepath']  = $query->filepath;
		$this->load->view('visitors/page/detail_sk_ppid',$data);	
	}

	public function alurpermohonan()
	{
		$this->load->view('visitors/page/alurpermohonan');
	}

	public function alurkeberatan()
	{
		$this->load->view('visitors/page/alurkeberatan');
	}


	public function pemerintahan_desa()
	{
		$this->load->view('visitors/navbar/pemerintahan_desa');
	}

	public function pembangunan_desa()
	{
		$this->load->view('visitors/navbar/pembangunan_desa');
	}

	public function pemberdayaan_masyarakat()
		{
			$this->load->view('visitors/navbar/pemberdayaan_masyarakat');
		}

	public function pembinaan_masyarakat()
		{
			$this->load->view('visitors/navbar/pembinaan_masyarakat');
		}

	public function bum_desa()
		{
			$this->load->view('visitors/navbar/bum_desa');
		}

	public function pkk_desa()
		{
			$this->load->view('visitors/navbar/pkk_desa');
		}

	public function potensi_desa()
		{
			$this->load->view('visitors/navbar/potensi_desa');
		}

	// public function informasiberkala()
	// {

	// 	$this->load->database();
	// 	$this->db->select('ppid.filepath as filepath');
	// 	$this->db->where(array('ppid.created_by' => $this->session->userdata('view_id')));
	// 	$this->db->where('ppid.isActive','1');
	// 	$data['filepath']  = $this->db->get('ppid')->result();
	// 	$this->load->view('visitors/page/berkala',$data);
	// 	//var_dump($data['filepath']);
	// }
	public function informasiberkala()
	{

		$this->load->database();
		$this->db->select('ppid.filepath as filepath, ppid.id_kategori as id_kategori');
		$this->db->where(array('ppid.created_by' => $this->session->userdata('view_id')));
		$this->db->where('ppid.isActive','1');
		$ppids = $this->db->get('ppid')->result();

		$filepath = array();

		foreach ($ppids as $key => $value) {
			$id_kategori = $value->id_kategori;
			$filepath[$id_kategori] = $value->filepath;
		}
		
		$data = [
			'filepath' => $filepath,
		];

		$this->load->view('visitors/page/berkala',$data);
		//var_dump($data['filepath']);
	}

	public function peraturandesa()
	{
		$this->load->view('visitors/page/peraturandesa');
		//var_dump($data['filepath']);
	}

}
