<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include('application/controllers/auth/DefaultController.php');

class AdminController extends DefaultController {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct()
	{
		parent::__construct();
		$this->checkLogin();
	}

	public function index()
	{
		$this->load->view('users/page/home');
	}

	public function dashboardinfo()
	{
		//load berita
		$this->load->database();
		$this->db->select('*');
		$this->db->from('berita');
		if($this->session->userdata('role') !=1)
			$this->db->where(array('berita.isActive' => 1, 'berita.created_by' => $this->session->userdata('userid')));
		$berita = $this->db->get();
		$data['berita'] = $berita->result();

		//load agenda
		$this->load->database();
		$this->db->select('*');
		$this->db->from('agenda');
		if($this->session->userdata('role') !=1)
			$this->db->where(array('agenda.isActive' => 1, 'agenda.created_by' => $this->session->userdata('userid')));
		$agenda = $this->db->get();
		$data['agenda'] = $agenda->result();		

		//load pengumuman
		$this->load->database();
		$this->db->select('*');
		$this->db->from('pengumuman');
		if($this->session->userdata('role') !=1)
			$this->db->where(array('pengumuman.isActive' => 1, 'pengumuman.created_by' => $this->session->userdata('userid')));
		$agenda = $this->db->get();
		$data['pengumuman'] = $agenda->result();

		//load galeri
		$this->load->database();
		$this->db->select('*');
		$this->db->from('galeri');
		if($this->session->userdata('role') !=1)
			$this->db->where(array('galeri.isActive' => 1, 'galeri.created_by' => $this->session->userdata('userid')));
		$galeri = $this->db->get();
		$data['galeri'] = $galeri->result();

		echo json_encode($data);
	}

}
