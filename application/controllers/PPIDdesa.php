<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include('application/controllers/auth/DefaultController.php');

class PPIDdesa extends DefaultController {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     *      http://example.com/index.php/welcome
     *  - or -
     *      http://example.com/index.php/welcome/index
     *  - or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
     */
    public function __construct()
    {
        parent::__construct();
        $this->checkLogin();
    }

    public function index()
    {
        // $this->load->model('Model_ppid_desa_desa');
        // $data['kategori'] = $this->Model_ppid_desa->getKategori();
        $this->load->view('users/page/profilppid');
    }

    public function getData()
    {
        $this->load->database();
        $this->load->model('Model_ppid_desa');
        $list = $this->Model_ppid_desa->get_datatables();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $item) {
            $no++;
            $row = array();
            $row[]          = $no;
            $row[]          = $item->judul;
            $row[]          = $item->isi;
            // $row[]          = '<center><a class="btn btn-info btn-sm" href="$item->filepath" target="_blank" title="Download">Download <i class="fa fa-download"></i></a></center>';
            $row[]          = '<button class="btn btn-info btn-sm" onclick="detail('."'".$item->id."'".')" title="Detail"><i class="fa fa-sticky-note-o"></i></button> &nbsp;
            <button class="btn btn-warning btn-sm" title="Edit" onclick="update('."'".$item->id."'".')"><i class="fa fa-edit"></i></button>';

            $data[] = $row;
        }
        $output = array(
            "draw"            => $_POST['draw'],
            "recordsTotal"    => $this->Model_ppid_desa->count_all(),
            "recordsFiltered" => $this->Model_ppid_desa->count_filtered(),
            "data"            => $data,
        );
        echo json_encode($output);
    }

    public function check_judul(){
        $this->load->database();
        $this->db->where('judul',$_POST['judul']);
        $this->db->where('created_by',$this->session->userdata('userid'));
        $cek = $this->db->get('profil_ppid')->num_rows();
        echo $cek;
    }

    public function insertData()
    {
     $this->load->model('Model_ppid_desa');
     $this->load->database();

     $status = "";
     $msg = "";
     $file_element_name = 'file';
     $filepath = "";

     $config['upload_path']   = './upload_file/ppid_desa/';
     $config['allowed_types'] = 'gif|jpg|png|jpeg|pdf|doc|docx';
     $config['max_size']      = 8192;
     $config['encrypt_name']  = TRUE;

     $this->load->library('upload',$config);
     $this->upload->initialize($config);

     if(!$this->upload->do_upload($file_element_name))
     {
        $status = 'error';
        $msg = $this->upload->display_errors('', '');
    }
    else
    {
        $data = $this->upload->data();
        $c = base_url();
        $a = 'upload_file/ppid_desa/';
        $b = $data['file_name'];
        $filepath = $c.$a.$b;
        $data = array(
            'judul'         => $_POST['judul'],
            'isi'           => $_POST['isi'],
            'filepath'      => $filepath,
            'created_by'    => $this->session->userdata('userid'),
            'updated_by'    => $this->session->userdata('userid'),
            'created_at'    => mdate('%Y-%m-%d', now()),
            'updated_at'    => mdate('%Y-%m-%d', now()),
            'isActive'      => 1
        );
        $doupload = $this->Model_ppid_desa->insert_file($data);
        if($doupload )
        {
            $status = "success";
            $msg    = "File successfully uploaded";
        }
        else
        {
            unlink($data['full_path']);
            $status = "error";
            $msg    = "Something went wrong when saving the file, please try again.";
        }
    }
    @unlink($_FILES[$file_element_name]);

    echo json_encode(array('status' => $status, 'msg' => $msg));
}

private function get_by_id($id)
{
    $this->load->database();
    $this->db->where('id',$id);
    $query = $this->db->get('profil_ppid');
    return $query->row();
}

public function getById($id)
{
    $this->load->database();
    $this->db->select('profil_ppid.id,profil_ppid.judul,profil_ppid.isi,profil_ppid.filepath,profil_ppid.created_by,profil_ppid.isActive');
    $this->db->from('profil_ppid');
    $this->db->where('profil_ppid.id',$id);
    $q = $this->db->get();
    $data['data'] = $q->row();

    echo json_encode($data);
}

public function editData()
{
    $this->load->database();
    $this->load->model('Model_ppid_desa');
    $status            = "";
    $msg               = "";
    $file_element_name = 'file';
    $filepath          = "";
    $where = array(
        'id'    => $_POST['id']
    );

    if($_FILES["file"]["error"] == 4)
    {
        $data = array(
            'judul'           => $_POST['judul'],
            'isi'           => $_POST['isi'],
            'updated_at'    => mdate('%Y-%m-%d', now()),
            'updated_by'    => $this->session->userdata('userid') 
        );
        $update = $this->Model_ppid_desa->update_data($where,$data);
        if($update == true)
        {
            $status = "success";
            $msg    = "Success updated item";
        }
        else
        {
            $status = "error";
            $msg    = "Error updated item"; 
        }
    }
    else
    {
        $config['upload_path'] = './upload_file/ppid_desa/';
        $config['allowed_types'] = 'gif|jpg|png|jpeg';
        $config['max_size'] = 8192;
        $config['encrypt_name'] = TRUE;

        $this->upload->initialize($config);
        $this->load->library('upload',$config);

        if($this->upload->do_upload($file_element_name))
        {
            $data = $this->upload->data();
            $filepath = base_url().'upload_file/ppid_desa/'.$data['file_name'];
            $data = array(
                'judul'           => $_POST['judul'],
                'isi'           => $_POST['isi'],
                'filepath'      => $filepath,
                'updated_at'    => mdate('%Y-%m-%d', now()),
                'updated_by'    => $this->session->userdata('userid') 
            );
            $update = $this->Model_ppid_desa->update_data($where,$data);
            if($update == true)
            {
                $status = "success";
                $msg = "Success updated item";
            }
            else
            {
                unlink($data['full_path']);
                $status = "error";
                $msg = "Error updated item";
            }
        }
        @unlink($_FILES[$file_element_name]);
    }   
    echo json_encode(array('status' => $status, 'msg' => $msg));
}

public function delete($id)
{
    $this->load->database();
    $this->load->model('Model_ppid_desa');
    $status = "";
    $msg    = "";

    $where = array(
        'id'    => $_POST['id']
    );

    $data = array(
        'isActive'      => 0,
        'updated_at'    => mdate('%Y-%m-%d', now()),
        'updated_by'    => $this->session->userdata('userid') 
    );
    $update = $this->Model_ppid_desa->update_data($where,$data);
    if($update == true)
    {
        $status = "success";
        $msg    = "Success deleted item";
    }
    else
    {
        $status = "error";
        $msg    = "Error deleted item"; 
    }
    echo json_encode(array('status' => $status, 'msg' => $msg));
}

public function activate($id)
{
    $this->load->database();
    $this->load->model('Model_ppid_desa');
    $status = "";
    $msg    = "";

    $where = array(
        'id'    => $_POST['id']
    );

    $data = array(
        'isActive'      => 1,
        'updated_at'    => mdate('%Y-%m-%d', now()),
        'updated_by'    => $this->session->userdata('userid') 
    );
    $update = $this->Model_ppid_desa->update_data($where,$data);
    if($update == true)
    {
        $status = "success";
        $msg    = "Success activated item";
    }
    else
    {
        $status = "error";
        $msg    = "Error activated item";    
    }
    echo json_encode(array('status' => $status, 'msg' => $msg));
}

public function delete_data($id)
{
    $this->load->database();
    $this->load->model('Model_ppid_desa');
    $status = "";
    $msg = "";

    $where = array(
        'id'    => $_POST['id']
    );

    $update = $this->Model_ppid_desa->delete_data($where);
    if($update == true)
    {
        $status = "success";
        $msg = "Success deleted item";
    }
    else
    {
        $status = "error";
        $msg = "Error deleted item";    
    }
    echo json_encode(array('status' => $status, 'msg' => $msg));
}
}
