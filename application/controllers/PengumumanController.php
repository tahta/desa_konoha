<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include('application/controllers/auth/DefaultController.php');

class PengumumanController extends DefaultController {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct()
	{
		parent::__construct();
		$this->checkLogin();
	}

	public function index()
	{
		$this->load->view('users/page/pengumuman');
	}

    public function getData()
    {
        $this->load->database();
        $this->load->model('Model_pengumuman');
        $list = $this->Model_pengumuman->get_datatables();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $item) {
            $no++;
            $row = array();
            $row['no'] = $no;
            $row['id'] = $item->id;
            $row['judul'] = $item->judul;
            if($item->lampiran)
                $row['file'] = '<a class="btn btn-info btn-sm" title="Download File" href="'.$item->lampiran.'"><i class="fa fa-download"></i></a>';
            else if(!$item->lampiran)
                $row['file'] = 'not available';
            $row['created_by'] = $item->created_by;
            $row['updated_by'] = $item->updated_by;
            $row['created_at'] = $item->created_at;
            $row['updated_at'] = $item->updated_at;
            if($item->isActive == 1)
            {
                $row['isActive'] = 'Aktif';
                $row['action'] = '<button class="btn btn-warning btn-sm" title="Edit" onclick="update('."'".$item->id."'".')"><i class="fa fa-edit"></i></button> &nbsp;
            <button class="btn btn-danger btn-sm" title="Hapus" onclick="hapus('."'".$item->id."'".')"><i class="fa fa-trash-o"></i></button>';
            }
            else if($item->isActive == 0)
            {
                $row['isActive'] = 'Tidak Aktif';
                 if($this->session->userdata('role') ==1)
                {
                    $row['action'] = '<button class="btn btn-success btn-sm" onclick="activate('."'".$item->id."'".')" title="Aktifkan"><i class="fa fa-check"></i></button> &nbsp;';
                }
            }

            $data[] = $row;
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->Model_pengumuman->count_all(),
            "recordsFiltered" => $this->Model_pengumuman->count_filtered(),
            "data" => $data,
        );
        echo json_encode($output);
    }

    public function insertData()
    {
        $this->load->model('Model_pengumuman');
        $this->load->database();

        $status = "";
        $msg = "";
        $file_element_name = 'file';
        $filepath = "";

        $config['upload_path']   = './upload_file/pengumuman/';
        $config['allowed_types'] = 'gif|jpg|png|jpeg|pdf|doc|docx';
        $config['max_size']      = 8192;
        $config['encrypt_name']  = TRUE;

        $this->upload->initialize($config);
        $this->load->library('upload',$config);

        if(!$this->upload->do_upload($file_element_name))
        {
            $status = 'error';
            $msg = $this->upload->display_errors('', '');
        }
        else
        {
            $data = $this->upload->data();
            $c = base_url();
            $a = 'upload_file/pengumuman/';
            $b = $data['file_name'];
            $filepath = $c.$a.$b;
            $data = array(
                'judul'     => $_POST['judul'],
                'lampiran'  => $filepath,
                'created_by'=> $this->session->userdata('userid'),
                'updated_by'=> $this->session->userdata('userid'),
                'created_at'=> mdate('%Y-%m-%d', now()),
                'updated_at'=> mdate('%Y-%m-%d', now()),
                'isActive'  => 1
            );
            $doupload = $this->Model_pengumuman->insert_file($data);
            if($doupload )
            {
                $status = "success";
                $msg    = "File successfully uploaded";
            }
            else
            {
                unlink($data['full_path']);
                $status = "error";
                $msg    = "Something went wrong when saving the file, please try again.";
            }
        }
        @unlink($_FILES[$file_element_name]);

        echo json_encode(array('status' => $status, 'msg' => $msg));
    }

    public function getById($id)
    {
        $this->load->database();
        $this->db->select('*');
        $this->db->from('pengumuman');
        $this->db->where('pengumuman.id',$id);
        $q = $this->db->get();
        $data['data'] = $q->result();
        
        echo json_encode($data);
    }

    public function editData($id)
    {
        $this->load->database();
        $this->load->model('Model_pengumuman');
        $status            = "";
        $msg               = "";
        $file_element_name = 'file';
        $filepath          = "";
        $where = array(
            'id'    => $_POST['id']
        );
        
        if(!isset($_FILES[$file_element_name]))
        {
            $data = array(
                'judul'         => $_POST['judul'],
                'updated_at'    => mdate('%Y-%m-%d', now()),
                'updated_by'    => $this->session->userdata('userid') 
            );
            $update = $this->Model_pengumuman->update_data($where,$data);
            if($update == true)
            {
                $status = "success";
                $msg    = "Success updated item";
            }
            else
            {
                $status = "error";
                $msg    = "Error updated item";    
            }
        }
        else
        {
            $config['upload_path']   = './upload_file/pengumuman/';
            $config['allowed_types'] = 'gif|jpg|png|jpeg|pdf|doc|docx';
            $config['max_size']      = 8192;
            $config['encrypt_name']  = TRUE;


            $this->upload->initialize($config);
            $this->load->library('upload',$config);

            if($this->upload->do_upload($file_element_name))
            {
                $data = $this->upload->data();
                $c = base_url();
                $a = 'upload_file/pengumuman/';
                $b = $data['file_name'];
                $filepath = $c.$a.$b;
                $data = array(
                    'judul'         => $_POST['judul'],
                    'lampiran'      => $filepath,
                    'updated_at'    => mdate('%Y-%m-%d', now()),
                    'updated_by'    => $this->session->userdata('userid') 
                );
                $update = $this->Model_pengumuman->update_data($where,$data);
                if($update == true)
                {
                    $status = "success";
                    $msg    = "Success updated item";
                }
                else
                {
                    unlink($data['full_path']);
                    $status = "error";
                    $msg    = "Error updated item";
                }
            }
            @unlink($_FILES[$file_element_name]);
        }   
        echo json_encode(array('status' => $status, 'msg' => $msg));
    }

    public function delete($id)
    {
        $this->load->database();
        $this->load->model('Model_pengumuman');
        $status = "";
        $msg    = "";

        $where = array(
            'id'    => $_POST['id']
        );

        $data = array(
            'isActive'      => $_POST['isActive'],
            'updated_at'    => mdate('%Y-%m-%d', now()),
            'updated_by'    => $this->session->userdata('userid') 
        );
        $update = $this->Model_pengumuman->update_data($where,$data);
        if($update == true)
        {
            $status = "success";
            $msg = "Success updated item";
        }
        else
        {
            $status = "error";
            $msg = "Error updated item";    
        }
        echo json_encode(array('status' => $status, 'msg' => $msg));
    }
}