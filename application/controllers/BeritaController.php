<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include('application/controllers/auth/DefaultController.php');
date_default_timezone_set('Asia/Jakarta');

class BeritaController extends DefaultController {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     *      http://example.com/index.php/welcome
     *  - or -
     *      http://example.com/index.php/welcome/index
     *  - or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
     */
    public function __construct()
    {
        parent::__construct();
        $this->checkLogin();
    }

    public function index()
    {
        $this->load->model('Model_news');
        $data['kategori'] = $this->Model_news->getKategori();
        $this->load->view('users/page/berita',$data);
    }

    public function getData()
    {
        $this->load->database();
        $this->load->model('Model_news');
        $list = $this->Model_news->get_datatables();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $item) {
            $no++;
            $row = array();
            $row['no'] = $no;
            $row['id'] = $item->idberita;
            $row['tanggal'] = $item->tanggal;
            $row['judul'] = $item->judul;
            $row['isi'] = $item->isi;
            if($item->imgpath)
                $row['imgpath'] = 'available';
            else if(!$item->imgpath)
                $row['imgpath'] = 'not available';
            $row['created_by'] = $item->created_by;
            $row['updated_by'] = $item->updated_by;
            $row['created_at'] = $item->created_at;
            $row['updated_at'] = $item->updated_at;
            $row['nama'] = $item->author;
            if($item->isActive == 1)
            {
                $row['isActive'] = 'Aktif';
                if($this->session->userdata('role') ==1)
                {
                $row['action'] = '<button class="btn btn-info btn-sm" onclick="detail('."'".$item->idberita."'".')" title="Detail"><i class="fa fa-sticky-note-o"></i></button> &nbsp;
            <button class="btn btn-warning btn-sm" title="Edit" onclick="update('."'".$item->idberita."'".')"><i class="fa fa-edit"></i></button> &nbsp;
            <button class="btn btn-danger btn-sm" title="Verifikasi" onclick="hapus('."'".$item->idberita."'".')" style="background-color:#ff8843 !important; border-color:#ff8843 !important"><i class="fa fa-window-close"></i></button>';
                }else{
                    $row['action'] = '<button class="btn btn-info btn-sm" onclick="detail('."'".$item->idberita."'".')" title="Detail"><i class="fa fa-sticky-note-o"></i></button>';
                }
            }
            else if($item->isActive == 0)
            {
                $row['isActive'] = 'Tidak Aktif';
                if($this->session->userdata('role') ==1)
                {
                    $row['action'] = '<button class="btn btn-info btn-sm" onclick="detail('."'".$item->idberita."'".')" title="Detail"><i class="fa fa-sticky-note-o"></i></button> &nbsp;
            <button class="btn btn-success btn-sm" title="Verifikasi" onclick="activate('."'".$item->idberita."'".')"><i class="fa fa-check"></i></button>';
                } else {
                    $row['action'] = '<button class="btn btn-info btn-sm" onclick="detail('."'".$item->idberita."'".')" title="Detail"><i class="fa fa-sticky-note-o"></i></button> &nbsp;
            <button class="btn btn-warning btn-sm" title="Edit" onclick="update('."'".$item->idberita."'".')"><i class="fa fa-edit"></i></button> &nbsp;
            <button class="btn btn-danger btn-sm" title="Hapus" onclick="hapus_permanen('."'".$item->idberita."'".')"><i class="fa fa-trash-o"></i></button>';
                }
            }
            $row['nama_kategori'] = $item->nama_kategori;
            $data[] = $row;
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->Model_news->count_all(),
            "recordsFiltered" => $this->Model_news->count_filtered(),
            "data" => $data,
        );
        echo json_encode($output);
    }

    public function insertData()
    {
        $this->load->model('Model_news');
        $this->load->database();

        $status = "";
        $msg = "";
        $file_element_name = 'file';
        $imgpath = "";

        $config['upload_path'] = './upload_file/news_image/';
        $config['allowed_types'] = 'gif|jpg|png|jpeg';
        $config['max_size'] = 1024 * 8;
        $config['encrypt_name'] = TRUE;

        $this->upload->initialize($config);
        $this->load->library('upload',$config);

        if(!$this->upload->do_upload($file_element_name))
        {
            $status = 'error';
            $msg = $this->upload->display_errors('', '');
        }
        else
        {
            $data = $this->upload->data();
            $c = base_url();
            $a = 'upload_file/news_image/';
            $b = $data['file_name'];
            $imgpath = $c.$a.$b;
            $doupload = $this->Model_news->insert_file($_POST['tanggal'], $_POST['judul'], $_POST['isi'], $imgpath, $this->session->userdata('userid'), $this->session->userdata('userid'), mdate('%Y-%m-%d', now()), mdate('%Y-%m-%d', now()), 0, $_POST['nama_kategori']);
            if($doupload)
            {
                $status = "success";
                $msg = "File successfully uploaded";
            }
            else
            {
                unlink($data['full_path']);
                $status = "error";
                $msg = "Something went wrong when saving the file, please try again.";
            }
        }
        @unlink($_FILES[$file_element_name]);

        echo json_encode(array('status' => $status, 'msg' => $msg));
    }

    public function getById($id)
    {
        $this->load->database();
        $this->db->select('berita.id,berita.tanggal,berita.judul,berita.isi,berita.imgpath,berita.created_by,berita.isActive,berita.id_kategori,users.nama');
        $this->db->from('berita');
        $this->db->where('berita.id',$id);
        $this->db->join('users','berita.created_by = users.id','INNER');
        $q = $this->db->get();
        $data['data'] = $q->result();
        
        echo json_encode($data);
    }

    public function editData($id)
    {
        $this->load->database();
        $this->load->model('Model_news');
        $status = "";
        $msg = "";
        $file_element_name = 'file';
        $imgpath = "";
        $where = array(
            'id'    => $_POST['id']
        );
        
        if(!isset($_FILES[$file_element_name]))
        {
            $data = array(
                'tanggal'       => $_POST['tanggal'],
                'judul'         => $_POST['judul'],
                'isi'           => $_POST['isi'],
                'updated_at'    => mdate('%Y-%m-%d', now()),
                'updated_by'    => $this->session->userdata('userid'),
                'id_kategori'   => $_POST['nama_kategori2']
            );
            $update = $this->Model_news->update_data($where,$data);
            if($update == true)
            {
                $status = "success";
                $msg = "Success updated item";
            }
            else
            {
                $status = "error";
                $msg = "Error updated item";    
            }
        }
        else
        {
            $config['upload_path'] = './upload_file/news_image/';
            $config['allowed_types'] = 'gif|jpg|png|jpeg';
            $config['max_size'] = 1024 * 8;
            $config['encrypt_name'] = TRUE;

            $this->upload->initialize($config);
            $this->load->library('upload',$config);

            if($this->upload->do_upload($file_element_name))
            {
                $data = $this->upload->data();
                $imgpath = base_url().'upload_file/news_image/'.$data['file_name'];
                $data = array(
                    'tanggal'       => $_POST['tanggal'],
                    'judul'         => $_POST['judul'],
                    'isi'           => $_POST['isi'],
                    'imgpath'       => $imgpath,
                    'updated_at'    => mdate('%Y-%m-%d', now()),
                    'updated_by'    => $this->session->userdata('userid'),
                    'id_kategori'   => $_POST['nama_kategori2'] 
                );
                $update = $this->Model_news->update_data($where,$data);
                if($update == true)
                {
                    $status = "success";
                    $msg = "Success updated item";
                }
                else
                {
                    unlink($data['full_path']);
                    $status = "error";
                    $msg = "Error updated item";
                }
            }
            @unlink($_FILES[$file_element_name]);
        }   
        echo json_encode(array('status' => $status, 'msg' => $msg));
    }

    public function delete($id)
    {
        $this->load->database();
        $this->load->model('Model_news');
        $status = "";
        $msg = "";

        $where = array(
            'id'    => $_POST['id']
        );

        $data = array(
            'isActive'      => 0,
            'updated_at'    => mdate('%Y-%m-%d', now()),
            'updated_by'    => $this->session->userdata('userid') 
        );
        $update = $this->Model_news->update_data($where,$data);
        if($update == true)
        {
            $status = "success";
            $msg = "Success deleted item";
        }
        else
        {
            $status = "error";
            $msg = "Error deleted item";    
        }
        echo json_encode(array('status' => $status, 'msg' => $msg));
    }

    public function activate($id)
    {
        $this->load->database();
        $this->load->model('Model_news');
        $status = "";
        $msg = "";

        $where = array(
            'id'    => $_POST['id']
        );

        $data = array(
            'isActive'      => 1,
            'updated_at'    => mdate('%Y-%m-%d', now()),
            'updated_by'    => $this->session->userdata('userid') 
        );
        $update = $this->Model_news->update_data($where,$data);
        if($update == true)
        {
            $status = "success";
            $msg = "Success activated item";
        }
        else
        {
            $status = "error";
            $msg = "Error activated item";    
        }
        echo json_encode(array('status' => $status, 'msg' => $msg));
    }

    // public function addKomen()
    // {
    //     echo "tessssss";
    // }

    public function delete_data($id)
    {
        $this->load->database();
        $this->load->model('Model_news');
        $status = "";
        $msg = "";

        $where = array(
            'id'    => $_POST['id']
        );

        $update = $this->Model_news->delete_data($where);
        if($update == true)
        {
            $status = "success";
            $msg = "Success deleted item";
        }
        else
        {
            $status = "error";
            $msg = "Error deleted item";    
        }
        echo json_encode(array('status' => $status, 'msg' => $msg));
    }
}
