<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include('application/controllers/auth/DefaultController.php');
date_default_timezone_set('Asia/Jakarta');

class ResetPasswordController extends DefaultController {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     *      http://example.com/index.php/welcome
     *  - or -
     *      http://example.com/index.php/welcome/index
     *  - or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
     */
    public function __construct()
    {
        parent::__construct();
        $this->checkLogin();
    }

    public function ubahPassword()
    {
      $this->load->view('users/page/resetPassword');
  }

  public function updatePass()
  {
     $this->load->database();
     $update = $this->input->post();
     $id = $this->session->userdata('userid');
     $this->db->where('id',$id);
     $this->db->update('users',$update);
     $q = $this->db->get_where('users', array('id' => $id));

     echo json_encode($update);
 }
}
