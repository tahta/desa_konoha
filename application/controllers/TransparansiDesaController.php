<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include('application/controllers/auth/DefaultController.php');

class TransparansiDesaController extends DefaultController {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     *      http://example.com/index.php/welcome
     *  - or -
     *      http://example.com/index.php/welcome/index
     *  - or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
     */
    public function __construct()
    {
        parent::__construct();
        $this->checkLogin();
    }

    public function index()
    {
        $this->load->view('users/page/transparansi_desa');
    }

    public function getData()
    {
        $this->load->database();
        $this->load->model('Model_transparansi_desa');
        $list = $this->Model_transparansi_desa->get_datatables();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $item) {
            $no++;
            $row = array();
            $row['no']           = $no;
            $row['id']           = $item->idinformasi;
            $row['tanggal']      = $item->tanggal;
            $row['judul']        = $item->judul;
            $row['isi']          = $item->isi;
            if($item->filepath)
                $row['filepath'] = '<a class="btn btn-info btn-sm" title="Download File" href="'.$item->filepath.'" target="_blank"><i class="fa fa-download"></i></a>';
            else if(!$item->filepath)
                $row['filepath'] = 'not available';
            $row['created_by']   = $item->created_by;
            $row['updated_by']   = $item->updated_by;
            $row['created_at']   = $item->created_at;
            $row['updated_at']   = $item->updated_at;
            $row['nama']         = $item->author;
            if($item->isActive == 1)
            {
                $row['isActive'] = 'Aktif';
                if($this->session->userdata('role') ==1)
                {
                $row['action'] = '<button class="btn btn-info btn-sm" onclick="detail('."'".$item->idinformasi."'".')" title="Detail"><i class="fa fa-sticky-note-o"></i></button> &nbsp;
            <button class="btn btn-warning btn-sm" title="Edit" onclick="update('."'".$item->idinformasi."'".')"><i class="fa fa-edit"></i></button> &nbsp;
            <button class="btn btn-danger btn-sm" title="Verifikasi" onclick="hapus('."'".$item->idinformasi."'".')"><i class="fa fa-trash-o"></i></button>';
                }else{
                    $row['action'] = '<button class="btn btn-info btn-sm" onclick="detail('."'".$item->idinformasi."'".')" title="Detail"><i class="fa fa-sticky-note-o"></i></button>';
                }
            }
            else if($item->isActive == 0)
            {
                $row['isActive'] = 'Tidak Aktif';
                if($this->session->userdata('role') ==1)
                {
                    $row['action'] = '<button class="btn btn-info btn-sm" onclick="detail('."'".$item->idinformasi."'".')" title="Detail"><i class="fa fa-sticky-note-o"></i></button> &nbsp;
            <button class="btn btn-success btn-sm" title="Verifikasi" onclick="activate('."'".$item->idinformasi."'".')"><i class="fa fa-check"></i></button>';
                } else {
                    $row['action'] = '<button class="btn btn-info btn-sm" onclick="detail('."'".$item->idinformasi."'".')" title="Detail"><i class="fa fa-sticky-note-o"></i></button> &nbsp;
            <button class="btn btn-warning btn-sm" title="Edit" onclick="update('."'".$item->idinformasi."'".')"><i class="fa fa-edit"></i></button> &nbsp;
            <button class="btn btn-danger btn-sm" title="Hapus" onclick="hapus_permanen('."'".$item->idinformasi."'".')"><i class="fa fa-trash-o"></i></button>';
                }
            }
           

            $data[] = $row;
        }
        $output = array(
            "draw"            => $_POST['draw'],
            "recordsTotal"    => $this->Model_transparansi_desa->count_all(),
            "recordsFiltered" => $this->Model_transparansi_desa->count_filtered(),
            "data"            => $data,
        );
        echo json_encode($output);
    }

    public function insertData()
    {
        $this->load->model('Model_transparansi_desa');
        $this->load->database();

        $status            = "";
        $msg               = "";
        $file_element_name = 'file';
        $filepath           = "";

        $config['upload_path']   = './upload_file/informasi/';
        $config['allowed_types'] = 'gif|jpg|png|jpeg|pdf|doc|docx';
        $config['max_size']      = 8192;
        $config['encrypt_name']  = TRUE;

        $this->upload->initialize($config);
        $this->load->library('upload',$config);

        if(!$this->upload->do_upload($file_element_name))
        {
            $status = 'error';
            $msg    = $this->upload->display_errors('', '');
        }
        else
        {
            $data = $this->upload->data();
            $c = base_url();
            $a = 'upload_file/informasi/';
            $b = $data['file_name'];
            $filepath  = $c.$a.$b;
            $doupload  = $this->Model_transparansi_desa->insert_file($_POST['tanggal'], $_POST['judul'], $_POST['isi'], $filepath, $this->session->userdata('userid'), $this->session->userdata('userid'), mdate('%Y-%m-%d', now()), mdate('%Y-%m-%d', now()), 0, 'Transparansi Keuangan Desa');
            if($doupload )
            {
                $status = "success";
                $msg    = "File successfully uploaded";
            }
            else
            {
                unlink($data['full_path']);
                $status = "error";
                $msg    = "Something went wrong when saving the file, please try again.";
            }
        }
        @unlink($_FILES[$file_element_name]);

        echo json_encode(array('status' => $status, 'msg' => $msg));
    }

    public function getById($id)
    {
        $this->load->database();
        $this->db->select('informasi.id,informasi.tanggal,informasi.judul,informasi.isi,informasi.filepath,informasi.created_by,informasi.isActive,users.nama');
        $this->db->from('informasi');
        $this->db->where('informasi.id',$id);
        $this->db->join('users','informasi.created_by = users.id','INNER');
        $q = $this->db->get();
        $data['data'] = $q->result();
        
        echo json_encode($data);
    }

    public function editData($id)
    {
        $this->load->database();
        $this->load->model('Model_transparansi_desa');
        $status            = "";
        $msg               = "";
        $file_element_name = 'file';
        $filepath           = "";
        $where = array(
            'id'    => $_POST['id']
        );
        
        if(!isset($_FILES[$file_element_name]))
        {
            $data = array(
                'tanggal'       => $_POST['tanggal'],
                'judul'         => $_POST['judul'],
                'isi'           => $_POST['isi'],
                'updated_at'    => mdate('%Y-%m-%d', now()),
                'updated_by'    => $this->session->userdata('userid'), 
                'status'        => 'Transparansi Keuangan Desa' 
            );
            $update = $this->Model_transparansi_desa->update_data($where,$data);
            if($update == true)
            {
                $status = "success";
                $msg    = "Success updated item";
            }
            else
            {
                $status = "error";
                $msg    = "Error updated item"; 
            }
        }
        else
        {
            $config['upload_path'] = './upload_file/informasi/';
            $config['allowed_types'] = 'gif|jpg|png|jpeg|pdf|doc|docx';
            $config['max_size'] = 8192;
            $config['encrypt_name'] = TRUE;

            $this->upload->initialize($config);
            $this->load->library('upload',$config);

            if($this->upload->do_upload($file_element_name))
            {
                $data = $this->upload->data();
                $filepath = base_url().'upload_file/informasi/'.$data['file_name'];
                $data = array(
                    'tanggal'       => $_POST['tanggal'],
                    'judul'         => $_POST['judul'],
                    'isi'           => $_POST['isi'],
                    'filepath'      => $filepath,
                    'updated_at'    => mdate('%Y-%m-%d', now()),
                    'updated_by'    => $this->session->userdata('userid'),
                    'status'        => 'Transparansi Keuangan Desa' 
                );
                $update = $this->Model_transparansi_desa->update_data($where,$data);
                if($update == true)
                {
                    $status = "success";
                    $msg = "Success updated item";
                }
                else
                {
                    unlink($data['full_path']);
                    $status = "error";
                    $msg = "Error updated item";
                }
            }
            @unlink($_FILES[$file_element_name]);
        }   
        echo json_encode(array('status' => $status, 'msg' => $msg));
    }

    public function delete($id)
    {
        $this->load->database();
        $this->load->model('Model_transparansi_desa');
        $status = "";
        $msg    = "";

        $where = array(
            'id'    => $_POST['id']
        );

        $data = array(
            'isActive'      => 0,
            'updated_at'    => mdate('%Y-%m-%d', now()),
            'updated_by'    => $this->session->userdata('userid') 
        );
        $update = $this->Model_transparansi_desa->update_data($where,$data);
        if($update == true)
        {
            $status = "success";
            $msg    = "Success deleted item";
        }
        else
        {
            $status = "error";
            $msg    = "Error deleted item"; 
        }
        echo json_encode(array('status' => $status, 'msg' => $msg));
    }

    public function activate($id)
    {
        $this->load->database();
        $this->load->model('Model_transparansi_desa');
        $status = "";
        $msg    = "";

        $where = array(
            'id'    => $_POST['id']
        );

        $data = array(
            'isActive'      => 1,
            'updated_at'    => mdate('%Y-%m-%d', now()),
            'updated_by'    => $this->session->userdata('userid') 
        );
        $update = $this->Model_transparansi_desa->update_data($where,$data);
        if($update == true)
        {
            $status = "success";
            $msg    = "Success activated item";
        }
        else
        {
            $status = "error";
            $msg    = "Error activated item";    
        }
        echo json_encode(array('status' => $status, 'msg' => $msg));
    }

    public function delete_data($id)
    {
        $this->load->database();
        $this->load->model('Model_transparansi_desa');
        $status = "";
        $msg = "";

        $where = array(
            'id'    => $_POST['id']
        );

        $update = $this->Model_transparansi_desa->delete_data($where);
        if($update == true)
        {
            $status = "success";
            $msg = "Success deleted item";
        }
        else
        {
            $status = "error";
            $msg = "Error deleted item";    
        }
        echo json_encode(array('status' => $status, 'msg' => $msg));
    }
}
