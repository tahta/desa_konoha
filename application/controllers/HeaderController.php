<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include('application/controllers/auth/DefaultController.php');

class HeaderController extends DefaultController {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct()
	{
		parent::__construct();
		$this->checkLogin();
	}

	public function index()
	{
		$this->load->view('users/page/gambar-header');
	}

	public function getData()
	{
        $this->load->database();
        $this->load->model('Model_header');
        $list = $this->Model_header->get_datatables();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $item) {
            $no++;
            $row = array();
            $row['no'] = $no;
            $row['id'] = $item->idheader;
            $row['judul'] = $item->judul;
            $row['created_by'] = $item->created_by;
            $row['updated_by'] = $item->updated_by;
            $row['created_at'] = $item->created_at;
            $row['updated_at'] = $item->updated_at;
            $row['author'] = $item->author;
            if($item->isActive == 1)
            {
                $row['isActive'] = 'Aktif';
                $row['action'] = '<button class="btn btn-warning btn-sm" title="Edit" onclick="update('."'".$item->idheader."'".')"><i class="fa fa-edit"></i></button> &nbsp;
            <button class="btn btn-danger btn-sm" title="Hapus" onclick="hapus('."'".$item->idheader."'".')"><i class="fa fa-trash-o"></i></button> &nbsp;<a class="btn btn-info btn-sm" title="Download File" href="'.$item->imgpath.'" target=_blank><i class="fa fa-download"></i></a>';
            }
            else if($item->isActive == 0)
            {
                $row['isActive'] = 'Tidak Aktif';
                 if($this->session->userdata('role') ==1)
                {
                    $row['action'] = '<button class="btn btn-success btn-sm" onclick="activate('."'".$item->idheader."'".')" title="Aktifkan"><i class="fa fa-check"></i></button> &nbsp;';
                }
            }

            $data[] = $row;
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->Model_header->count_all(),
            "recordsFiltered" => $this->Model_header->count_filtered(),
            "data" => $data,
        );
        echo json_encode($output);
	}

	public function insertData()
	{
		$this->load->model('Model_header');
		$this->load->database();

		$status = "";
    	$msg = "";
    	$file_element_name = 'file';
    	$filepath = "";

    	$config['upload_path'] = './upload_file/header/';
    	$config['allowed_types'] = 'gif|jpg|png|jpeg';
    	$config['max_size'] = 8192;
    	$config['encrypt_name'] = TRUE;

    	$this->upload->initialize($config);
    	$this->load->library('upload',$config);

    	if(!$this->upload->do_upload($file_element_name))
    	{
    		$status = 'error';
    		$msg = $this->upload->display_errors('', '');
    	}
    	else
    	{
    		$data = $this->upload->data();
    		$c = base_url();
    		$a = 'upload_file/header/';
    		$b = $data['file_name'];
    		$filepath = $c.$a.$b;
    		$data = array(
    			'judul'		=> $_POST['judul'],
    			'imgpath'	=> $filepath,
    			'created_by'=> $this->session->userdata('userid'),
    			'updated_by'=> $this->session->userdata('userid'),
    			'created_at'=> mdate('%Y-%m-%d', now()),
    			'updated_at'=> mdate('%Y-%m-%d', now()),
    			'isActive' => 1
    		);
    		$doupload = $this->Model_header->insert_file($data);
    		if($doupload )
            {
                $status = "success";
                $msg = "File successfully uploaded";
            }
            else
            {
                unlink($data['full_path']);
                $status = "error";
                $msg = "Something went wrong when saving the file, please try again.";
            }
    	}
    	@unlink($_FILES[$file_element_name]);

    	echo json_encode(array('status' => $status, 'msg' => $msg));
	}

	public function getById($id)
	{
		$this->load->database();
		$this->db->select('*');
		$this->db->from('header_image');
		$this->db->where('header_image.id',$id);
		$q = $this->db->get();
		$data['data'] = $q->result();
		
		echo json_encode($data);
	}

	public function editData($id)
	{
		$this->load->database();
		$this->load->model('Model_header');
		$status = "";
    	$msg = "";
    	$file_element_name = 'file';
    	$filepath = "";
       	$where = array(
    		'id'	=> $_POST['id']
    	);
    	
    	if(!isset($_FILES[$file_element_name]))
    	{
    		$data = array(
    			'judul'		=> $_POST['judul'],
    			'updated_at' 	=> mdate('%Y-%m-%d', now()),
    			'updated_by'	=> $this->session->userdata('userid') 
    		);
    		$update = $this->Model_header->update_data($where,$data);
    		if($update == true)
    		{
    			$status = "success";
    			$msg = "Success updated item";
    		}
    		else
    		{
    			$status = "error";
    			$msg = "Error updated item";	
    		}
    	}
    	else
    	{
    		$config['upload_path'] = './upload_file/header/';
    		$config['allowed_types'] = 'gif|jpg|png|jpeg';
    		$config['max_size'] = 8192;
    		$config['encrypt_name'] = TRUE;


	    	$this->upload->initialize($config);
	    	$this->load->library('upload',$config);

	    	if($this->upload->do_upload($file_element_name))
	    	{
	    		$data = $this->upload->data();
    			$c = base_url();
    			$a = 'upload_file/header/';
    			$b = $data['file_name'];
    			$filepath = $c.$a.$b;
				$data = array(
    				'judul'		=> $_POST['judul'],
	    			'imgpath'		=> $filepath,
	    			'updated_at' 	=> mdate('%Y-%m-%d', now()),
	    			'updated_by'	=> $this->session->userdata('userid') 
    			);
    			$update = $this->Model_header->update_data($where,$data);
	    		if($update == true)
	            {
	                $status = "success";
    				$msg = "Success updated item";
	            }
	            else
	            {
	                unlink($data['full_path']);
	                $status = "error";
    				$msg = "Error updated item";
	            }
		    }
		    @unlink($_FILES[$file_element_name]);
    	}	
    	echo json_encode(array('status' => $status, 'msg' => $msg));
	}

	public function delete($id)
	{
		$this->load->database();
		$this->load->model('Model_header');
		$status = "";
    	$msg = "";

    	$where = array(
    		'id'	=> $_POST['id']
    	);

    	$data = array(
	    	'isActive' 		=> $_POST['isActive'],
	    	'updated_at' 	=> mdate('%Y-%m-%d', now()),
	    	'updated_by'	=> $this->session->userdata('userid') 
    	);
    	$update = $this->Model_header->update_data($where,$data);
    	if($update == true)
    	{
    		$status = "success";
    		$msg = "Success updated item";
    	}
    	else
    	{
    		$status = "error";
    		$msg = "Error updated item";	
    	}
    	echo json_encode(array('status' => $status, 'msg' => $msg));
	}
}