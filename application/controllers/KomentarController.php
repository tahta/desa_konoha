<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class KomentarController extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct()
	{
		parent::__construct();
		$this->load->model('Model_komentar');
		$this->load->helper('url');
        $this->load->library('session');
        $this->load->helper('captcha');
	}


    private function delTempImg(){
        $ts = intval(substr(time(), 0, 7));
        $files = scandir('image_for_captcha/');
        
        foreach ($files as $key => $value) {
            if($value != '.' && $value != '..'){
                $filename = intval(substr($value, 0, 7));
                if( $filename < $ts) {
                    unlink('image_for_captcha/'.$value);
                }
            }
        }
    }

    // 
    
    // public function refresh()
    // {
    //     $config = array(
    //         'img_url' => base_url() . 'image_for_captcha/',
    //         'img_path' => 'image_for_captcha/',
    //         'img_height' => 45,
    //         'word_length' => 5,
    //         'img_width' => '100',
    //         'font_size' => 10
    //     );
    //     $captcha = create_captcha($config);
    //     $this->session->unset_userdata('valuecaptchaCode');
    //     $this->session->set_userdata('valuecaptchaCode', $captcha['word']);
    //     echo $captcha['image'];
    // }

	public function addKomen()
    {

        if ($this->input->post('captcha')) {
            $captcha_insert = $this->input->post('captcha');
            $contain_sess_captcha = $this->session->userdata('valuecaptchaCode');
            if ($captcha_insert === $contain_sess_captcha) {
                $slug = $this->uri->segment(1);
                $idberita = $this->uri->segment(4);
                $nama = $this->input->post('nama');
                $email = $this->input->post('email');
                $isi = $this->input->post('isi');
                $data=$this->Model_komentar->input_data($idberita,$nama,$email,$isi);
                redirect($slug.'/berita/detail-berita/'.$idberita);
            } 
            else {
                echo "Captcha Salah";;
            }
        }

    }

    public function daftarKomentar()
    {
        $this->load->view('users/page/daftar_komentar');
    }

    public function getData()
    {
        $this->load->database();
        $this->load->model('Model_komentar');
        $list = $this->Model_komentar->get_datatables();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $item) {
            $no++;
            $row = array();
            $row['no']          = $no;
            $row['judul']       = $item->judul;
            $row['nama_kom']    = $item->nama_kom;
            $row['email_kom']   = $item->email_kom;
            $row['isi_kom']     = $item->isi_kom;
            if ($item->isActive == 1) {
            	$row['action']  = '<button class="btn btn-danger btn-sm" title="Edit" onclick="tolak('."'".$item->id_kom."'".')"><i class="fa fa-times"></i></button>';
            } elseif ($item->isActive == 0) {
            	$row['action']  = '<button class="btn btn-success btn-sm" title="Edit" onclick="terima('."'".$item->id_kom."'".')"><i class="fa fa-check"></i></button>';
            }

            $data[] = $row;
        }
        $output = array(
            "draw"            => $_POST['draw'],
            "recordsTotal"    => $this->Model_komentar->count_all(),
            "recordsFiltered" => $this->Model_komentar->count_filtered(),
            "data"            => $data,
        );
        echo json_encode($output);
    }

    public function getById($id_kom)
	{
		$this->load->database();
		$this->db->select('*');
		$this->db->from('komentar');
		$this->db->where('komentar.id_kom',$id_kom);
		$q = $this->db->get();
		$data['data'] = $q->result();
		
		echo json_encode($data);
	}

	public function terima($id_kom)
	{
		$this->load->database();
		$this->load->model('Model_komentar');
		$status = "";
    	$msg = "";

    	$where = array(
    		'id_kom'	=> $_POST['id_kom']
    	);

    	$data = array(
	    	'isActive'  => 1
    	);
    	$update = $this->Model_komentar->update_data($where,$data);
    	if($update == true)
    	{
    		$status = "success";
    		$msg = "Success deleted item";
    	}
    	else
    	{
    		$status = "error";
    		$msg = "Error deleted item";	
    	}
    	echo json_encode(array('status' => $status, 'msg' => $msg));
	}

	public function tolak($id_kom)
	{
		$this->load->database();
		$this->load->model('Model_komentar');
		$status = "";
    	$msg = "";

    	$where = array(
    		'id_kom'	=> $_POST['id_kom']
    	);

    	$data = array(
	    	'isActive'  => 0
    	);
    	$update = $this->Model_komentar->update_data($where,$data);
    	if($update == true)
    	{
    		$status = "success";
    		$msg = "Success deleted item";
    	}
    	else
    	{
    		$status = "error";
    		$msg = "Error deleted item";	
    	}
    	echo json_encode(array('status' => $status, 'msg' => $msg));
	}

}
