<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include('application/controllers/auth/DefaultController.php');


class GaleriController extends DefaultController {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct()
	{
		parent::__construct();
		$this->checkLogin();
	}

	public function index()
	{
		$this->load->view('users/page/galeri');
	}

	public function getData()
	{
		$this->load->database();
        $this->load->model('Model_galeri');
        $list = $this->Model_galeri->get_datatables();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $item) {
            $no++;
            $row = array();
            $row['no'] = $no;
            $row['id'] = $item->id;
            $row['tanggal'] = $item->tanggal;
            $row['judul'] = $item->judul;
            $row['created_by'] = $item->created_by;
            $row['updated_by'] = $item->updated_by;
            $row['created_at'] = $item->created_at;
            $row['updated_at'] = $item->updated_at;
            if($item->isActive == 1)
            {
                $row['isActive'] = 'Aktif';
                 $row['action'] = '<button class="btn btn-warning btn-sm" title="Edit" onclick="update('."'".$item->id."'".')"><i class="fa fa-edit"></i></button> &nbsp; 
            <button class="btn btn-info btn-sm" title="Preview Galeri" onclick="preview('."'".$item->id."'".')"><i class="fa fa-image"></i></button> &nbsp;
            <button class="btn btn-danger btn-sm" title="Hapus" onclick="hapus('."'".$item->id."'".')"><i class="fa fa-trash-o"></i></button> &nbsp';
            }
            else if($item->isActive == 0)
            {
                $row['isActive'] = 'Tidak Aktif';
                if($this->session->userdata('role') == 1)
                {
                    $row['action'] = '
            <button class="btn btn-success btn-sm" title="Aktifkan" onclick="activate('."'".$item->id."'".')"><i class="fa fa-check"></i></button> &nbsp';
                }
            }


            $data[] = $row;
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->Model_galeri->count_all(),
            "recordsFiltered" => $this->Model_galeri->count_filtered(),
            "data" => $data,
        );
        echo json_encode($output);
	}

    function insertData()
    {
        //echo "yes";
        $this->load->model('Model_galeri');
        $this->load->database();

        $status = "";
        $msg = "";
        $file_element_name = 'files';
        $filepath = "";
        $number_of_files_uploaded = count($_FILES['files']['name']);

        $data = array(
                'judul'     => $_POST['judul'],
                'tanggal'   => $_POST['tanggal'],
                'created_by'=> $this->session->userdata('userid'),
                'updated_by'=> $this->session->userdata('userid'),
                'created_at'=> mdate('%Y-%m-%d', now()),
                'updated_at'=> mdate('%Y-%m-%d', now()),
                'isActive' => 1
            );

        $galeriid = $this->Model_galeri->insert_gallery($data);

        for($i=0;$i<$number_of_files_uploaded;$i++)
        {
            $_FILES['userfile']['name']     = $_FILES['files']['name'][$i];
            $_FILES['userfile']['type']     = $_FILES['files']['type'][$i];
            $_FILES['userfile']['tmp_name'] = $_FILES['files']['tmp_name'][$i];
            $_FILES['userfile']['error']    = $_FILES['files']['error'][$i];
            $_FILES['userfile']['size']     = $_FILES['files']['size'][$i];

            $config['upload_path'] = './upload_file/galeri/';
            $config['allowed_types'] = 'jpg|png|jpeg';
            $config['max_size'] = 2048;
            $config['encrypt_name'] = TRUE;

            $this->upload->initialize($config);
            $this->load->library('upload',$config);
            if(!$this->upload->do_upload())
            {
                $status = 'error';
                $msg = $this->upload->display_errors('', '');
            }
            else 
            {
                $data = $this->upload->data();
                $c = base_url();
                $a = 'upload_file/galeri/';
                $b = $data['file_name'];
                $filepath = $c.$a.$b;
                $data = array(
                    'idgaleri'   => $galeriid,
                    'imgpath'  => $filepath,
                    'created_by'=> $this->session->userdata('userid'),
                    'updated_by'=> $this->session->userdata('userid'),
                    'created_at'=> mdate('%Y-%m-%d', now()),
                    'updated_at'=> mdate('%Y-%m-%d', now()),
                    'isActive' => 1
                );
                $doupload = $this->Model_galeri->insert_detail_gallery($data);
                if($doupload )
                {
                    $status = "success";
                    $msg = "File successfully uploaded";
                }
                else
                {
                    unlink($data['full_path']);
                    $status = "error";
                    $msg = "Something went wrong when saving the file, please try again.";
                }
            }
        }
        echo json_encode(array('status' => $status, 'msg' => $msg));
    }

    function getGallery($id)
    {
        $this->load->database();
        $this->db->select('*');
        $this->db->from('galeri');
        $this->db->where(array('galeri.id' => $id));
        $q = $this->db->get();
        $data['data'] = $q->result();

        echo json_encode($data);
    }

    function editGallery()
    {
        $this->load->database();
        $this->load->model('Model_galeri');
        $where = array(
            'id'    => $_POST['id']
        );

        $data = array(
                'tanggal'   => $_POST['tanggal'],
                'judul'     => $_POST['judul'],
                'updated_at'    => mdate('%Y-%m-%d', now()),
                'updated_by'    => $this->session->userdata('userid') 
        );
        $update = $this->Model_galeri->update_gallery($where,$data);
        if($update == true)
        {
            $status = "success";
            $msg = "Success updated item";
        }
        else
        {
            $status = "error";
            $msg = "Error updated item";    
        }
        echo json_encode(array('status' => $status, 'msg' => $msg));
    }

    function getDetailGallery($id){
        $this->load->database();
        $this->db->select('*');
        $this->db->from('detail_galeri');
        $this->db->where(array(
            'detail_galeri.idgaleri' => $id,
            'detail_galeri.isActive' => 1
        ));
        $q = $this->db->get();
        $data['data'] = $q->result();
        
        echo json_encode($data);
    }

    public function delete($id)
    {
        $this->load->database();
        $this->load->model('Model_galeri');
        $status = "";
        $msg = "";

        $where = array(
            'id'    => $_POST['id']
        );

        $data = array(
            'isActive'      => $_POST['isActive'],
            'updated_at'    => mdate('%Y-%m-%d', now()),
            'updated_by'    => $this->session->userdata('userid') 
        );
        $update = $this->Model_galeri->update_gallery($where,$data);

        $wheredetail = array(
            'idgaleri' => $_POST['id']
        );

        $datadetail = array(
            'isActive'      => $_POST['isActive'],
            'updated_at'    => mdate('%Y-%m-%d', now()),
            'updated_by'    => $this->session->userdata('userid') 
        );

        $updatedetail = $this->Model_galeri->update_detail_gallery($wheredetail,$data);

        if($update == true && $updatedetail == true)
        {
            $status = "success";
            $msg = "Success updated item";
        }
        else
        {
            $status = "error";
            $msg = "Error updated item";    
        }
        echo json_encode(array('status' => $status, 'msg' => $msg));
    }   
}