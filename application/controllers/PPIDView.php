<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class PPIDView extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct()
	{
		parent::__construct();
		$this->load->library('session');
		// $this->load->helper('captcha');
		$this->load->database();
		$this->setSession();
	}

	private function setSession(){
		$slug = $this->uri->segment(1);
		if(isset($slug)){
			$this->load->database();
			$this->db->select('id, desa, alamat, email, no_telp, peta_desa, slug');
			$this->db->where('slug', $slug);
			$this->db->from('users');
			$query = $this->db->get()->result();
			if($query){
				$this->session->set_userdata('view_id', $query[0]->id);
				$this->session->set_userdata('view_desa', $query[0]->desa);
				$this->session->set_userdata('view_alamat', $query[0]->alamat);
				$this->session->set_userdata('view_email', $query[0]->email);
				$this->session->set_userdata('view_no_telp', $query[0]->no_telp);
				$this->session->set_userdata('view_peta_desa', $query[0]->peta_desa);
				$this->session->set_userdata('view_slug', $query[0]->slug);

			}
		}
	}

	public function profil_ppid($id){
		$this->load->database();
		$userid= $this->session->userdata('view_id');
		$slug = $this->uri->segment(1);
		$this->db->select('profil_ppid.id as id, profil_ppid.judul as judul, profil_ppid.isi as isi, profil_ppid.filepath as filepath, profil_ppid.created_by as created_by');
		$this->db->where(array('profil_ppid.judul' => $id, 'profil_ppid.created_by' => $userid));
		$query = $this->db->get('profil_ppid')->row();

		if (empty($query->judul)) {
			$data['judul']   = 'Belum Diisi';
			$data['isi']     = '';
			$data['filepath'] = '';
			$this->load->view('visitors/ppid/profil_ppid',$data);
		}
		else{
			$data['userid']     = $userid;
			$data['judul']   = $query->judul;
			$data['isi']     = $query->isi;
			$data['filepath'] = $query->filepath;
			$this->load->view('visitors/ppid/profil_ppid',$data);
		}
	}

	public function diponline()
	{
		$userid= $this->session->userdata('view_id');
		$this->db->select('*');
		$this->db->where(array('created_by' => $userid));
		$datas = $this->db->get('dip_online')->result();
		$data = [ 'datas' => $datas ];
		$this->load->view('visitors/ppid/diponline', $data);
	}
}
