<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include('application/controllers/auth/DefaultController.php');

class KontakController extends DefaultController {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     *      http://example.com/index.php/welcome
     *  - or -
     *      http://example.com/index.php/welcome/index
     *  - or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
     */
    public function __construct()
    {
        parent::__construct();
        $this->checkLogin();
    }

    public function index()
    {
        $this->load->view('users/page/kontak');
    }

    public function getData()
    {
        $this->load->database();
        $this->load->model('Model_kontak');
        $list = $this->Model_kontak->get_datatables();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $item) {
            $no++;
            $row = array();
            $row['no']                = $no;
            $row['id']                = $item->id;
            $row['desa']              = $item->desa;
            $row['alamat']            = $item->alamat;
            $row['email']             = $item->email;
            $row['no_telp']           = $item->no_telp;
            $row['action']            = '<button class="btn btn-warning btn-sm" title="Edit" onclick="update('."'".$item->id."'".')"><i class="fa fa-edit"></i></button>';
           
            $data[] = $row;
        }
        $output = array(
            "draw"            => $_POST['draw'],
            "recordsTotal"    => $this->Model_kontak->count_all(),
            "recordsFiltered" => $this->Model_kontak->count_filtered(),
            "data"            => $data,
        );
        echo json_encode($output);
    }

    public function getById($id)
    {
        $this->load->database();
        $this->db->select('users.id as id, users.desa as desa, users.alamat as alamat, users.email as email, users.no_telp as no_telp');
        $this->db->from('users');
        $this->db->where('users.id',$id);
        $q = $this->db->get();
        $data['data'] = $q->result();
        
        echo json_encode($data);
    }

    public function editData($id)
    {
        $this->load->database();
        $insert = $this->input->post();
        $this->db->where('id',$id);
        $this->db->update('users',$insert);
        $q = $this->db->get_where('users', array('id' => $id));

        echo json_encode($insert);
    }

}
