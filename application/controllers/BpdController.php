<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include('application/controllers/auth/DefaultController.php');

class BpdController extends DefaultController {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     *      http://example.com/index.php/welcome
     *  - or -
     *      http://example.com/index.php/welcome/index
     *  - or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
     */
    public function __construct()
    {
        parent::__construct();
        $this->checkLogin();
    }

    public function index()
    {
        $this->load->view('users/page/bpd');
    }

    public function getData()
    {
        $this->load->database();
        $this->load->model('Model_bpd');
        $list = $this->Model_bpd->get_datatables();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $item) {
            $no++;
            $row = array();
            $row['no']          = $no;
            $row['id']          = $item->id;
            $row['judul']       = $item->judul;
            $row['isi']         = $item->isi;
            $row['filepath']    = $item->filepath;
            $row['isActive']    = 'Aktif';
            $row['action']      = '<button class="btn btn-info btn-sm" onclick="detail('."'".$item->id."'".')" title="Detail"><i class="fa fa-sticky-note-o"></i></button> &nbsp;
            <button class="btn btn-warning btn-sm" title="Edit" onclick="update('."'".$item->id."'".')"><i class="fa fa-edit"></i></button>';

            $data[] = $row;
        }
        $output = array(
            "draw"            => $_POST['draw'],
            "recordsTotal"    => $this->Model_bpd->count_all(),
            "recordsFiltered" => $this->Model_bpd->count_filtered(),
            "data"            => $data,
        );
        echo json_encode($output);
    }

    public function getById($id)
    {
        $this->load->database();
        $this->db->select('profil.id,profil.judul, profil.isi, profil.filepath,profil.created_by,profil.isActive,users.nama');
        $this->db->from('profil');
        $this->db->where('profil.id',$id);
        $this->db->join('users','profil.created_by = users.id','INNER');
        $q = $this->db->get();
        $data['data'] = $q->result();
        
        echo json_encode($data);
    }

    public function editData($id)
    {
        $this->load->database();
        $this->load->model('Model_bpd');
        $status            = "";
        $msg               = "";
        $file_element_name = 'file';
        $filepath          = "";
        $where = array(
            'id'    => $_POST['id']
        );
        
        if(!isset($_FILES[$file_element_name]))
        {
            $data = array(
                'isi'           => $_POST['isi'],
                'updated_at'    => mdate('%Y-%m-%d', now()),
                'updated_by'    => $this->session->userdata('userid') 
            );
            $update = $this->Model_bpd->update_data($where,$data);
            if($update == true)
            {
                $status = "success";
                $msg    = "Success updated item";
            }
            else
            {
                $status = "error";
                $msg    = "Error updated item"; 
            }
        }
        else
        {
            $config['upload_path'] = './upload_file/profil/';
            $config['allowed_types'] = 'gif|jpg|png|jpeg';
            $config['max_size'] = 8192;
            $config['encrypt_name'] = TRUE;

            $this->upload->initialize($config);
            $this->load->library('upload',$config);

            if($this->upload->do_upload($file_element_name))
            {
                $data = $this->upload->data();
                $filepath = base_url().'upload_file/profil/'.$data['file_name'];
                $data = array(
                    'isi'           => $_POST['isi'],
                    'filepath'      => $filepath,
                    'updated_at'    => mdate('%Y-%m-%d', now()),
                    'updated_by'    => $this->session->userdata('userid') 
                );
                $update = $this->Model_bpd->update_data($where,$data);
                if($update == true)
                {
                    $status = "success";
                    $msg = "Success updated item";
                }
                else
                {
                    unlink($data['full_path']);
                    $status = "error";
                    $msg = "Error updated item";
                }
            }
            @unlink($_FILES[$file_element_name]);
        }   
        echo json_encode(array('status' => $status, 'msg' => $msg));
    }

}
