<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include('application/controllers/auth/DefaultController.php');

class UserController extends DefaultController {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct()
	{
		parent::__construct();
		$this->checkLogin();
    if($this->session->userdata('role') != 1)
      redirect('admin', 'refresh');
	}

	public function index()
	{
		$this->load->view('users/page/users');
	}

	public function getData()
	{
		/*
		$this->load->database();
		$this->db->order_by("users.id", "desc");
       	$query = $this->db->get_where("users",array('isActive'=>1));
       	$data['data'] = $query->result();
       	$data['total'] = $this->db->count_all("users");

       	echo json_encode($data);
       	*/
       	$this->load->database();
        $this->load->model('Model_users');
        $list = $this->Model_users->get_datatables();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $item) {
            $no++;
            $row = array();
            $row['no'] = $no;
            $row['id'] = $item->id;
            $row['username'] = $item->username;
            $row['nama'] = $item->nama;
            if($item->isActive == 1)
            {
                $row['isActive'] = 'Aktif';
                $row['action']   = '<button class="btn btn-warning btn-sm" title="Edit" onclick="update('."'".$item->id."'".')"><i class="fa fa-edit"></i></button> &nbsp;
            <button class="btn btn-danger btn-sm" title="Hapus" onclick="hapus('."'".$item->id."'".')"><i class="fa fa-trash-o"></i></button>';
            }
            else if($item->isActive == 0)
            {
                $row['isActive'] = 'Tidak Aktif';
                if($this->session->userdata('role') ==1)
                {
                    $row['action'] = '<button class="btn btn-success btn-sm" title="Aktifkan" onclick="activate('."'".$item->id."'".')"><i class="fa fa-check"></i></button>';
                }
            }

            $data[] = $row;
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->Model_users->count_all(),
            "recordsFiltered" => $this->Model_users->count_filtered(),
            "data" => $data,
        );
        echo json_encode($output);
	}

	public function insertData()
	{
		$this->load->database();
		$insert = $this->input->post();
       	$this->db->insert('users', $insert);
       	$id = $this->db->insert_id();
       	$q = $this->db->get_where('users', array('id' => $id));

       	echo json_encode($q->row());
	}

	public function getById($id)
   	{
       $this->load->database();
       $q = $this->db->get_where('users', array('id' => $id));
       $data['data']=$q->result();
       echo json_encode($data);
   	}

   	public function editData($id)
   	{
   		$this->load->database();
   		$insert = $this->input->post();
   		$this->db->where('id',$id);
   		$this->db->update('users',$insert);
   		$q = $this->db->get_where('users', array('id' => $id));

   		echo json_encode($insert);
   	}

   	public function haspusData($id)
   	{
   		$this->load->database();
   		$insert = $this->input->post();
   		$this->db->where('id',$id);
   		$this->db->update('users',$insert);
   		$q = $this->db->get_where('users', array('id' => $id));

   		echo json_encode($insert);
   	}

    public function activate($id)
    {
        $this->load->database();
        $this->load->model('Model_users');
        $status = "";
        $msg    = "";

        $where = array(
            'id'    => $_POST['id']
        );

        $data = array(
            'isActive'      => 1,
            'updated_at'    => mdate('%Y-%m-%d', now()),
            'updated_by'    => $this->session->userdata('userid') 
        );
        $update = $this->Model_users->update_data($where,$data);
        if($update == true)
        {
            $status = "success";
            $msg    = "Success activated item";
        }
        else
        {
            $status = "error";
            $msg    = "Error activated item";    
        }
        echo json_encode(array('status' => $status, 'msg' => $msg));
    }
}