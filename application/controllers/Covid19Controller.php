<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include('application/controllers/auth/DefaultController.php');

class Covid19Controller extends DefaultController {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     *      http://example.com/index.php/welcome
     *  - or -
     *      http://example.com/index.php/welcome/index
     *  - or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
     */
    public function __construct()
    {
        parent::__construct();
        header('Access-Control-Allow-Origin: *');
        $this->load->helper('url');
        // $this->load->model('Excel_import_model');
        $this->load->library('excel');
        $this->checkLogin();
    }

    public function index()
    {
        $this->load->view('users/page/importcovid19');
    }

    public function laporan()
    {
        $conn = $this->load->database('covid19', TRUE);
        //alldata
        $datas=$conn->query('SELECT * FROM `covid` WHERE (alamat LIKE "%'.$this->session->userdata('slug').'%" AND alamat LIKE "%'.explode('.', $_SERVER['HTTP_HOST'])[0].'%") OR (alamat_tujuan LIKE "%'.$this->session->userdata('slug').'%" AND alamat_tujuan LIKE "%'.explode('.', $_SERVER['HTTP_HOST'])[0].'%")');
        //data otg
        $otg=$conn->query('SELECT * FROM `covid` WHERE otg IS NOT NULL AND (alamat LIKE "%'.$this->session->userdata('slug').'%" AND alamat LIKE "%'.explode('.', $_SERVER['HTTP_HOST'])[0].'%") OR otg IS NOT NULL AND (alamat_tujuan LIKE "%'.$this->session->userdata('slug').'%" AND alamat_tujuan LIKE "%'.explode('.', $_SERVER['HTTP_HOST'])[0].'%")');
        //$otg = $conn->get('covid')->num_rows();
        //data odp
        $odp=$conn->query('SELECT * FROM `covid` WHERE odp IS NOT NULL AND (alamat LIKE "%'.$this->session->userdata('slug').'%" AND alamat LIKE "%'.explode('.', $_SERVER['HTTP_HOST'])[0].'%") OR odp IS NOT NULL AND (alamat_tujuan LIKE "%'.$this->session->userdata('slug').'%" AND alamat_tujuan LIKE "%'.explode('.', $_SERVER['HTTP_HOST'])[0].'%")');
        //data pdp
        $pdp=$conn->query('SELECT * FROM `covid` WHERE pdp IS NOT NULL AND (alamat LIKE "%'.$this->session->userdata('slug').'%" AND alamat LIKE "%'.explode('.', $_SERVER['HTTP_HOST'])[0].'%") OR pdp IS NOT NULL AND (alamat_tujuan LIKE "%'.$this->session->userdata('slug').'%" AND alamat_tujuan LIKE "%'.explode('.', $_SERVER['HTTP_HOST'])[0].'%")');
        //data masih dirawat
        $dirawat=$conn->query('SELECT * FROM `covid` WHERE masih_dirawat IS NOT NULL AND (alamat LIKE "%'.$this->session->userdata('slug').'%" AND alamat LIKE "%'.explode('.', $_SERVER['HTTP_HOST'])[0].'%") OR masih_dirawat IS NOT NULL AND (alamat_tujuan LIKE "%'.$this->session->userdata('slug').'%" AND alamat_tujuan LIKE "%'.explode('.', $_SERVER['HTTP_HOST'])[0].'%")');
        //data orang sembuh
        $sembuh=$conn->query('SELECT * FROM `covid` WHERE sembuh IS NOT NULL AND (alamat LIKE "%'.$this->session->userdata('slug').'%" AND alamat LIKE "%'.explode('.', $_SERVER['HTTP_HOST'])[0].'%") OR sembuh IS NOT NULL AND (alamat_tujuan LIKE "%'.$this->session->userdata('slug').'%" AND alamat_tujuan LIKE "%'.explode('.', $_SERVER['HTTP_HOST'])[0].'%")');
        //data orang meninggal
        $meninggal=$conn->query('SELECT * FROM `covid` WHERE meninggal IS NOT NULL AND (alamat LIKE "%'.$this->session->userdata('slug').'%" AND alamat LIKE "%'.explode('.', $_SERVER['HTTP_HOST'])[0].'%") OR meninggal IS NOT NULL AND (alamat_tujuan LIKE "%'.$this->session->userdata('slug').'%" AND alamat_tujuan LIKE "%'.explode('.', $_SERVER['HTTP_HOST'])[0].'%")');
        //-------------------------------------------------------------
        $data = [ 
        'datas' => $datas->result_array(),
        'otg'   => $otg->result_array(),
        'odp'   => $odp->result_array(),
        'pdp' => $pdp->result_array(),
        'dirawat'   => $dirawat->result_array(),
        'sembuh'   => $sembuh->result_array(),
        'meninggal'   => $meninggal->result_array()
        ];
        $this->load->view('users/page/laporancovid19',$data);
    }

    public function import()
    {
        $excel = $_FILES['excel'];
        $config['upload_path']          = './tmp/';
        $config['allowed_types']        = 'xlsx';
        $config['max_size']             = 3000;
        $config['file_name']            = time().'.xlsx';

        $target = basename('tmp/'.time().'.xlsx') ;
        $this->upload->initialize($config);
        $this->load->library('upload',$config);

        $filename = '';
        if (!$this->upload->do_upload('excel')) {
            $error = $this->upload->display_errors();
            print_r($error);
        } else {
            $result = $this->upload->data();
            $filename = $result['file_name'];
        }

        echo $filename;
    }

    public function insert()
    {
        $conn = $this->load->database('covid19', TRUE);
            //$conn->empty_table('covid');
        $path = 'tmp/'.$_GET['filename'];
        $object = PHPExcel_IOFactory::load($path);
        foreach($object->getWorksheetIterator() as $worksheet)
        {
            $highestRow = $worksheet->getHighestRow();
            $highestColumn = $worksheet->getHighestColumn();
            for($row=7; $row<=50; $row++)
            {
                $nik = $worksheet->getCellByColumnAndRow(1, $row)->getValue();
                $telp = $worksheet->getCellByColumnAndRow(2, $row)->getValue();
                $nama = $worksheet->getCellByColumnAndRow(3, $row)->getValue();
                $umur = $worksheet->getCellByColumnAndRow(4, $row)->getValue();
                $jenis_kelamin = $worksheet->getCellByColumnAndRow(5, $row)->getValue();
                $asal_kedatangan = $worksheet->getCellByColumnAndRow(6, $row)->getValue();
                $kecamatan = $worksheet->getCellByColumnAndRow(7, $row)->getValue();
                $desa = $worksheet->getCellByColumnAndRow(8, $row)->getValue();
                $alamat = $worksheet->getCellByColumnAndRow(9, $row)->getValue();
                $kecamatan_tujuan = $worksheet->getCellByColumnAndRow(10, $row)->getValue();
                $desa_tujuan = $worksheet->getCellByColumnAndRow(11, $row)->getValue(); 
                $alamat_tujuan = $worksheet->getCellByColumnAndRow(12, $row)->getValue(); 
                $nik_tujuan = $worksheet->getCellByColumnAndRow(13, $row)->getValue(); 
                $nama_tujuan = $worksheet->getCellByColumnAndRow(14, $row)->getValue(); 
                $suhu_tubuh = $worksheet->getCellByColumnAndRow(15, $row)->getValue(); 
                $keluhan = $worksheet->getCellByColumnAndRow(16, $row)->getValue(); 
                $otg = $worksheet->getCellByColumnAndRow(17, $row)->getValue(); 
                $odp = $worksheet->getCellByColumnAndRow(18, $row)->getValue();
                $pdp = $worksheet->getCellByColumnAndRow(19, $row)->getValue();
                $konfirmasi = $worksheet->getCellByColumnAndRow(20, $row)->getValue(); 
                $rs_ya = $worksheet->getCellByColumnAndRow(21, $row)->getValue(); 
                $rs_tidak = $worksheet->getCellByColumnAndRow(22, $row)->getValue(); 
                $rs_merawat = $worksheet->getCellByColumnAndRow(23, $row)->getValue();
                $faktor_resiko = $worksheet->getCellByColumnAndRow(24, $row)->getValue();
                $diagnosis = $worksheet->getCellByColumnAndRow(25, $row)->getValue(); 
                $kode_icdx = $worksheet->getCellByColumnAndRow(26, $row)->getValue(); 
                $rdt = $worksheet->getCellByColumnAndRow(27, $row)->getValue(); 
                $swab1 = $worksheet->getCellByColumnAndRow(28, $row)->getValue(); 
                $swab2 = $worksheet->getCellByColumnAndRow(29, $row)->getValue(); 
                $lab = $worksheet->getCellByColumnAndRow(30, $row)->getValue(); 
                $rontgen = $worksheet->getCellByColumnAndRow(31, $row)->getValue(); 
                $rapid_test = $worksheet->getCellByColumnAndRow(32, $row)->getValue(); 
                $swab = $worksheet->getCellByColumnAndRow(33, $row)->getValue(); 
                $masih_dirawat = $worksheet->getCellByColumnAndRow(34, $row)->getValue(); 
                $sembuh = $worksheet->getCellByColumnAndRow(35, $row)->getValue(); 
                $meninggal = $worksheet->getCellByColumnAndRow(36, $row)->getValue(); 
                $keterangan = $worksheet->getCellByColumnAndRow(37, $row)->getValue();
                
                if(empty($nik) && empty($nama)){
                    break;
                }
                
                $data = array(
                    'nik'       =>  $nik,
                    'telp'      =>  $telp,
                    'nama'      =>  $nama,
                    'umur'      =>  $umur,
                    'jenis_kelamin'     =>  $jenis_kelamin,
                    'asal_kedatangan'       =>  $asal_kedatangan,
                    'kecamatan'     =>  $kecamatan,
                    'desa'      =>  $desa,
                    'alamat'        =>  $alamat,
                    'kecamatan_tujuan'      =>  $kecamatan_tujuan,
                    'desa_tujuan'       =>  $desa_tujuan,
                    'alamat_tujuan'     =>  $alamat_tujuan,
                    'nik_tujuan'        =>  $nik_tujuan,
                    'nama_tujuan'       =>  $nama_tujuan,
                    'suhu_tubuh'        =>  $suhu_tubuh,
                    'keluhan'       =>  $keluhan,
                    'otg'       =>  $otg,
                    'odp'       =>  $odp,
                    'pdp'       =>  $pdp,
                    'konfirmasi'        =>  $konfirmasi,
                    'rs_ya'     =>  $rs_ya,
                    'rs_tidak'      =>  $rs_tidak,
                    'rs_merawat'        =>  $rs_merawat,
                    'faktor_resiko'     =>  $faktor_resiko,
                    'diagnosis'     =>  $diagnosis,
                    'kode_icdx'     =>  $kode_icdx,
                    'rdt'       =>  $rdt,
                    'swab1'     =>  $swab1,
                    'swab2'     =>  $swab2,
                    'lab'       =>  $lab,
                    'rontgen'       =>  $rontgen,
                    'rapid_test'        =>  $rapid_test,
                    'swab'      =>  $swab,
                    'masih_dirawat'     =>  $masih_dirawat,
                    'sembuh'        =>  $sembuh,
                    'meninggal'     =>  $meninggal,
                    'keterangan'        =>  $keterangan
                    );

                    // cek nik
                $cek = $conn->query("SELECT nik FROM `covid` WHERE nik='".$nik."'");
                
                if($cek->num_rows()>0){
                    $conn->where('nik', $nik);
                    $conn->update('covid',$data);
                    echo "Data Berhasil Diupdate";
                }

                else{
                    $conn->insert('covid',$data);
                    echo 'Data Berhasil Di Import';
                }
            }
        }
    }
}
