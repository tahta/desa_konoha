<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include('application/controllers/auth/DefaultController.php');

class Api extends DefaultController {

    public function __construct()
    {
        parent::__construct();

        header('Access-Control-Allow-Origin: *');
        $this->load->library('xmlrpc');
        $this->load->library('xmlrpcs');
        $this->load->helper('url');
    }

    public function getBerita(){
        $this->load->database();
        $this->db->select("berita.tanggal, users.api_key, berita.id_kategori, berita.isActive");
        $this->db->order_by("created_by", "asc");
        $this->db->join("users", "users.id=berita.created_by");
        $query =  $this->db->get('berita')->result();

        echo json_encode($query); 
    }

    public function getPPID(){
        $this->load->database();
        $this->db->select("ppid.tanggal, users.api_key, ppid.id_kategori, ppid.isActive");
        $this->db->join("users", "users.id=ppid.created_by");
        $query =  $this->db->get('ppid')->result();

        echo json_encode($query); 
    }

}