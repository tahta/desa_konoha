<?php $this->load->view('visitors/layout/header'); ?>
<section>
<div class="container">
   <!--Section: Live preview-->
    <section>    
        <div class="col-12" id="title">
          <center><h3 style="margin-top: 50px;" class="font-weight-bold dark-grey-text mb-3"><?php echo $judul; ?></h3></center><hr align="center">
          <center><h6 id="oleh"><i class='fa fa-calendar' style='color: #28166F;'></i>&nbsp;<?php echo $tanggal; ?> |&nbsp;<i class='fa fa-user' style='color: #28166F;'></i>&nbsp;<?php echo $author; ?></h6></center>
        </div> 
        <br>
        <div class="col-12" id="isi">
          <img id="gambar" style="width: 50%;  float:left; padding-right: 2rem; padding-bottom: 2rem" src='<?php echo $imgpath; ?>'>
          <div style="margin-bottom: 50px; text-align: justify;" id="uisi"><?php echo $isi; ?></div>
        </div>
        <hr>
        
    

    <div class="col-12" name="komentar" id="komentar">
     <div class="row"> 
      
           <script>
       $(document).ready(function(){
            $.get('<?php echo base_url().'captcha/refresh'; ?>', function(data){
                   $('#image_captcha').html(data);
               });
           $('#captcha-refresh').on('click', function(){
               $.get('<?php echo base_url().'captcha/refresh'; ?>', function(data){
                   $('#image_captcha').html(data);
               });
           });
       });
   </script>
   <script src="<?php echo base_url();?>assets/jquery/jquery.js"></script> 


       <div class="col-md-5 col-sm-12" style="background-color: ">
        <h2 class="modal-title">Tulis Komentar</h2> 
         <form method="post" action="<?php echo base_url()."".$this->uri->segment(1).'/'; ?>berita/tambah-komentar/<?php echo $id; ?>">
          <div class="row mb-2">
            <div class="col-sm-10"><input class="form-control" type="text" placeholder="Nama" name="nama" id="nama"></div>
          </div>
          <div class="row mb-2">
            <div class="col-sm-10"><input class="form-control" type="text" placeholder="Alamat e-mail" name="email" id="email"></div>
          </div>
          <div class="row ">
            <div class="col-sm-10 float-right"><textarea name="isi" id="isi" rows="5" cols="80" class="form-control"></textarea></div>
          </div><br>
           <div class="row mb-2">
            <div class="col-sm-12">
               <!-- <?php var_dump($this->session->userdata('valuecaptchaCode'));?> -->
            <p id="image_captcha"><?php echo $captchaImg; ?></p>
            <a href="javascript:void(0);" class="captcha-refresh" id="captcha-refresh"><img src="<?php echo base_url().'images/refresh.png'; ?>" style="width: 20px;"/>Reload Captcha </a>
            </div>
          </div>
          <div class="row mb-2">
            <div class="col-sm-10"><input class="form-control" type="text" name="captcha" value=""/></div>
          </div>
           <!-- <button href="#" id="btnSave" class="btn btn-qibul">Simpan</button> -->
           <input class="btn btn-qibul" type="submit" name="submit" value="Kirim">
          <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
      </form>
      </div>
      
      <div class="col-md-7 mb-7" style="background-color: ">
      <br>
        <h3 class="modal-title">Komentar</h3>
         <?php foreach($komentar as $komen) { ?>


      <div class="alert alert-dark" role="alert" id="komen">
          <p style="color: #3385ff;"><?php echo $komen->nama_kom." ".$komen->created_at; ?></p>
          <p style="font-size: 13px; font-style: italic;"><?php echo $komen->isi_kom; ?></p>
      </div>      
      <?php } ?>
      </div>
      
     
        </div>
        </div>  
        <br><br><br>

    </section>
    <!--Section: Live preview-->     
</div>
</section>
</body>

<?php $this->load->view('visitors/layout/footer'); ?>
</html>
