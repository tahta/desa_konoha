<?php $this->load->view('visitors/layout/header'); ?>
<style>
  .btn-info {
    background-color: #f8c300 !important;
    color: #fff !important;
}
</style>
<section>
<div class="container">
  <div id="galeri">
    <div id="container">
      <div id="portfolio">
        <?php
            $idgaleri = $id;
            $this->db->select('galeri.judul');
            $this->db->where('galeri.id', $idgaleri);
            $this->db->where('galeri.isActive', 1);
            $get_data = $this->db->get('galeri')->row(); 
        ?>
        <h2><?php echo $get_data->judul; ?></h2>
        <div class="portfolio-container" id="columns">
          <ul class="page-galeri">
            <?php
            $idgaleri = $id;
            $this->db->select('detail_galeri.id, detail_galeri.idgaleri, detail_galeri.imgpath, detail_galeri.isActive, galeri.judul');
            $this->db->join('galeri','detail_galeri.idgaleri=galeri.id', 'INNER');
            $this->db->where('detail_galeri.idgaleri', $idgaleri);
            $this->db->where('detail_galeri.isActive', 1);
            $get_data = $this->db->get('detail_galeri')->result();
            foreach ($get_data as $val) { ?>

              <a class='portfolio-item-preview' data-rel='prettyPhoto'><img src="<?php echo $val->imgpath; ?>" alt="" width="210" height="145" class="portfolio-img pretty-box" style="background-color: #f8f8f8;background: -moz-linear-gradient(center bottom , #f8f8f8 0px, #FFF 100%) repeat scroll 0 0 transparent;background: -webkit-gradient(linear,left bottom,left top, color-stop(0, #f8f8f8),color-stop(1, #FFF));border: 1px solid #ebebeb !important;padding: 4px;"></a>

            <?php } ?>                
          </ul>
        </div>
      </div>
    </div>
  </div>
  <center><a href="<?php echo base_url(); ?>galeri/semua-galeri" class="btn btn-info" style=""><i class="fa fa-arrow-left" aria-hidden="true"></i> Kembali </a></center><br>
</div>
</section>
</body>
<?php $this->load->view('visitors/layout/footer'); ?>
</html>
