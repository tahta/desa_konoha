<?php $this->load->view('visitors/layout/header'); ?>
<section>
<div class="container">
  <section>
    <div class="col-12" id="title">
      <h2 id="judul" style="text-align: center;">Data Penerima Bantuan</h2><hr align="center">
    </div> 

    <div class="col-lg-12 col-md-12 magazine-section my-5">
       <?php
          
           $this->db->select('informasi.id,informasi.judul, informasi.isi, informasi.status, informasi.isActive, users.slug');
           $this->db->where(array('informasi.status' => 'Data Penerima Bantuan', 'informasi.isActive' => '1', 'users.slug' => $this->uri->segment(1)));
           $this->db->join('users','informasi.created_by = users.id','INNER');
           $get_data = $this->db->get('informasi')->result();


          foreach ($get_data as $val) {
        ?>
      <div class="single-news mb-2">
        <div class="row">
          <a href="detail_bantuan/<?php echo $val->id; ?>">
            <div class="col-md-9">
              <h4 class="font-weight-bold dark-grey-text" style="font-weight: bold;"><?php echo $val->judul; ?></h4>
              <div class="d-flex justify-content-between">
                <div class="col-11 text-truncate pl-0 mb-3">
                  <a href="detail_bantuan/<?php echo $val->id; ?>" class="dark-grey-text"><?php echo substr($val->isi,0,200); ?></a>
                </div>
                <a href="detail_bantuan/<?php echo $val->id; ?>"><i class="fa fa-angle-double-right"></i></a>
              </div>
            </div>
          </a>
        </div>
      </div>

      <?php } ?>

    </div>
  </section>  
</div>  
</section>
</body>

<?php $this->load->view('visitors/layout/footer'); ?>
