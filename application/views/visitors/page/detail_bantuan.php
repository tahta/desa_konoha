<?php $this->load->view('visitors/layout/header'); ?>
<section>
<div class="container">
   <!--Section: Live preview-->
    <section>    
        <div class="col-12" id="title">
          <center><h3 style="margin-top: 50px;" class="font-weight-bold dark-grey-text mb-3"><?php echo $judul; ?></h3></center><hr align="center">
        </div> 
        <br>
        <div class="col-12" id="isi">
          <div style="margin-bottom: 50px; text-align: justify;" id="uisi"><?php echo $isi; ?></div>
          <?php if ($filepath != '') { ?>
            <ul>
              <li style="color: #1BB9CB;"><i class="fa fa-download" aria-hidden="true"></i> &nbsp; <a href="<?php echo $filepath; ?>" target="_blank" style="color:#1bb9cb; font-size: 17px;">Download File</a></li>
            </ul>
          <?php } ?>
        </div>
    </section>
    <!--Section: Live preview-->     
</div>
</section>
</body>

<?php $this->load->view('visitors/layout/footer'); ?>
 
</html>
