<?php $this->load->view('visitors/layout/header'); ?>
<!-- PopUP -->
<div id="dmainPop" class="modal fade" role="dialog">
  <div class="modal-header" style="border-bottom: none;">
    <button type="button" class="close" data-dismiss="modal">&times;</button>
  </div>
  
  <div class="modal-dialog modal-lg" style="max-width:100% !important;">
    <div class="modal-dialog modal-lg" style="max-width:95% !important;">
      <center><img src="http://rejoso.nganjukkab.go.id/assets/img/kelink2.jpeg" class="img-responsive" width="100%"></center>
    </div>
  </div>
</div>
<!-- slider carousel olshop -->
<section class="container p-t-3">
    <div class="row">
        <div class="col-lg-12">
            <h1>Bootstrap 4 Card Slider</h1>
        </div>
    </div>
</section>
<section class="carousel slide" data-ride="carousel" id="postsCarousel">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 text-md-right lead">
                <a class="btn btn-secondary-outline prev" href="" title="go back"><i class="fa fa-lg fa-chevron-left"></i></a>
                <a class="btn btn-secondary-outline next" href="" title="more"><i class="fa fa-lg fa-chevron-right"></i></a>
            </div>
        </div>
    </div>
    <div class="container p-t-0 m-t-2 carousel-inner">
        <div class="row row-equal carousel-item active m-t-0">
            <div class="col-md-4">
                <div class="card">
                    <div class="card-img-top card-img-top-250">
                        <img class="img-fluid" src="http://i.imgur.com/EW5FgJM.png" alt="Carousel 1">
                    </div>
                    <div class="card-body">
								    									<span class="text-success"><b>Rp. 99,00 </b>
																		&nbsp;&nbsp;<small style="color:red; text-decoration: line-through red;">Rp. 100,00 (1,00%)</small>
									</span>
									<p class="card-text">
										<small class="text-muted"><b><i class="fa fa-user"></i>&nbsp;AGUS TRIONO</b></small><br>
										Melayani Servis semua jenis Motor  buka Senin sampai sabtu menyedikan juga Sparepart									</p>
									<div class="d-flex justify-content-between align-items-center">
										<div class="btn-group">
																														<a class="btn btn-sm btn-success" href="https://api.whatsapp.com/send?phone=+6285357733936&amp;text=Saya ingin membeli Jasa Service yang anda tawarkan" rel="noopener noreferrer" target="_blank" title="WhatsApp"><i class="fa fa-whatsapp"></i> Beli</a>
																				<a class="btn btn-sm btn-primary text-white" data-remote="false" data-toggle="modal" data-target="#modalBesar5" title="Lokasi"><i class="fa fa-map"></i> Lokasi</a>
										<a class="btn btn-sm btn-danger" href="https://www.google.com/maps/dir//-2.8869575048126537,101.45426832139492" rel="noopener noreferrer" target="_blank" title="Arah Tujuan"><i class="fa fa-map-marker"></i> Arah</a>
										</div>
									</div>
								</div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="card">
                    <div class="card-img-top card-img-top-250">
                        <img class="img-fluid" src="http://i.imgur.com/Hw7sWGU.png" alt="Carousel 2">
                    </div>
                    <div class="card-block p-t-2">
                        <h6 class="small text-wide p-b-2">Development</h6>
                        <h2>
                            <a href>How to Make Every Line Count.</a>
                        </h2>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="card">
                    <div class="card-img-top card-img-top-250">
                        <img class="img-fluid" src="http://i.imgur.com/g27lAMl.png" alt="Carousel 3">
                    </div>
                    <div class="card-block p-t-2">
                        <h6 class="small text-wide p-b-2">Design</h6>
                        <h2>
                            <a href>Responsive is Essential.</a>
                        </h2>
                    </div>
                </div>
            </div>
        </div>
        <div class="row row-equal carousel-item m-t-0">
            <div class="col-md-4">
                <div class="card">
                    <div class="card-img-top card-img-top-250">
                        <img class="img-fluid" src="http://i.imgur.com/Hw7sWGU.png" alt="Carousel 4">
                    </div>
                    <div class="card-block p-t-2">
                        <h6 class="small text-wide p-b-2">Another</h6>
                        <h2>
                            <a href>Tagline or Call-to-action.</a>
                        </h2>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="card">
                    <div class="card-img-top card-img-top-250">
                        <img class="img-fluid" src="http://i.imgur.com/g27lAMl.png" alt="Carousel 5">
                    </div>
                    <div class="card-block p-t-2">
                        <h6 class="small text-wide p-b-2"><span class="pull-xs-right">12.04</span> Category 1</h6>
                        <h2>
                            <a href>This is a Blog Title.</a>
                        </h2>
                    </div>
                </div>
            </div>
            <div class="col-md-4 fadeIn wow">
                <div class="card">
                    <div class="card-img-top card-img-top-250">
                        <img class="img-fluid" src="http://i.imgur.com/g27lAMl.png" alt="Carousel 6">
                    </div>
                    <div class="card-block p-t-2">
                        <h6 class="small text-wide p-b-2">Category 3</h6>
                        <h2>
                            <a href>Catchy Title of a Blog Post.</a>
                        </h2>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<style>
/* equal card height */
.row-equal > div[class*='col-'] {
    display: flex;
    flex: 1 0 auto;
}

.row-equal .card {
   width: 100%;
}

/* ensure equal card height inside carousel */
.carousel-inner>.row-equal.active, 
.carousel-inner>.row-equal.next, 
.carousel-inner>.row-equal.prev {
    display: flex;
}

/* prevent flicker during transition */
.carousel-inner>.row-equal.active.left, 
.carousel-inner>.row-equal.active.right {
    opacity: 0.5;
    display: flex;
}


/* control image height */
.card-img-top-250 {
    max-height: 250px;
    overflow:hidden;
}
  </style>
<!-- end slider carousel olshop -->
<section style="padding-bottom: 50px;">
  <div class="container">
    <div class="row">
      <div class="col-md-8 col-sm-12">
        <div style="margin-top: 2rem; " >
          <div class="card-header card animated bounceInDown" style="border-bottom: 5px solid #f8c300; padding: 5px; width: 98%;">
            <a style="font-size: 1.5rem; font-family: 'Open Sans', cursive; color: #fff; margin-left: 10px; ">Kabar Desa</a>
          </div>
        </div>
        <div class="berita-dinsos1">  

        </div><!-- End Item -->

        <div class="berita-dinsos" style="padding-top: 35px;">
        </div>
        <a href="<?php echo $this->uri->segment(1);?>/berita/semua-berita"><i class="fa fa-angle-double-right">Selengkapnya</i></a>
      </div>

      <div class="col-md-4 mb-4">
        <!-- Form with header -->
        <div class="card" style="margin-top: 2rem;" >
          <div class="card-header card animated fadeInUp" style="border-bottom: 5px solid #f8c300; padding: 5px;">
            <a style="font-size: 1.5rem; font-family: 'Open Sans', cursive; color: #fff; margin-left: 10px; ">Komentar Terkini</a>
          </div>
          <div class="card-body card animated fadeInUp">
            <!-- Header -->
            <table class="table table-hover table-responsive" >
              <thead>
                <tr>
                  <div class="col-12" name="komentar" id="komentar">
                    <div style="background-color: ">
                      <h3 class="modal-title">Komentar</h3> 
                    </div>
                    <?php foreach($komentar as $komen) { ?>

                      <div class="alert alert-dark" role="alert" id="komen">
                        <a href="<?php echo base_url($this->session->userdata('view_slug')); ?>/berita/detail-berita/<?php echo $komen->id_berita; ?>"><?php echo $komen->nama_kom." ".$komen->created_at; ?></a>
                        <p style="font-size: 13px; font-style: italic;"><?php echo $komen->isi_kom; ?></p>
                      </div>      
                    <?php } ?>
                  </div>
                </tr>
              </thead>
              <tbody >

              </tbody>
            </table>
          </div>
        </div></br></br></br></br>

        <div class="col-md-4">
          <a href="https://www.lapor.go.id/" target="_blank"><img style="width: 300px; height: 300px"; src="<?php echo base_url();?>assets/img/lapor.jpg"></a>
        </div>
      </div>
      <br>
      <br>

      <div class="col-12 col-md-6">
        <!-- Form with header -->
        <div class="card" style="margin-top: 2rem;" >
          <div class="card-header card" style="border-bottom: 5px solid #f8c300; padding: 5px;">
            <a style="font-size: 1.5rem; font-family: 'Open Sans', cursive; color: #fff; margin-left: 10px; ">Pengumuman</a>
          </div>
          <div class="card-body card">
            <!-- Header -->
            <table class="table table-hover table-responsive" >
              <thead>
                <tr>
                  <th scope="col" style="font-weight: bold;">Tanggal</th>
                  <th scope="col" style="font-weight: bold;">Pengumuman</th>
                  <th scope="col" style="font-weight: bold;"></th>
                </tr>
              </thead>
              <tbody id="isipengumuman">

              </tbody>
            </table>
          </div>
        </div>
      </div>

      <div class="col-12 col-md-6">
        <div class="card" style="margin-top: 2rem;" >
          <div class="card-header card" style="border-bottom: 5px solid #f8c300; padding: 5px;">
            <a style="font-size: 1.5rem; font-family: 'Open Sans', cursive; color: #fff; margin-left: 10px; ">Agenda</a>
          </div>
          <div class="card-body card">
            <!-- Header -->
            <table class="table table-hover table-responsive" >
              <thead>
                <tr>
                  <th scope="col" style="font-weight: bold;">Tanggal</th>
                  <th scope="col" style="font-weight: bold;">Agenda</th>
                  <th scope="col" style="font-weight: bold;"></th>
                </tr>
              </thead>
              <tbody id="isiagenda">

              </tbody>
            </table>

          </div>
        </div>
        <!-- Form with header -->

      </div>

      <!-- Modal agenda -->
      <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header" style="background-color: #007bff;">
              <h5 class="modal-title" id="exampleModalLabel" style="color: #fff;">Detail Agenda</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="color: #fff;">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <div class="text-center">
                <div class="fa fa-pencil-square-o fa-4x mb-3 animated rotateIn dinsos-color"></i></div>
                <h3 id="judul"></h3>
                <h4 id="tanggal"></h4>
                <h4 id="bagian"></h4>
              </div>
            </div>
            <div class="modal-footer justify-content-center">
              <a type="button" class="btn btn-qibul" id="download-file" target="_blank">Download File<i class="fa fa-download ml-1"></i></a>
            </div>
          </div>
        </div>
      </div>

      <!-- Modal pengumuman -->
      <div class="modal fade" id="PengumumanModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header" style="background-color: #007bff;">
              <h5 class="modal-title" id="exampleModalLabel" style="color: #fff;">Detail Pengumuman</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="color: #fff;">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <div class="text-center">
                <div class="fa fa-pencil-square-o fa-4x mb-3 animated rotateIn dinsos-color"></i></div>
                <h3 id="judul"></h3>
              </div>
            </div>
            <div class="modal-footer justify-content-center">
              <a type="button" class="btn btn-qibul" id="download-file" target="_blank">Download File<i class="fa fa-download ml-1"></i></a>
            </div>
          </div>
        </div>
      </div>

    </section>

    <section >
      <div class="container" style="padding-top: 20px;">
        <div>
          <div class="card-header card" style="border-bottom: 5px solid #f8c300; padding: 5px; width: 98%;">
            <a style="font-size: 1.5rem; font-family: 'Open Sans', cursive; color: #fff; margin-left: 10px; ">Seputar Nganjuk</a>
          </div>
        </div>
        <!--Grid row-->
        <div class="row" style="padding-top: 20px;">

          <div class="col-md-6 nganjuk-slide1" style="padding-bottom: 50px;">

          </div>

          <div class="col-md-6 nganjuk-slide2">

          </div>  
        </div>
        <!--Grid row-->
      </div>
    </section>

    <section style="background-color: #e0ebfc;">
      <div class="container">
        <h2 id="judul">
          Galeri<hr align="left">
        </h2>
        <!--Grid row-->
        <div class="row">
          <!--Carousel Wrapper-->
          <div id="multi-item-example" class="carousel slide carousel-multi-item" data-ride="carousel">
            <!--Indicators-->
            <ol class="carousel-indicators">
              <li data-target="#multi-item-example" data-slide-to="0" class="active"></li>
              <li data-target="#multi-item-example" data-slide-to="1"></li>
              <li data-target="#multi-item-example" data-slide-to="2"></li>
            </ol>
            <!--/.Indicators-->
            <!--Slides-->
            <div class="carousel-inner" role="listbox">
              <!--First slide-->
              <div class="carousel-item active galeri1">

              </div>
              <!--/.First slide-->

              <!--Second slide-->
              <div class="carousel-item galeri2">

              </div>
              <!--/.Second slide-->

              <!--Third slide-->
              <div class="carousel-item galeri3">

              </div>
              <!--/.Third slide-->
            </div>
            <!--/.Slides-->
          </div>
          <!--/.Carousel Wrapper-->
        </div>
        <!--Grid row-->
      </div>
      <!-- Modal -->
      <div class="modal fade" id="exampleModal1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel">Detail Galeri</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <center><img id="image-gallery-image" class="img-responsive" style="width: 100%" src=""></center>
            </div>
          </div>
        </div>
      </div>
    </section>
  </script>
  <!-- PopUP -->
  <script type="text/javascript">
    $('#mainPop').modal('show');
    
    (function($) {
    "use strict";

    // manual carousel controls
    $('.next').click(function(){ $('.carousel').carousel('next');return false; });
    $('.prev').click(function(){ $('.carousel').carousel('prev');return false; });
    
})(jQuery);
  </script>
  <script src="<?php echo base_url(); ?>services/HomeService.js"></script>
  <?php $this->load->view('visitors/layout/footer'); ?>