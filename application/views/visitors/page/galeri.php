<?php $this->load->view('visitors/layout/header'); ?>
<section>
<div class="container">
  <section>
    <div class="col-12" id="title">
      <h2 id="judul" style="text-align: center;">DAFTAR BERITA</h2><hr align="center">
    </div> 

    <div id="galeri">
    <div id="container">
      <div id="portfolio">
        <!--END filtering-nav-->
        <div class="portfolio-container" id="columns">
          <ul class="page-galeri">
            <?php
              $this->db->select('detail_galeri.id, detail_galeri.idgaleri, detail_galeri.imgpath, detail_galeri.isActive, galeri.judul');
              $this->db->join('galeri','detail_galeri.idgaleri=galeri.id', 'INNER');
              $this->db->where('detail_galeri.isActive', 1);
              $this->db->order_by("detail_galeri.id", "desc");
              $this->db->group_by('detail_galeri.idgaleri');
              $get_data = $this->db->get('detail_galeri')->result();

              foreach ($get_data as $val) { ?>

                <a title="<?php echo $val->judul; ?>" href="detail-galeri/<?php echo $val->idgaleri; ?>" class='portfolio-item-preview' data-rel='prettyPhoto'><img src="<?php echo $val->imgpath; ?>" alt="" width="210" height="145" class="portfolio-img pretty-box" style="background-color: #f8f8f8;background: -moz-linear-gradient(center bottom , #f8f8f8 0px, #FFF 100%) repeat scroll 0 0 transparent;background: -webkit-gradient(linear,left bottom,left top, color-stop(0, #f8f8f8),color-stop(1, #FFF));border: 1px solid #ebebeb !important;padding: 4px;"></a>

            <?php } ?>
          </ul>
          <!--END ul-->
        </div>
        <!--END portfolio-wrap-->
      </div>
      <div style="clear:both; height: 40px"></div>
    </div>
  </div>  


    
  </section>  
</div>  
</section>
</body>

<?php $this->load->view('visitors/layout/footer'); ?>


<a id="button"></a>

  <script type="text/javascript">
    var btn = $('#button');

    $(window).scroll(function() {
      if ($(window).scrollTop() > 300) {
        btn.addClass('show');
      } else {
        btn.removeClass('show');
      }
    });

    btn.on('click', function(e) {
      e.preventDefault();
      $('html, body').animate({scrollTop:0}, '300');
    });


  </script>
  <script type="text/javascript">
  window.onscroll = function() {myFunction()};

  var header = document.getElementById("myheader");
  var sticky = header.offsetTop;


  function myFunction() {
    if (window.pageYOffset > 350) {
      header.classList.add("sticky");
    } else {
    header.classList.remove("sticky");
    }
  } 
  
  jQuery(document).ready(function($){
  //set your google maps parameters
  var $latitude = 61.5255069,
    $longitude = -0.0836207,
    $map_zoom = 14;

  //google map custom marker icon - .png fallback for IE11
  var is_internetExplorer11= navigator.userAgent.toLowerCase().indexOf('trident') > -1;
  var $marker_url = ( is_internetExplorer11 ) ? 'https://s3-us-west-2.amazonaws.com/s.cdpn.io/148866/cd-icon-location.png' : 'https://s3-us-west-2.amazonaws.com/s.cdpn.io/148866/cd-icon-location_1.svg';
    
  //define the basic color of your map, plus a value for saturation and brightness
  var $main_color = '#2d313f',
    $saturation= -20,
    $brightness= 5;

  //we define here the style of the map
  var style= [ 
    {
      //set saturation for the labels on the map
      elementType: "labels",
      stylers: [
        {saturation: $saturation}
      ]
    },  
      { //poi stands for point of interest - don't show these lables on the map 
      featureType: "poi",
      elementType: "labels",
      stylers: [
        {visibility: "off"}
      ]
    },
    {
      //don't show highways lables on the map
          featureType: 'road.highway',
          elementType: 'labels',
          stylers: [
              {visibility: "off"}
          ]
      }, 
    {   
      //don't show local road lables on the map
      featureType: "road.local", 
      elementType: "labels.icon", 
      stylers: [
        {visibility: "off"} 
      ] 
    },
    { 
      //don't show arterial road lables on the map
      featureType: "road.arterial", 
      elementType: "labels.icon", 
      stylers: [
        {visibility: "off"}
      ] 
    },
    {
      //don't show road lables on the map
      featureType: "road",
      elementType: "geometry.stroke",
      stylers: [
        {visibility: "off"}
      ]
    }, 
    //style different elements on the map
    { 
      featureType: "transit", 
      elementType: "geometry.fill", 
      stylers: [
        { hue: $main_color },
        { visibility: "on" }, 
        { lightness: $brightness }, 
        { saturation: $saturation }
      ]
    }, 
    {
      featureType: "poi",
      elementType: "geometry.fill",
      stylers: [
        { hue: $main_color },
        { visibility: "on" }, 
        { lightness: $brightness }, 
        { saturation: $saturation }
      ]
    },
    {
      featureType: "poi.government",
      elementType: "geometry.fill",
      stylers: [
        { hue: $main_color },
        { visibility: "on" }, 
        { lightness: $brightness }, 
        { saturation: $saturation }
      ]
    },
    {
      featureType: "poi.sport_complex",
      elementType: "geometry.fill",
      stylers: [
        { hue: $main_color },
        { visibility: "on" }, 
        { lightness: $brightness }, 
        { saturation: $saturation }
      ]
    },
    {
      featureType: "poi.attraction",
      elementType: "geometry.fill",
      stylers: [
        { hue: $main_color },
        { visibility: "on" }, 
        { lightness: $brightness }, 
        { saturation: $saturation }
      ]
    },
    {
      featureType: "poi.business",
      elementType: "geometry.fill",
      stylers: [
        { hue: $main_color },
        { visibility: "on" }, 
        { lightness: $brightness }, 
        { saturation: $saturation }
      ]
    },
    {
      featureType: "transit",
      elementType: "geometry.fill",
      stylers: [
        { hue: $main_color },
        { visibility: "on" }, 
        { lightness: $brightness }, 
        { saturation: $saturation }
      ]
    },
    {
      featureType: "transit.station",
      elementType: "geometry.fill",
      stylers: [
        { hue: $main_color },
        { visibility: "on" }, 
        { lightness: $brightness }, 
        { saturation: $saturation }
      ]
    },
    {
      featureType: "landscape",
      stylers: [
        { hue: $main_color },
        { visibility: "on" }, 
        { lightness: $brightness }, 
        { saturation: $saturation }
      ]
      
    },
    {
      featureType: "road",
      elementType: "geometry.fill",
      stylers: [
        { hue: $main_color },
        { visibility: "on" }, 
        { lightness: $brightness }, 
        { saturation: $saturation }
      ]
    },
    {
      featureType: "road.highway",
      elementType: "geometry.fill",
      stylers: [
        { hue: $main_color },
        { visibility: "on" }, 
        { lightness: $brightness }, 
        { saturation: $saturation }
      ]
    }, 
    {
      featureType: "water",
      elementType: "geometry",
      stylers: [
        { hue: $main_color },
        { visibility: "on" }, 
        { lightness: $brightness }, 
        { saturation: $saturation }
      ]
    }
  ];
    
  //set google map options
  var map_options = {
        center: new google.maps.LatLng($latitude, $longitude),
        zoom: $map_zoom,
        panControl: false,
        zoomControl: false,
        mapTypeControl: false,
        streetViewControl: false,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        scrollwheel: false,
        styles: style,
    }
    //inizialize the map
  var map = new google.maps.Map(document.getElementById('google-container'), map_options);
  //add a custom marker to the map        
  var marker = new google.maps.Marker({
      position: new google.maps.LatLng($latitude, $longitude),
      map: map,
      visible: true,
    icon: $marker_url,
  });

  //add custom buttons for the zoom-in/zoom-out on the map
  function CustomZoomControl(controlDiv, map) {
    //grap the zoom elements from the DOM and insert them in the map 
      var controlUIzoomIn= document.getElementById('cd-zoom-in'),
        controlUIzoomOut= document.getElementById('cd-zoom-out');
      controlDiv.appendChild(controlUIzoomIn);
      controlDiv.appendChild(controlUIzoomOut);

    // Setup the click event listeners and zoom-in or out according to the clicked element
    google.maps.event.addDomListener(controlUIzoomIn, 'click', function() {
        map.setZoom(map.getZoom()+1)
    });
    google.maps.event.addDomListener(controlUIzoomOut, 'click', function() {
        map.setZoom(map.getZoom()-1)
    });
  }

  var zoomControlDiv = document.createElement('div');
  var zoomControl = new CustomZoomControl(zoomControlDiv, map);

    //insert the zoom div on the top left of the map
    map.controls[google.maps.ControlPosition.LEFT_TOP].push(zoomControlDiv);
});

  
  </script>


  <!--

  <footer>
    <div class="container">

      
    </div>
  </footer>-->
  <script type="text/javascript">
    var $headline = $('.headline'),
    $inner = $('.inner'),
    $nav = $('nav'),
    navHeight = 75;

$(window).scroll(function() {
  var scrollTop = $(this).scrollTop(),
      headlineHeight = $headline.outerHeight() - navHeight,
      navOffset = $nav.offset().top;

  $headline.css({
    'opacity': (1 - scrollTop / headlineHeight)
  });
  $inner.children().css({
    'transform': 'translateY('+ scrollTop * 0.4 +'px)'
  });
  if (navOffset > headlineHeight) {
    $nav.addClass('scrolled');
  } else {
    $nav.removeClass('scrolled');
  }
});
  </script>

</html>