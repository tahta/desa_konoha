<?php $this->load->view('visitors/layout/header'); ?>
<section>
<div class="container">
    <section>        
        <div class="col-12" id="title">
          <h2 id="judul" style="text-align: center;"><?php echo $judul; ?></h2><hr align="center">
        </div> 
        <br>
        <div class="col-12" id="isi">
          <?php
          if ($status == 'Sejarah') { ?>
            <div style="margin-bottom: 50px;" id="uisi"><?php echo $isi; ?></div>
          <?php } 
          else { ?>
          <center><img id="gambar" style="width: 50%; padding-right: 2rem; padding-bottom: 2rem" src='<?php echo $filepath; ?>'></center>
          <div style="margin-bottom: 50px;" id="uisi"><?php echo $isi; ?></div>
          <?php } ?>

        </div>
    </section>
</div>
</section>
</body>

<?php $this->load->view('visitors/layout/footer'); ?>
