<?php $this->load->view('visitors/layout/header'); ?>
<link rel="stylesheet" href="<?php echo base_url();?>assets/style-kelurahan.css">

<section>
<div class="container">
    <section>        
        <div class="col-12" id="title">
          <h2 id="judul" style="text-align: center; color: #000000">DAFTAR KELURAHAN</h2><hr align="center">
        </div> 
        <div class="row">
        <div class="col-md-12">
          <!-- Start welcome area -->
          <div class="welcome-area">
            <div class="welcome-content">
              <ul class="wc-table">
              	<?php
                  $get_data = $this->db->get_where('data_desa', array('isActive' => '1'))->result();
                  foreach ($get_data as $val) {
                ?>
                <li>
                	<a href="<?php echo base_url(); ?>kelurahan/<?php echo $val->id; ?>">
	                  <div class="single-wc-content wow fadeInUp">
	                    <span class="fa fa-university wc-icon" style="color: #000000"></span>
	                    <h4 class="wc-tittle"><?php echo $val->namadesa; ?></h4>
	                  </div>
                  	</a>
                </li>
            	<?php } ?>
              </ul>
            </div>
          </div>
          <!-- End welcome area -->
        </div>
      </div>
    </section>
</div>
</section>
</body>

<?php $this->load->view('visitors/layout/footer'); ?>
