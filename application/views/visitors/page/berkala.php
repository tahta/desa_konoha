<?php $this->load->view('visitors/layout/header'); 
 //$filepath=array(); ?>
<section class="berkala" id="berkala" >
<div class="container">
  <div>
    <br>
    <h2>
        Informasi Berkala Pemerintah Desa
    </h2>
    <br>
</div>
<div>
    <table border="1" class="table table-striped table-hover table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
        <tbody>
            <tr>
                <td valign="top" width="43">
                    <p>
                        A
                    </p>
                </td>
                <td valign="top" width="432">
                    <p>
                        PROFIL PEMERINTAH DESA  
                    </p>
                    <p>
                        1. Alamat
                    </p>
                    <p>
                        2. Visi dan Misi
                    </p>
                    <p>
                        3. Tugas Kepala Desa dan Perangkat Desa
                    </p>
                    <p>
                        4. Profil Kepala Desa dan Perangkat Desa
                    </p>
                    
                </td>
                <td valign="top" width="96">
                    <p>
                         
                    </p>
                    <p>
                         
                    </p>
                    <p>
                        
                    </p>
                    
                </td>
                <td valign="top" width="90">
                    <p>
                         
                    </p>
                    <p>
                        <a
                            href="<?php if(isset($filepath[1])){ echo $filepath[1] ? $filepath[1] : '#' ; }else {echo '#'; } ?>"
                            target="_blank"
                        >
                            LIHAT
                        </a>
                    </p>
                    <p>
                         
                    </p>
                    <p>
                         
                    </p>
                    <p>
                         
                    </p>
                    <p>
                         
                    </p>
                    <p>
                         
                    </p>
                    <p>
                         
                    </p>
                    <p>
                         
                    </p>
                    <p>
                         
                    </p>
                </td>
            </tr>
            <tr>
                <td valign="top" width="43">
                    <p>
                        B
                    </p>
                </td>
                <td valign="top" width="432">
                    <p>
                        INFORMASI PROGRAM DAN KEGIATAN
                    </p>
                    <p>
                        1.    Nama program dan kegiatan
                    </p>
                    <p>
                        2. Besaran anggaran
                    </p>
                    <p>
                        3. Sumber dana
                    </p>
                    
                </td>
                <td valign="top" width="96">
                    <p>
                         
                    </p>
                    <p>
                         
                    </p>
                    <p>
                        
                    </p>
                    
                </td>
                <td valign="top" width="90">
                    <p>
                         
                    </p>
                    <p>
                        <a
                            href="<?php if(isset($filepath[2])){ echo $filepath[2] ? $filepath[2] : '#' ; }else {echo '#'; } ?>"
                            target="_blank"
                        >
                            LIHAT RINGKASAN
                        </a>
                    </p>
                    <p>
                         
                    </p>
                    <p>
                         
                    </p>
                    <p>
                         
                    </p>
                    <p>
                         
                    </p>
                    <p>
                         
                    </p>
                    <p>
                         
                    </p>
                    <p>
                         
                    </p>
                    <p>
                         
                    </p>
                </td>
            </tr>
            
            <tr>
                <td valign="top" width="43">
                    <p>
                        C
                    </p>
                </td>
                <td valign="top" width="432">
                    <p>
                        INFORMASI PROGRAM MASUK DESA
                    </p>
                </td>
                <td valign="top" width="96">
                    <p>
                        
                    </p>
                    <p>
                         
                    </p>
                </td>
                <td valign="top" width="90">
                    <p>
                        <a
                            href="<?php if(isset($filepath[3])){ echo $filepath[3] ? $filepath[3] : '#' ; }else {echo '#'; } ?>"
                            target="_blank"
                        >
                            LIHAT
                        </a>
                    </p>
                </td>
            </tr>
            <tr>
                
            <tr>
                <td valign="top" width="43">
                    <p>
                        D
                    </p>
                </td>
                <td valign="top" width="432">
                    <p>
                        DOKUMEN PERENCANAAN DAN PENGANGGARAN
                    </p>
                </td>
                <td valign="top" width="96">
                    <p>
                         
                    </p>
                </td>
                <td valign="top" width="90">
                    <p>
                         
                    </p>
                </td>
            </tr>
            <tr>
                <td valign="top" width="43">
                    1.
                </td>
                <td valign="top" width="432">
                    Rencana Pembangunan Jangka Menengah Desa
                    <p>
                        (RPJM Desa)
                    </p>
                </td>
                <td>
                     
                </td>
                <td valign="top" width="90">
                    <a
                        href="<?php if(isset($filepath[4])){ echo $filepath[4] ? $filepath[4] : '#' ; }else {echo '#'; } ?>"
                        target="_blank"
                    >
                        LIHAT
                    </a>
                </td>
            </tr>
            <tr>
                <td valign="top" width="43">
                    2.
                </td>
                <td valign="top" width="432">
                    Rencana Kerja Pemerintah Desa
                    <p>
                        (RKP Desa)
                    </p>
                </td>
                <td>
                    
                </td>
                <td valign="top" width="90">
                    <a
                        href="<?php if(isset($filepath[5])){ echo $filepath[5] ? $filepath[5] : '#' ; }else {echo '#'; } ?>"
                        target="_blank"
                    >
                        LIHAT
                    </a>
                </td>
            </tr>
            <tr>
                <td valign="top" width="43">
                    3.
                </td>
                <td valign="top" width="432">
                    Daftar Usulan Rencana Kerja Pemerintah Daerah
                    <p>
                        (DU - RKPD)
                    </p>
                </td>
                <td>
                     
                </td>
                <td valign="top" width="90">
                    <a
                        href="<?php if(isset($filepath[6])){ echo $filepath[6] ? $filepath[6] : '#' ; }else {echo '#'; } ?>"
                        target="_blank"
                    >
                        LIHAT
                    </a>
                </td>
            </tr>
            <tr>
                <td valign="top" width="43">
                    4.
                </td>
                <td valign="top" width="432">
                    Anggaran Pendapatan dan Belanja Desa
                    <p>
                        (APB Desa)
                    </p>
                </td>
                <td>
                    
                </td>
                <td valign="top" width="90">
                    <a
                        href="<?php if(isset($filepath[7])){ echo $filepath[7] ? $filepath[7] : '#' ; }else {echo '#'; } ?>"
                        target="_blank"
                    >
                        LIHAT
                    </a>
                </td>
            </tr>
            <tr>
                <td valign="top" width="43">
                    <p>
                        E
                    </p>
                </td>
                <td valign="top" width="432">
                    <p>
                        PERDES APB DESA
                    </p>
                </td>
                <td valign="top" width="96">
                    <p>
                         
                    </p>
                </td>
                <td valign="top" width="90">
                    <p>
                        <a
                            href="<?php if(isset($filepath[8])){ echo $filepath[8] ? $filepath[8] : '#' ; }else {echo '#'; } ?>"
                            target="_blank"
                        >
                            LIHAT
                        </a>
                    </p>
                </td>
            </tr>
            <tr>
                <td valign="top" width="43">
                    <p>
                        F
                    </p>
                </td>
                <td valign="top" width="432">
                    <p>
                        LAPORAN KINERJA
                    </p>
                </td>
                <td valign="top" width="96">
                    <p>
                        
                    </p>
                </td>
                <td valign="top" width="90">
                    <p>
                        <a>
                            
                        </a>
                    </p>
                </td>
            </tr>
            <tr>
                <td valign="top" width="43">
                    1.
                </td>
                <td valign="top" width="432">
                    Laporan Penyelenggaraan Pemerintah Desa
                    <p>
                        Ahli Tahun Anggaran (LPP Desa)
                    </p>
                </td>
                <td>
                     
                </td>
                <td valign="top" width="90">
                    <a
                        href="<?php if(isset($filepath[9])){ echo $filepath[9] ? $filepath[9] : '#' ; }else {echo '#'; } ?>"
                        target="_blank"
                    >
                        LIHAT
                    </a>
                </td>
            </tr>
            <tr>
                <td valign="top" width="43">
                    2.
                </td>
                <td valign="top" width="432">
                    Laporan Penyelenggaraan Pemerintah Desa
                    <p>
                        Ahli Masa Jabatan (LPP Desa)
                    </p13
                </td>
                <td>
                    
                </td>
                <td valign="top" width="90">
                    <a
                        href="<?php if(isset($filepath[10])){ echo $filepath[10] ? $filepath[10] : '#' ; }else {echo '#'; } ?>"
                        target="_blank"
                    >
                        LIHAT
                    </a>
                </td>
            </tr>
            <tr>
                <td valign="top" width="43">
                    3.
                </td>
                <td valign="top" width="432">
                    Laporan Kinerja Kepala Desa
                    <p>
                        (LK Kades)
                    </p>
                </td>
                <td>
                     
                </td>
                <td valign="top" width="90">
                    <a
                        href="<?php if(isset($filepath[11])){ echo $filepath[11] ? $filepath[11] : '#' ; }else {echo '#'; } ?>"
                        target="_blank"
                    >
                        LIHAT
                    </a>
                </td>
            </tr>
            <tr>
                <td valign="top" width="43">
                    4.
                </td>
                <td valign="top" width="432">
                    Laporan Pelaksanaan Tugas Perangkat Desa
                    <p>
                        (LPT)
                    </p>
                </td>
                <td>
                    
                </td>
                <td valign="top" width="90">
                    <a
                        href="<?php if(isset($filepath[12])){ echo $filepath[12] ? $filepath[12] : '#' ; }else {echo '#'; } ?>"
                        target="_blank"
                    >
                        LIHAT
                    </a>
                </td>
            </tr>
            <tr>
                <td valign="top" width="43">
                    <p>
                        G
                    </p>
                </td>
                <td valign="top" width="432">
                    <p>
                        LAPORAN KEUANGAN
                    </p>
                </td>
                <td valign="top" width="96">
                    <p>
                         
                    </p>
                </td>
                <td valign="top" width="90">
                    <p>
                        <a>
                            
                        </a>
                    </p>
                </td>
            </tr>
            <tr>
                <td valign="top" width="43">
                    1.
                </td>
                <td valign="top" width="432">
                    LRA APB Desa
                    
                </td>
                <td>
                     
                </td>
                <td valign="top" width="90">
                    <a
                        href="<?php if(isset($filepath[13])){ echo $filepath[13] ? $filepath[13] : '#' ; }else {echo '#'; } ?>"
                        target="_blank"
                    >
                        LIHAT
                    </a>
                </td>
            </tr>
            <tr>
                <td valign="top" width="43">
                    2.
                </td>
                <td valign="top" width="432">
                    Laporan Realisasi Kegiatan
                    
                </td>
                <td>
                    
                </td>
                <td valign="top" width="90">
                    <a
                        href="<?php if(isset($filepath[14])){ echo $filepath[14] ? $filepath[14] : '#' ; }else {echo '#'; } ?>"
                        target="_blank"
                    >
                        LIHAT
                    </a>
                </td>
            </tr>
            <tr>
                <td valign="top" width="43">
                    <p>
                        H
                    </p>
                </td>
                <td valign="top" width="432">
                    <p>
                        PRODUK HUKUM DESA
                    </p>
                </td>
                <td valign="top" width="96">
                    <p>
                        
                    </p>
                </td>
                <td valign="top" width="90">
                    <!-- <p>
                        <a
                            href="<?php if(isset($filepath[15])){ echo $filepath[15] ? $filepath[15] : '#' ; }else {echo '#'; } ?>"
                            target="_blank"
                        >
                            LIHAT
                        </a>
                    </p> -->
                </td>
            </tr>
            <tr>
                <td valign="top" width="43">
                    1.
                </td>
                <td valign="top" width="432">
                    Peraturan Desa
                    
                </td>
                <td>
                     
                </td>
                <td valign="top" width="90">
                    <a
                        href="<?php echo base_url(); ?><?php echo $this->uri->segment('1'); ?>/informasi/berkala/peraturandesa"
                        target="_blank"
                    >
                        LIHAT
                    </a>
                </td>
            </tr>
            <tr>
                <td valign="top" width="43">
                    2.
                </td>
                <td valign="top" width="432">
                    Peraturan Kepala Desa
                </td>
                <td>
                    
                </td>
                <td valign="top" width="90">
                    <a
                        href="<?php if(isset($filepath[16])){ echo $filepath[16] ? $filepath[16] : '#' ; }else {echo '#'; } ?>"
                        target="_blank"
                    >
                        LIHAT
                    </a>
                </td>
            </tr>
            <tr>
                <td valign="top" width="43">
                    3.
                </td>
                <td valign="top" width="432">
                    Peraturan Bersama Kepala Desa
                </td>
                <td>
                     
                </td>
                <td valign="top" width="90">
                    <a
                        href="<?php if(isset($filepath[17])){ echo $filepath[17] ? $filepath[17] : '#' ; }else {echo '#'; } ?>"
                        target="_blank"
                    >
                        LIHAT
                    </a>
                </td>
            </tr>
            <tr>
                <td valign="top" width="43">
                    4.
                </td>
                <td valign="top" width="432">
                    Keputusan Kepala Desa
                </td>
                <td>
                    
                </td>
                <td valign="top" width="90">
                    <a
                        href="<?php if(isset($filepath[18])){ echo $filepath[18] ? $filepath[18] : '#' ; }else {echo '#'; } ?>"
                        target="_blank"
                    >
                        LIHAT
                    </a>
                </td>
            </tr>
        </tbody>
    </table>
</div>
</div>
</section>

<style type="text/css">
    a {
        font-size: 12px;
        }
</style>
</body>

<?php $this->load->view('visitors/layout/footer'); ?>
</html>
