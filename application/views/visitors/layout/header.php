<!DOCTYPE html>
<html lang="en" >

<head>
  <meta charset="UTF-8">
  <?php
  $get_data = $this->db->get('kontak')->row();
  ?>
  <title>Portal <?php echo $this->session->userdata('view_desa'); ?></title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  
  <link href="<?php echo base_url();?>assets/frontend/images/log.png" rel="shortcut icon" />
  
  <!--<script type="text/javascript" src="<?php #echo base_url();?>assets/jquery/jquery.js"></script>-->
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <link rel="stylesheet" href="<?php echo base_url();?>assets/bootstrap/css/bootstrap.css">
  <script type="text/javascript" src="<?php echo base_url();?>assets/bootstrap/js/bootstrap.js"></script>
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/dist/css/qibul.css">
  <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.css">

  <link href="<?php echo base_url();?>assets/frontend/css/style.css" rel="stylesheet">
  <link href="<?php echo base_url();?>assets/frontend/css/mdb.css" rel="stylesheet">
  <script type="text/javascript" src="https://widget.kominfo.go.id/gpr-widget-kominfo.min.js"></script>

  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:700" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Squada+One" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Paytone+One" rel="stylesheet">

  <style type="text/css">
    .nav-link, .dropdown-item{
      font-size: 15px;
    }
    .card-header{
      background-color: #a62639;
    }
    .blockquote {
      display: block;
      margin-block-start: 1em;
      margin-block-end: 1em;
      margin-inline-start: 40px;
      margin-inline-end: 40px;
    }
  </style>

</head>

<body>
  <div>
    <div class="" style="z-index: 90; padding: 8px; background-color: #a62639; font-family: 'Open Sans', sans-serif;">

      <div class="top-bar element-animate fadeInUp element-animated container">  
        <div class="row">
          <div class="col-6"> 
            <img class="logo" src="<?php echo base_url();?>assets/frontend/images/log.png" alt="">
            <div class="logo-text">
              <div class="text-atas">
                <span class="brand-text font-weight-light" style="font-family: 'Squada One';">
                  <strong><span style="color: #fff;"> <?php echo $this->session->userdata('view_desa'); ?></span></strong>
                </span>
              </div>
              <div class="text-bawah">
                <span class="brand-text font-weight-light" style="font-family: 'Squada One';">
                  <span style="color: #fff;"><?php echo $get_data->nama_kecamatan; ?></span>
                </span>
              </div>
            </div>

          </div>
          <div class="col-6" style="text-align: right;">
            <div class="navbar navbar-expand-lg navbar-light" id="myheader">
              <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarColor03" aria-controls="navbarColor03" aria-expanded="false" aria-label="Toggle navigation">
               <span class="navbar-toggler-icon"></span>
             </button>
             <div class="collapse navbar-collapse" id="navbarColor03">
              
               <ul class="navbar-nav mr-auto">
                <li class="nav-item">
                  <a class="nav-link active" href="<?php echo base_url($this->session->userdata('view_slug')); ?>" style="color: #fff;">Beranda</a>
                </li>
                
                <li class="dropdown ">
                  <a class="nav-link" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="color: #fff;">PPID Desa <i class="fa fa-angle-down"></i></a>
                  <ul class="dropdown-menu dropdown-primary">
                    <li class="dropdown submenu">
                      <a href="#" class="nav-link dropdown-item" data-toggle="dropdown">Profil PPID Desa</a>
                      <div class="dropdown-menu dropdown-primary submenu-block" aria-labelledby="navbarDropdown">
                        <a href="<?php echo base_url($this->session->userdata('view_slug')); ?>/ppidview/profilsingkat" class="dropdown-item submenu-item" style="color: #6f6f6f;">Profil Singkat</a>
                        <a href="<?php echo base_url($this->session->userdata('view_slug')); ?>/ppidview/tugasfungsi" class="dropdown-item submenu-item" style="color: #6f6f6f;">Tugas dan Fungsi</a>
                        <a href="<?php echo base_url($this->session->userdata('view_slug')); ?>/ppidview/visimisi" class="dropdown-item submenu-item" style="color: #6f6f6f;">Visi Misi</a>
                        <a href="<?php echo base_url($this->session->userdata('view_slug')); ?>/ppidview/struktur" class="dropdown-item submenu-item" style="color: #6f6f6f;">Struktur Org.</a>
                        <a href="<?php echo base_url($this->session->userdata('view_slug')); ?>/ppidview/maklumat" class="dropdown-item submenu-item" style="color: #6f6f6f;">Maklumat Pelayanan</a>
                      </div>
                    </li>
                    <li><a href="<?php echo base_url($this->session->userdata('view_slug')); ?>/informasi/diponline" class="dropdown-item">DIP Online</a></li>
                    <li><a href="<?php echo base_url($this->session->userdata('view_slug')); ?>/informasi/berkala" class="dropdown-item">Informasi Berkala</a></li>
                    <li><a href="<?php echo base_url($this->session->userdata('view_slug')); ?>/informasi/setiapsaat" class="dropdown-item">Informasi Setiap Saat</a></li>
                    <li><a href="<?php echo base_url($this->session->userdata('view_slug')); ?>/informasi/sertamerta" class="dropdown-item">Informasi Serta-Merta</a></li>
                    <li><a href="<?php echo base_url($this->session->userdata('view_slug')); ?>/informasi/alurpermohonan" class="dropdown-item">Alur Permohonan Informasi</a></li>
                    <li><a href="<?php echo base_url($this->session->userdata('view_slug')); ?>/informasi/alurkeberatan" class="dropdown-item">Alur Permohonan Keberatan</a></li>
                    <li><a href="<?php echo base_url($this->session->userdata('view_slug')); ?>/informasi/sk_ppid" class="dropdown-item">Surat Keputusan PPID</a></li>
                  </li>
                </ul>
              </li>
              
              <li class="dropdown ">
                <a class="nav-link" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="color: #fff;">Transparansi Desa <i class="fa fa-angle-down"></i></a>
                <ul class="dropdown-menu dropdown-primary">
                  <li><a href="<?php echo base_url($this->session->userdata('view_slug')); ?>/informasi/transparansi_desa" class="dropdown-item">Transparansi Keuangan Desa</a></li>
                  <li><a href="<?php echo base_url($this->session->userdata('view_slug')); ?>/informasi/penerima_bantuan" class="dropdown-item">Data Penerima Bantuan</a></li>
                  <li><a href="<?php echo base_url($this->session->userdata('view_slug')); ?>/informasi/prestasi_desa" class="dropdown-item">Prestasi Desa</a></li>
                </li>
              </ul>
            </li>
            <li class="dropdown ">
              <a class="nav-link" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="color: #fff;">Profil <i class="fa fa-angle-down"></i></a>
              <div class="dropdown-menu dropdown-primary" aria-labelledby="navbarDropdown">
                <?php
                $this->db->select('profil.id as id, profil.judul as judul, profil.isi as isi, profil.filepath as filepath, profil.status as status, profil.created_by as created_by, users.slug as slug');
                $this->db->join('users','profil.created_by = users.id','INNER');
                $this->db->where('users.slug', $this->uri->segment(1));
                $get_data = $this->db->get('profil')->result();
                foreach ($get_data as $val) {
                  ?>
                  <a class="dropdown-item" href="<?php echo base_url($this->session->userdata('view_slug')); ?>/profil/<?php echo $val->id; ?>"><?php echo $val->status; ?></a>
                <?php } ?>
              </div>
            </li>
          </div>


        </div>
      </div>
    </div>
    
  </div>  
</div>
<!--Carousel Wrapper-->
<div id="carousel-example-2" class="carousel slide carousel-fade" data-ride="carousel">
 
  <!--Indicators-->
  <ol class="carousel-indicators poster-indicators">
    
  </ol>
  <!--/.Indicators-->
  <!--Slides-->
  <div id="header" class="carousel-inner poster-inner" role="listbox">
    
  </div>
  
  <!--/.Slides-->
  <!--Controls-->
  <a class="carousel-control-prev" href="#carousel-example-2" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="carousel-control-next" href="#carousel-example-2" role="button" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
  <!--/.Controls-->
</div>
<!--/.Carousel Wrapper-->
</div>

<div class="navbar navbar-expand-lg navbar-light sticky-top" id="myheader" style="z-index: 90; background-color: #db324d; font-family: 'Open Sans', sans-serif; box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);">
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarColor02" aria-controls="navbarColor03" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="container">  
   <div class="row">
    <div class="col-12"> 
      
      <p style="color: #f8c300; font-size: 22px;">&nbspKarya Desaku</p>
      
    </div>

    <div class="col-12"> 
     <div class="collapse navbar-collapse" id="navbarColor02">
      <ul class="navbar-nav mr-auto">
        <li class="nav-item">
          <a class="nav-link active" href="<?php echo base_url($this->session->userdata('view_slug')); ?>/pemerintahan_desa" style="color: #fff;">Pemerintahan Desa</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="<?php echo base_url($this->session->userdata('view_slug')); ?>/pembangunan_desa" style="color: #fff;">Pembangunan Desa</a>
        </li>
        <li class="nav-item">
          <a class="nav-link active" href="<?php echo base_url($this->session->userdata('view_slug')); ?>/pemberdayaan_masyarakat" style="color: #fff;">Pemberdayaan Masyarakat</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="<?php echo base_url($this->session->userdata('view_slug')); ?>/pembinaan_masyarakat" style="color: #fff;">Pembinaan Masyarakat</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="<?php echo base_url($this->session->userdata('view_slug')); ?>/bum_desa" style="color: #fff;">BUM Desa</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="<?php echo base_url($this->session->userdata('view_slug')); ?>/pkk_desa" style="color: #fff;">PKK</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="<?php echo base_url($this->session->userdata('view_slug')); ?>/potensi_desa" style="color: #fff;">Potensi Desa</a>
        </li>
        
      </ul>
      
    </div>

  </div>

</div>



</div>  
</div>
<script src="<?php echo base_url(); ?>services/HeaderVisitorService.js"></script>