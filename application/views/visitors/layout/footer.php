</body>

<!--Section: -->
  <section id="stylishcolor">
      <!-- Footer -->
      <footer class="page-footer font-small pt-4 " style="background-color: #3385ff;">
        <div class="container text-center text-md-left">
          <div class="row text-center text-md-left pb-3">
            <div class="col-md-3 col-lg-3 col-xl-3 mx-auto ">
              <p><div style="font-size:12pt; font-weight: bold; margin-top: -7%;" ><?php echo $this->session->userdata('view_desa'); ?></div></p>
              <p>Kecamatan Loceret</p>
              <a href='https://www.symptoma.it/'>search engine for diseases</a> <script type='text/javascript' src='https://www.freevisitorcounters.com/auth.php?id=1e8f4f33e92388021a712b616aafda8ce7a43728'></script>
<script type="text/javascript" src="https://www.freevisitorcounters.com/en/home/counter/634363/t/0"></script>
            </div>

            <hr class="w-100 clearfix d-md-none">

            <div class="col-md-3 col-lg-3 col-xl-3 mx-auto ">
              <p><div style="font-size:12pt; font-weight: bold; margin-top: -7%;" >Kontak</div></p>
              <p>
                <i class="fa fa-home mr-1"></i><?php echo $this->session->userdata('view_desa'); ?></p>
              <p>
                <i class="fa fa-envelope mr-1"></i><?php echo $this->session->userdata('view_alamat'); ?></p>
              <p>
                <i class="fa fa-phone mr-1"></i> <?php echo $this->session->userdata('view_no_telp'); ?></p>
            </div>
            <hr class="w-100 clearfix d-md-none">

            <!-- Grid column -->
            <div class="col-md-6 peta" style="box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19); padding: 0px 0px 0px 0px;">
                <?php echo $this->session->userdata('view_peta_desa'); ?>
            </div>
          </div>
          <hr>

          <!-- Grid row -->
          <div class="row d-flex align-items-center">

            <!-- Grid column -->
            <div class="col-md-7 col-lg-8">

              <!--Copyright-->
              <p class="text-center text-md-left">© 2019 Copyright:
                <a href="https://diskominfo.nganjukkab.go.id/">
                  <strong>E-GOV Team Dinas Kominfo Kab. Nganjuk</strong>
                </a>
              </p>

            </div>

            <!-- Grid column -->

            <!-- Grid column -->
            <div class="col-md-5 col-lg-4 ml-lg-0">


            </div>
            <!-- Grid column -->

          </div>
          <!-- Grid row -->

        </div>
        </div>
      </footer>
      <!-- Footer -->
</section>

  <a id="button"></a>

  <script type="text/javascript">
    var btn = $('#button');

    $(window).scroll(function() {
      if ($(window).scrollTop() > 300) {
        btn.addClass('show');
      } else {
        btn.removeClass('show');
      }
    });

    btn.on('click', function(e) {
      e.preventDefault();
      $('html, body').animate({scrollTop:0}, '300');
    });


  </script>
  <script type="text/javascript">
  window.onscroll = function() {myFunction()};

  var header = document.getElementById("myheader");
  var sticky = header.offsetTop;


  function myFunction() {
    if (window.pageYOffset > 350) {
      header.classList.add("sticky");
    } else {
    header.classList.remove("sticky");
    }
  } 
  
  jQuery(document).ready(function($){
  //set your google maps parameters
  var $latitude = 61.5255069,
    $longitude = -0.0836207,
    $map_zoom = 14;

  //google map custom marker icon - .png fallback for IE11
  var is_internetExplorer11= navigator.userAgent.toLowerCase().indexOf('trident') > -1;
  var $marker_url = ( is_internetExplorer11 ) ? 'https://s3-us-west-2.amazonaws.com/s.cdpn.io/148866/cd-icon-location.png' : 'https://s3-us-west-2.amazonaws.com/s.cdpn.io/148866/cd-icon-location_1.svg';
    
  //define the basic color of your map, plus a value for saturation and brightness
  var $main_color = '#2d313f',
    $saturation= -20,
    $brightness= 5;

  //we define here the style of the map
  var style= [ 
    {
      //set saturation for the labels on the map
      elementType: "labels",
      stylers: [
        {saturation: $saturation}
      ]
    },  
      { //poi stands for point of interest - don't show these lables on the map 
      featureType: "poi",
      elementType: "labels",
      stylers: [
        {visibility: "off"}
      ]
    },
    {
      //don't show highways lables on the map
          featureType: 'road.highway',
          elementType: 'labels',
          stylers: [
              {visibility: "off"}
          ]
      }, 
    {   
      //don't show local road lables on the map
      featureType: "road.local", 
      elementType: "labels.icon", 
      stylers: [
        {visibility: "off"} 
      ] 
    },
    { 
      //don't show arterial road lables on the map
      featureType: "road.arterial", 
      elementType: "labels.icon", 
      stylers: [
        {visibility: "off"}
      ] 
    },
    {
      //don't show road lables on the map
      featureType: "road",
      elementType: "geometry.stroke",
      stylers: [
        {visibility: "off"}
      ]
    }, 
    //style different elements on the map
    { 
      featureType: "transit", 
      elementType: "geometry.fill", 
      stylers: [
        { hue: $main_color },
        { visibility: "on" }, 
        { lightness: $brightness }, 
        { saturation: $saturation }
      ]
    }, 
    {
      featureType: "poi",
      elementType: "geometry.fill",
      stylers: [
        { hue: $main_color },
        { visibility: "on" }, 
        { lightness: $brightness }, 
        { saturation: $saturation }
      ]
    },
    {
      featureType: "poi.government",
      elementType: "geometry.fill",
      stylers: [
        { hue: $main_color },
        { visibility: "on" }, 
        { lightness: $brightness }, 
        { saturation: $saturation }
      ]
    },
    {
      featureType: "poi.sport_complex",
      elementType: "geometry.fill",
      stylers: [
        { hue: $main_color },
        { visibility: "on" }, 
        { lightness: $brightness }, 
        { saturation: $saturation }
      ]
    },
    {
      featureType: "poi.attraction",
      elementType: "geometry.fill",
      stylers: [
        { hue: $main_color },
        { visibility: "on" }, 
        { lightness: $brightness }, 
        { saturation: $saturation }
      ]
    },
    {
      featureType: "poi.business",
      elementType: "geometry.fill",
      stylers: [
        { hue: $main_color },
        { visibility: "on" }, 
        { lightness: $brightness }, 
        { saturation: $saturation }
      ]
    },
    {
      featureType: "transit",
      elementType: "geometry.fill",
      stylers: [
        { hue: $main_color },
        { visibility: "on" }, 
        { lightness: $brightness }, 
        { saturation: $saturation }
      ]
    },
    {
      featureType: "transit.station",
      elementType: "geometry.fill",
      stylers: [
        { hue: $main_color },
        { visibility: "on" }, 
        { lightness: $brightness }, 
        { saturation: $saturation }
      ]
    },
    {
      featureType: "landscape",
      stylers: [
        { hue: $main_color },
        { visibility: "on" }, 
        { lightness: $brightness }, 
        { saturation: $saturation }
      ]
      
    },
    {
      featureType: "road",
      elementType: "geometry.fill",
      stylers: [
        { hue: $main_color },
        { visibility: "on" }, 
        { lightness: $brightness }, 
        { saturation: $saturation }
      ]
    },
    {
      featureType: "road.highway",
      elementType: "geometry.fill",
      stylers: [
        { hue: $main_color },
        { visibility: "on" }, 
        { lightness: $brightness }, 
        { saturation: $saturation }
      ]
    }, 
    {
      featureType: "water",
      elementType: "geometry",
      stylers: [
        { hue: $main_color },
        { visibility: "on" }, 
        { lightness: $brightness }, 
        { saturation: $saturation }
      ]
    }
  ];
    
  //set google map options
  var map_options = {
        center: new google.maps.LatLng($latitude, $longitude),
        zoom: $map_zoom,
        panControl: false,
        zoomControl: false,
        mapTypeControl: false,
        streetViewControl: false,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        scrollwheel: false,
        styles: style,
    }
    //inizialize the map
  var map = new google.maps.Map(document.getElementById('google-container'), map_options);
  //add a custom marker to the map        
  var marker = new google.maps.Marker({
      position: new google.maps.LatLng($latitude, $longitude),
      map: map,
      visible: true,
    icon: $marker_url,
  });

  //add custom buttons for the zoom-in/zoom-out on the map
  function CustomZoomControl(controlDiv, map) {
    //grap the zoom elements from the DOM and insert them in the map 
      var controlUIzoomIn= document.getElementById('cd-zoom-in'),
        controlUIzoomOut= document.getElementById('cd-zoom-out');
      controlDiv.appendChild(controlUIzoomIn);
      controlDiv.appendChild(controlUIzoomOut);

    // Setup the click event listeners and zoom-in or out according to the clicked element
    google.maps.event.addDomListener(controlUIzoomIn, 'click', function() {
        map.setZoom(map.getZoom()+1)
    });
    google.maps.event.addDomListener(controlUIzoomOut, 'click', function() {
        map.setZoom(map.getZoom()-1)
    });
  }

  var zoomControlDiv = document.createElement('div');
  var zoomControl = new CustomZoomControl(zoomControlDiv, map);

    //insert the zoom div on the top left of the map
    map.controls[google.maps.ControlPosition.LEFT_TOP].push(zoomControlDiv);
});

  
  </script>


  <!--

  <footer>
    <div class="container">

      
    </div>
  </footer>-->
  <script type="text/javascript">
    var $headline = $('.headline'),
    $inner = $('.inner'),
    $nav = $('nav'),
    navHeight = 75;

$(window).scroll(function() {
  var scrollTop = $(this).scrollTop(),
      headlineHeight = $headline.outerHeight() - navHeight,
      navOffset = $nav.offset().top;

  $headline.css({
    'opacity': (1 - scrollTop / headlineHeight)
  });
  $inner.children().css({
    'transform': 'translateY('+ scrollTop * 0.4 +'px)'
  });
  if (navOffset > headlineHeight) {
    $nav.addClass('scrolled');
  } else {
    $nav.removeClass('scrolled');
  }
});
  </script>

</html>
