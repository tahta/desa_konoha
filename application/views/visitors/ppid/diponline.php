<?php $this->load->view('visitors/layout/header'); ?>
<!-- DataTables -->
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/datatables2/css/jquery.dataTables.min.css">
<section>
    <div class="container">
        <section>
            <div class="row">
                <div class="col-md-12">
                    <ul>
                        <li><b>Daftar Informasi Publik</b>
                        </li>
                    </ul>

                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-bordered" id="tblDip" width="100%" cellspacing="0"
                            style="font-size: 10px">
                            <thead>
                                <tr>
                                    <th style="vertical-align: middle; text-align: center;">No</th>
                                    <th style="vertical-align: middle; text-align: center;">Judul</th>
                                    <th style="vertical-align: middle; text-align: center;">Isi</th>
                                    <th style="vertical-align: middle; text-align: center;">File</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php 
                                foreach ($datas as $key => $value) { ?>
                                    <tr>
                                        <td style="vertical-align: middle; text-align: center;"><?= $key+1 ?></td>
                                        <td style="vertical-align: middle; text-align: center;"><?= $value->judul ?></td>
                                        <td style="vertical-align: middle; text-align: center;"><?= $value->isi ?></td>
                                        <td style="vertical-align: middle; text-align: center;"><?= '<center><a class="btn btn-info btn-sm" href='.$value->filepath.' target="_blank" title="Download">Download <i class="fa fa-download"></i></a></center>' ?></td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>

                <br>


            </div>
        </div>
    </section>
</div>
</section>
</body>

<?php $this->load->view('visitors/layout/footer'); ?>

<script type="text/javascript">
    $(document).ready(function() {
        console.log('abc');
        $('#tblDip').DataTable({
            "lengthMenu": [
            [10, 25, 50, -1],
            [10, 25, 50, "All"]
            ],
            "processing": true,
            "ordering": false,
            "columnDefs": [{
                "orderable": false
            }],
        });

    });
</script>

<!-- DataTables -->
<script src="<?php echo base_url(); ?>assets/datatables2/js/jquery.dataTables.min.js"></script>