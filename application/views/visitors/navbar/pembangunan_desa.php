<?php $this->load->view('visitors/layout/header'); ?>
<section>
<div class="container">
  <section>
    <div class="col-12" id="title">
      <h2 id="judul" style="text-align: center;">Pembangunan Desa</h2><hr align="center">
    </div> 

    <div class="col-lg-12 col-md-12 magazine-section my-5">
      <?php
          $this->db->select('berita.id as idberita, berita.tanggal as tanggal, berita.judul as judul, berita.isi as isi, berita.imgpath as imgpath, berita.created_by as created_by, berita.updated_by as updated_by, berita.created_at as created_at, berita.updated_at as updated_at, berita.isActive as isActive, users.nama as author, users.slug');
          $this->db->join('users','berita.created_by = users.id','INNER');
          $this->db->where('id_kategori', '3');
          $this->db->where('berita.isActive', '1');
          $this->db->where('users.slug', $this->uri->segment(1));
          $this->db->order_by("berita.id", "desc");
          $get_data = $this->db->get('berita')->result();
          foreach ($get_data as $val) {
        ?>
      <div class="single-news mb-2">
        <div class="row">
          <div class="col-md-3">
            <div class="view overlay rounded z-depth-1 mb-2">
              <img class="img-fluid" src="<?php echo $val->imgpath; ?>" alt="Sample image">
              <a>
                <div class="mask rgba-white-slight"></div>
              </a>
            </div>
          </div>
          <a href="berita/detail-berita/<?php echo $val->idberita; ?>">
            <div class="col-md-9">
              <p class="font-weight-bold dark-grey-text" style="font-weight: bold;"><?php echo $val->judul; ?></p>
              <p class="font-weight-bold dark-grey-text"><i class="fa fa-calendar"> <?php echo $val->tanggal; ?></i> | <i class="fa fa-user-o"> <?php echo $val->author; ?></i> </p>
              <div class="d-flex justify-content-between">
                <div class="col-11 text-truncate pl-0 mb-3">
                  <a href="berita/detail-berita/<?php echo $val->idberita; ?>" class="dark-grey-text"><?php echo substr($val->isi,0,200); ?></a>
                </div>
                <a href="berita/detail-berita/<?php echo $val->idberita; ?>"><i class="fa fa-angle-double-right"></i></a>
              </div>
            </div>
          </a>
        </div>
      </div>

      <?php } ?>

    </div>
  </section>  
</div>  
</section>
</body>

<?php $this->load->view('visitors/layout/footer'); ?>
