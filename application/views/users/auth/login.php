<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title><?php echo $tes; ?></title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link href="<?php echo base_url();?>assets/frontend/images/log.png" rel="shortcut icon" />

  <!-- Font Awesome -->
  <!--<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">-->
  <link rel="stylesheet"  href="<?php echo base_url(); ?>assets/font-awesome/css/font-awesome.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.6.2/jquery.min.js"></script>
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/dist/css/adminlte.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/iCheck/square/blue.css">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/iCheck/square/blue.css">
  <!-- toast message -->
  <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
  <link href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css" rel="stylesheet">
  <link href="<?php echo base_url();?>assets/frontend/css/style.css" rel="stylesheet">
  <link href="<?php echo base_url();?>assets/frontend/css/mdb.css" rel="stylesheet">
   <link href="https://fonts.googleapis.com/css?family=Open+Sans:700" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Squada+One" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Paytone+One" rel="stylesheet">

</head>
<?php if($this->session->flashdata('error'))
  { 
    echo '<script type="text/javascript">  
            $(document).ready(function(){toastr.error("'.$this->session->flashdata('error').'", "Error!", {timeOut: 4000})}); 
          </script>'; 
  } 
?>
<body class="hold-transition login-page">
  
<div class="login-box" style="animation: fade-in 0.80s 0.25s ease-in forwards;
}">
  <!-- /.login-logo -->
  <div class="card card-outline">
    <div class="card-header" style="background-color: #f8c300;">
      
    </div>
    <div class="card-body login-card-body">
      <?php
        $get_data = $this->db->get('kontak')->row();
      ?>
      <div class="login-logo">
         <img src="<?php echo base_url();?>assets/frontend/images/log.png" class="img"
             style="opacity: .8; width:30%">
          <h3 style="font-family: 'Squada One';"><span>LOGIN DESA</span></h3><hr>
      </div>
      <form action="<?php echo site_url('auth/validatelogin'); ?>" method="post">
        <div class="form-group has-feedback">
          <input type="username" class="form-control" name="username" placeholder="Username" style="border-left: 5px solid #f8c300;">
          <span class="fa fa-user form-control-feedback" style="color: #f8c300;">&nbsp;<span style="font-family: 'Open Sans'">Username</span></span>
        </div>
        <div class="form-group has-feedback">
          <input type="password" class="form-control" name="password" placeholder="Password" style="border-left: 5px solid #f8c300;">
          <span class="fa fa-lock form-control-feedback" style="color: #f8c300;">&nbsp;<span style="font-family: 'Open Sans'">Password</span></span>
        </div>
        <div class="row">
          <!-- /.col -->
          <div class="col-8">
            <button type="submit" class="btn" name="submit" value="login" style="background-color: #f8c300;">Sign In</button>
          </div>
          <!-- /.col -->
        </div>
      </form>
      <!-- /.social-auth-links -->
    </div>
    <!-- /.login-card-body -->
  </div>
</div>
<!-- /.login-box -->

<!-- jQuery -->
<script src="<?php echo base_url(); ?>assets/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="<?php echo base_url(); ?>assets/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- iCheck -->
<script src="<?php echo base_url(); ?>assets/iCheck/icheck.min.js"></script>
<script>
  $(function () {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass   : 'iradio_square-blue',
      increaseArea : '20%' // optional
    })
  })
</script>
</body>
</html>
