<?php 
  $this->view('users/layout/header');
  $this->view('users/layout/navbar');
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper" style="border-top: 2px solid #f8c300; border-bottom: 2px solid #f8c300;">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0 dinsos-color">Daftar Galeri Kegiatan</h1>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>admin">Home</a></li>
            <li class="breadcrumb-item active">Galeri</li>
          </ol>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>

 <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-12">
        <div class="card card-qibul card-outline">
          <div class="card-header">
            <div class="col-sm-2 float-left">
              <button type="button" data-toggle="modal" data-target="#insert-modal" class="btn btn-qibul btn-sm"><i class="fa fa-plus-square-o"></i> &nbsp; Tambah Data</button>
            </div>
            <!--
            <div class="col-sm-3 float-sm-right">
              <div class="input-group input-group-sm">
              <input type="text" class="form-control">
              <span class="input-group-append">
                <button type="button" class="btn btn-info btn-flat"><i class="fa fa-search"></i></button>
              </span>
            </div>
            </div>
            -->
          </div>
          <div class="card-body">
            <table id="example2" class="table table-bordered table-hover table-responsive">
              <thead>
              <tr>
                <th style="width: 5%; vertical-align">No</th>
                <th style="width: 15%vertical-align">Tanggal</th>
                <th style="vertical-align">Nama Kegiatan</th>
                <th style="width: 15%vertical-align">Status</th>
                <th style="width: 20%; vertical-align">Action</th>
              </tr>
              </thead>
              <tbody>
              </tbody>
            </table>
          </div>
        </div>
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<!--insert modal-->
<div class="modal fade" id="insert-modal" role="dialog">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header modal-header-qibul">
        <h3 class="modal-title">Form Galeri</h3>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      </div>
      <form action="" method="post" id="newsform">
        <div class="modal-body form">
          <div class="row mb-2">
            <div class="col-sm-3"><label class="control-label" for="title">Tanggal</label></div>
            <div class="col-sm-9">
              <input type="text" class="form-control" id="tanggal" name="tanggal">
            </div>
          </div>
          <div class="row mb-2">
            <div class="col-sm-3"><label class="control-label" for="title">Nama Kegiatan</label></div>
            <div class="col-sm-9"><input class="form-control" type="text" placeholder="Nama Kegiatan" name="judul" id="judul"></div>
          </div>
          <div class="row mb-2">
            <div class="col-sm-3"><label class="control-label" for="title">Foto Kegiatan</label></div>
            <div class="col-sm-9 float-right"><input type="file" class="form-control" name="files[]" id="file" multiple="multiple">
             <span><i style="color: red; font-size: 9pt">*) jpg/png/jpeg kurang dari 2 MB (Bisa upload lebih dari 1 gambar)</i></span></div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" id="btnSave" class="btn btn-qibul" onclick="insert()">Simpan</button>
          <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
        </div>
      </form>
    </div>
  </div>
</div>
<!---->

<!--preview modal-->
<div class="modal fade" id="preview-modal" role="dialog">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header modal-header-qibul">
        <h3 class="modal-title">Detail Galeri</h3>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      </div>
      <div class="modal-body" style="text-align: center;">
        <img id="image-gallery-image" class="img-responsive" style="width: 50%" src="">
      </div>
      <div class="modal-footer text-justify">
        <button type="button" id="show-previous-image" class="btn btn-qibul" title="Previous"><i class="fa fa-arrow-left"></i></button>
        <button type="button" id="show-next-image" class="btn btn-qibul" title="Next"><i class="fa fa-arrow-right"></i></button>
      </div>
    </div>
  </div>
</div>
<!---->

<!--edit modal-->
<div class="modal fade" id="update-modal" role="dialog">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header modal-header-qibul">
        <h3 class="modal-title">Edit Galeri</h3>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      </div>
      <form action="" method="post" id="newsform">
        <div class="modal-body form">
          <div class="row mb-2">
            <div class="col-sm-3"><label class="control-label" for="title">Tanggal</label></div>
            <div class="col-sm-9">
              <input type="text" class="form-control" id="tanggal2" name="tanggal">
              <input type="hidden" class="form-control" id="id" name="id">
            </div>
          </div>
          <div class="row mb-2">
            <div class="col-sm-3"><label class="control-label" for="title">Nama Kegiatan</label></div>
            <div class="col-sm-9"><input class="form-control" type="text" placeholder="Nama Agenda" name="judul" id="judul"></div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" id="btnSave" class="btn btn-qibul" onclick="update_data()">Simpan</button>
          <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
        </div>
      </form>
    </div>
  </div>
</div>
<!---->

<script type="text/javascript">
  var url = "galeri/ambil-data";
</script>
<script src="<?php echo base_url(); ?>services/GaleriService.js"></script>
<?php  
  $this->view('users/layout/footer');
?>