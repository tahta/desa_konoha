<?php 
  $this->view('users/layout/header');
  $this->view('users/layout/navbar');
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper" style="border-top: 2px solid #f8c300; border-bottom: 2px solid #f8c300;">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0 dinsos-color">Profil Sejarah</h1>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>admin">Home</a></li>
            <li class="breadcrumb-item active">Profil Sejarah</li>
          </ol>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>

 <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-12">
        <div class="card card-qibul card-outline">
          <div class="card-header">
          </div>
          <div class="card-body">
            <table id="example2" class="table table-bordered table-hover">
              <thead>
              <tr>
                <th style="width: 5%; vertical-align">No</th>
                <th style="width: 15%; vertical-align">Judul</th>
                <th style="width: 30%; vertical-align">Isi</th>
                <th style="width: 15%; vertical-align">Action</th>
              </tr>
              </thead>
              <tbody id="show_data">
              </tbody>
            </table>
          </div>
        </div>
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<!--update Modal-->
<div class="modal fade" id="update-modal" role="dialog">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header modal-header-qibul">
        <h3 class="modal-title">Form Edit Profil Sejarah</h3>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      </div>
      <form action="" method="post">
        <div class="modal-body form">
          <input class="form-control" type="hidden" name="id">
          <div class="row mb-2">
            <div class="col-sm-3"><label class="control-label" for="title">Profil Sejarah</label></div>
            <div class="col-sm-9 float-right"><textarea name="isi" id="editor1" rows="10" cols="80"></textarea></div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" id="btnSave" class="btn btn-qibul" onclick="update_data()">Simpan</button>
          <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
        </div>
      </form>
    </div>
  </div>
</div>
<!---->

<!--Detail Modal-->
<div class="modal fade" id="detail-modal" role="dialog">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header modal-header-qibul">
        <h3 class="modal-title">Detail Profil Sejarah</h3>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      </div>
      <div class="modal-body form">
        <div class="col-12" id="title">
          <h4 style="text-align: center;">Ini Judul</h4>
        </div>
        <div class="col-12" id="isi">
          <div id="uisi"></div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Keluar</button>
      </div>
    </div>
  </div>
</div>
<!---->

<script type="text/javascript">
  var url = "profil-sejarah/ambil-data";
  CKEDITOR.replace( 'editor1' );
</script>
<script src="<?php echo base_url(); ?>services/ProfilSejarahService.js"></script>
<?php  
  $this->view('users/layout/footer');
?>