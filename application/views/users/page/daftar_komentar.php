<?php 
  $this->view('users/layout/header');
  $this->view('users/layout/navbar');
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper" style="border-top: 2px solid #f8c300; border-bottom: 2px solid #f8c300;">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0 dinsos-color">Daftar Komentar</h1>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>admin">Home</a></li>
            <li class="breadcrumb-item active">Daftar Komentar</li>
          </ol>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>

 <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-12">
        <div class="card card-qibul card-outline">
          <div class="card-header">
          </div>
          <div class="card-body">
            <table id="example2" class="table table-bordered table-hover">
              <thead>
              <tr>
                <th style="width: 5%; vertical-align">No</th>
                <th style="width: 20%; vertical-align">Judul</th>
                <th style="width: 10%; vertical-align">Nama</th>
                <th style="width: 10%; vertical-align">Email</th>
                <th style="width: 10%; vertical-align">Isi Komentar</th>
                <th style="width: 15%; vertical-align">Action</th>
              </tr>
              </thead>
              <tbody id="show_data">
              </tbody>
            </table>
          </div>
        </div>
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<script type="text/javascript">
  var url = "daftar-komentar/ambil-data";
</script>
<script src="<?php echo base_url(); ?>services/KomentarService.js"></script>
<?php  
  $this->view('users/layout/footer');
?>