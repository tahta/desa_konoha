<?php 
$this->view('users/layout/header');
$this->view('users/layout/navbar');
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper" style="border-top: 2px solid #f8c300; border-bottom: 2px solid #f8c300;">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0 dinsos-color">DIP Online PPID Desa</h1>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>admin">Home</a></li>
            <li class="breadcrumb-item active">DIP Online PPID Desa</li>
          </ol>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-12">
        <div class="card card-qibul card-outline">
          <div class="card-header">
           <div class="col-sm-2 float-left">
            <button type="button" onclick="add_data()" class="btn btn-qibul btn-flat"><i class="fa fa-plus-square-o"></i> &nbsp; Tambah Data</button>
          </div>
        </div>
        <div class="card-body">
          <table id="tblIsu" class="table table-bordered table-hover">
            <thead>
              <tr>
                <th style="width: 5%; vertical-align">No</th>
                <th style="width: 20%; vertical-align">Judul</th>
                <th style="width: 25%; vertical-align">Isi</th>
                <th style="width: 10%; vertical-align">File</th>
                <th style="width: 10%; vertical-align">Dibuat Tgl.</th>
                <th style="width: 10%; vertical-align">Action</th>
              </tr>
            </thead>
            <tbody id="show_data">
            </tbody>
          </table>
        </div>
      </div>
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->
</section>
<!-- /.content -->
</div>
<!-- /.content-wrapper -->

<!--update Modal-->
<div class="modal fade" id="modal_form" role="dialog">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header modal-header-qibul">
        <h3 class="modal-title">Form DIP Online PPID Desa</h3>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      </div>
      <form action="#" method="post" id="form" enctype="multipart/form-data">
        <div class="modal-body form">
          <div class="row mb-2">
            <div class="col-sm-2"><label class="control-label" for="title">Judul</label></div>
            <div class="col-sm-10"><input class="form-control" type="text" placeholder="Judul" name="judul" id="judul"></div>
          </div>
          <div class="row mb-2">
            <input type="hidden" class="form-control" name="id">
            <div class="col-sm-2"><label class="control-label" for="title">Lampiran File</label></div>
            <div class="col-sm-10 float-right"><input type="file" class="form-control" name="file" id="file">
              <span><i style="color: red; font-size: 8pt">* File : jpg/png/jpeg/pdf/doc/docx</i></span>
            </div>
          </div>
          <div class="row mb-2">
            <div class="col-sm-2"><label class="control-label" for="title">Isi</label></div>
            <div class="col-sm-10 float-right"><textarea name="isi" id="isi" rows="10" cols="80"></textarea></div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" id="btnSave" class="btn btn-qibul" onclick="save()">Simpan</button>
          <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
        </div>
      </form>
    </div>
  </div>
</div>
<!---->

<?php  
$this->view('users/layout/footer');
?>

<script type="text/javascript">
var save_method; //for save method string
var table;

$(document).ready(function() {
  $('#modal_form').on('hidden.bs.modal', function () {
    $("input").prop('readonly', false);
    $('#btnSave').show();
    $('#judul').removeAttr('disabled');
    $('#isi').removeAttr('disabled');
  });
    //datatables
    table = $('#tblIsu').DataTable({ 

        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        "order": [], //Initial no order.

        // Load data for the table's content from an Ajax source
        "ajax": {
          "url": "dip_online/ambil-data",
          "type": "POST"
        },

        //Set column definition initialisation properties.
        "columnDefs": [
        { 
          "targets": [ -1 ]
        },
        ],

      });

  });
function add_data()
{
  save_method = 'add';
    $('#form')[0].reset(); // reset form on modals
    // $('#result').text(' Silahkan memilih kategori').css({'color': 'red','font-weight': 'bold'});
    // $('#btnSave').hide();
    $('#modal_form').modal('show'); // show bootstrap modal
  }

  function save()
  {
    $('#btnSave').text('saving...'); //change button text
    $('#btnSave').attr('disabled',true); //set button disable 
    var url;

    if(save_method == 'add') {
      url = "dip_online/insert-data";
    } else {
      url = "dip_online/edit-data";
    }
    // ajax adding data to database
    var data = new FormData($('#form')[0]);

    $.ajax({
      url : url,
      type: "POST",
      data: data,
      async: false,
      cache: false,
      contentType: false,
      processData: false,

      success: function(data)
      {
        // console.log(data);
            if(data.status) //if success close modal and reload ajax table
            {
              $('#modal_form').modal('hide');
              reload_table();
            }
            else
            {
              // alert('Error embuh opo iki');
            }
            $('#btnSave').text('save'); //change button text
            $('#btnSave').attr('disabled',false); //set button enable 
            location.reload();
          },
          error: function (jqXHR, textStatus, errorThrown)
          {
            alert('Error adding / update data');
            $('#btnSave').text('save'); //change button text
            $('#btnSave').attr('disabled',false); //set button enable 

          }
        });
  }

  function update(id){
    $('#form')[0].reset(); // reset form on modals
    $.ajax({
      dataType: 'json',
      type: 'POST',
      url: 'dip_online/ambil-data-by-id/'+id,
      success: function (data) {
        $('[name="id"]').val(data.data.id);
        $('[name="judul"]').val(data.data.judul);
        $('[name="isi"]').val(data.data.isi);
        $('#modal_form').modal('show');
        console.log(data.data.isi);
      },
      error: function ( data ) {
        console.log('error');
      }
    });
  }

  function detail(id){
    $('#form')[0].reset(); // reset form on modals
    $("input").prop('readonly', true);
    $('#judul').prop("disabled", true);
    $('#isi').prop("disabled", true);
    $('#jenis_isu').prop('disabled',true);
    $('#btnSave').hide();
    $.ajax({
      dataType: 'json',
      type: 'POST',
      url: 'dip_online/ambil-data-by-id/'+id,
      success: function (data) {
        $('[name="id"]').val(data.data.id);
        $('[name="judul"]').val(data.data.judul);
        $('[name="isi"]').val(data.data.isi);
        // $('#linkgambar').attr("href",data.data.filepath);
        // $('#gambar').attr("src",data.data.filepath);
        $('#modal_form').modal('show');
        console.log(data.data.isi);
      },
      error: function ( data ) {
        console.log('error');
      }
    });
  }

  function hapus(id){
  $.ajax({
    dataType: 'json',
    url: 'dip_online/ambil-data-by-id/'+id,
    success: function ( data) {
      // console.log(data.data['isActive']); //view the returned json in the console
      var isactive = data.data['isActive'];
      $.confirm({
        title : 'Hapus Data',
        content : 'Apakah Anda Yakin Untuk Menghapus Data?',
        buttons: {
        hapus: {
            btnClass: 'btn-blue',
            text:'Hapus',
            action: function(){
              $.ajax({
                dataType: 'json',
                type:'POST',
                url: 'dip_online/remove/'+id,
                data:{
                  id:id,
                  isActive:0
                }
              }).done(function(data){
                $(".modal").modal('hide');
                location.reload();
                toastr.success('Item Deleted Successfully.', 'Success Alert', {timeOut: 5000});
              });
            }
        },
        batal: {
            btnClass: 'btn-red any-other-class',
            text:'Batal'
          },
        }
      });   
    },
    error: function ( data ) {
      console.log('error');
    }
  });    
}

  </script>