<?php 
$this->view('users/layout/header');
$this->view('users/layout/navbar');
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper" style="border-top: 2px solid #f8c300; border-bottom: 2px solid #f8c300;">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-12">
          <ol class="breadcrumb float-sm-left">
            <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>admin">Dashboard</a></li>
            <li class="breadcrumb-item active">Import Data COVID-19</li>
          </ol>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-12">
        <div class="card card-qibul card-outline">
          <div class="card-header">
            <h5 class="m-0 dinsos-color">Import Data COVID-19 (*.XLXS)</h5>
          </div>
          <div class="card-body">
            <form action="" method="post" id="reset">
              <div class="modal-body form">

                <div class="row mb-2">
                  <div class="col-sm-8"><label class="control-label" for="title">Pilih File (*.xlsx) <br>(* Format Excel Harus Sesuai , Download Format <a href="<?php echo base_url(); ?>assets/formlaporancorona.xlsx">Disini !)</a></label></div>
                  <div class="col-sm-4"></div>
                </div>

                <div class="row mb-2">
                  <div class="col-sm-8"><input type="file" name="xlsx" id="xlsx" accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"></div>
                  <div class="col-sm-4"></div>
                </div>

                <div class="modal-footer">
                  <a class="btn btn-success" id="import" style="color: #ffff;">Import</a>
                </div>
              </div>
            </form>

          </div>

          <div class="col-lg-12" id="progress-div" style="display:none">
            <!-- Basic Card Example -->
            <div class="card shadow mb-4">
              <div class="card-body">
                <h5 align="center" id="header-progress">Hasil Import Bisa Dilihat Di Menu Laporan</h5>
                <br>
                <div class="progress">
                  <div class="progress-bar" id="progress_bar" role="progressbar" style="width: 0%;"
                  aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                </div>
                <br>
                * <span id="label-uraian" style="font-size: 10px"></span>
              </div>
            </div>
          </div>


        </div>
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<script type="text/javascript">
  $('#import').on('click', function() {
            // Show Progress Bar
            var progres = 0;
            $('#progress-div').show();
            $('#progress_bar').css('width', '0%');
            $('#header-progress').text("Hasil Import Bisa Dilihat Di Menu Laporan").css('color', '#858796');
            // $('#label-uraian').text("Please Wait").css('color', '#858796');

            var excel = $('#xlsx').prop('files')[0];
            var form_data = new FormData();

            form_data.append('excel', excel);
            $.ajax({
                url: '<?= base_url('admin/importcovid19/import') ?>', // point to server-side PHP script 
                dataType: 'text', // what to expect back from the PHP script, if anything
                cache: false,
                contentType: false,
                processData: false,
                data: form_data,
                type: 'post',
                success: function(data) {
                  console.log(data);
                  var filename = data;
                  var temp = '<?= base_url('admin/importcovid19/insert') ?>';
                  progres = 0;
                  var totalImport = 0;
                  for (i = 1; i < 2; i++) {
                    (function (i){
                      url = "<?= base_url('admin/importcovid19/insert') ?>" + '?filename=' + filename;
                      $.get(url, function(data) {
                                    // console.log(JSON.parse(data));
                                  });
                    })(i);
                  }
                },
              });
          });
        </script>

        <?php  
        $this->view('users/layout/footer');
        ?>