<?php 
$this->view('users/layout/header');
$this->view('users/layout/navbar');
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper" style="border-top: 2px solid #f8c300; border-bottom: 2px solid #f8c300;">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-12">
          <ol class="breadcrumb float-sm-left">
            <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>admin">Dashboard</a></li>
            <li class="breadcrumb-item active">Reset Password</li>
          </ol>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-12">
        <div class="card card-qibul card-outline">
          <div class="card-header">
            <h5 class="m-0 dinsos-color">Reset Password</h5>
          </div>
          <div class="card-body">
            <form action="" method="post" id="reset">
              <div class="modal-body form">
                <div class="row mb-2">
                  <div class="col-sm-2"><label class="control-label" for="title">Ubah Password</label></div>
                  <div class="col-sm-10"><input class="form-control" type="password" placeholder="Ubah Password" name="password" id="password"></div>
                </div>
                <div class="row mb-2">
                  <div class="col-sm-2"><label class="control-label" for="title">Konfirmasi Password</label></div>
                  <div class="col-sm-10"><input class="form-control" type="password" placeholder="Konfirmasi Password" id="password_ulang"></div>
                </div>
                <div class="modal-footer">
                  <button type="button" id="btnSave" class="btn btn-qibul" onclick="resetPass()">Simpan</button>
                  <button type="button" class="btn btn-danger" onclick="location.reload();">Batal</button>
                </div>
              </form>

            </div>
          </div>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <script language="JavaScript" src="<?php echo base_url();?>assets/library/md5.js"></script>
  <script src="<?php echo base_url(); ?>services/ResetPass.js"></script>
  <?php  
  $this->view('users/layout/footer');
  ?>