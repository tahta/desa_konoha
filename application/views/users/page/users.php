<?php 
  $this->view('users/layout/header');
  $this->view('users/layout/navbar');
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper" style="border-top: 2px solid #f8c300; border-bottom: 2px solid #f8c300;">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0 dinsos-color">Daftar Pengguna</h1>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>admin">Home</a></li>
            <li class="breadcrumb-item active">Pengaturan Pengguna</li>
          </ol>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>

 <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-12">
        <div class="card card-qibul card-outline">
          <div class="card-header">
            <div class="col-sm-2 float-left">
              <button type="button" data-toggle="modal" data-target="#insert-modal" class="btn btn-qibul btn-sm"><i class="fa fa-plus-square-o"></i> &nbsp; Tambah Data</button>
            </div>
            <!--
            <div class="col-sm-3 float-sm-right">
              <div class="input-group input-group-sm">
              <input type="text" class="form-control" name="search">
              <span class="input-group-append">
                <button type="button" class="btn btn-info btn-flat"><i class="fa fa-search"></i></button>
              </span>
            </div>
            </div>
            -->
          </div>
          <div class="card-body">
            <table id="example2" class="table table-bordered table-hover">
              <thead>
              <tr>
                <th style="width: 5%; vertical-align">No</th>
                <th style="width: 30%; vertical-align">Nama Pengguna</th>
                <th style="width: 30%; vertical-align">Username</th>
                <th style="width: 10%; vertical-align">Status</th>
                <th style="width: 20%; vertical-align">Action</th>
              </tr>
              </thead>
              <tbody>
              </tbody>
            </table>
          </div>
        </div>
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<!--Add Modal-->
<div class="modal fade" id="insert-modal" role="dialog">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h3 class="modal-title">Tambah Data User</h3>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      </div>
      <form action="pengaturan-user/insert-data" method="post">
        <div class="modal-body form">
          <div class="row mb-2">
            <div class="col-sm-3"><label class="control-label" for="title">Nama Pengguna</label></div>
            <div class="col-sm-9"><input class="form-control" type="text" placeholder="Nama Pengguna" name="nama"></div>
          </div>
          <div class="row mb-2">
            <div class="col-sm-3"><label class="control-label" for="title">Username</label></div>
            <div class="col-sm-9"><input class="form-control" type="text" placeholder="Username" name="username"></div>
          </div>
          <div class="row mb-2">
            <div class="col-sm-3"><label class="control-label" for="title">Password</label></div>
            <div class="col-sm-9 float-right"><input class="form-control" type="text" placeholder="Password" name="password"></div>
          </div>
          <div class="row mb-2">
            <div class="col-sm-3"><label class="control-label" for="title">Role</label></div>
            <div class="col-sm-9 float-right">
              <select class="form-control" name="roleid">
                <option value="0" disabled>-PILIH ROLE-</option>
                <option value="1">Super Admin</option>
                <option value="2">Admin</option>
              </select>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" id="btnSave" class="btn btn-qibul" onclick="insert()">Simpan</button>
          <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
        </div>
      </form>
    </div>
  </div>
</div>
<!---->

<!--Update Modal-->
<div class="modal fade" id="update-modal" role="dialog">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h3 class="modal-title">Ubah Data User</h3>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      </div>
      <form action="" method="post">
        <div class="modal-body form">
          <div class="row mb-2">
            <div class="col-sm-3"><label class="control-label" for="title">Nama Pengguna</label></div>
            <div class="col-sm-9">
              <input class="form-control" type="text" placeholder="Nama Pengguna" name="nama">
              <input class="form-control" type="hidden" name="id">
              <input class="form-control" type="hidden" name="password">
              <input class="form-control" type="hidden" name="isactive">
            </div>
          </div>
          <div class="row mb-2">
            <div class="col-sm-3"><label class="control-label" for="title">Username</label></div>
            <div class="col-sm-9"><input class="form-control" type="text" placeholder="Username" name="username"></div>
          </div>
          <div class="row mb-2">
            <div class="col-sm-3"><label class="control-label" for="title">Role</label></div>
            <div class="col-sm-9 float-right">
              <select class="form-control" name="roleid">
                <option value="0" disabled>-PILIH ROLE-</option>
                <option value="1">Super Admin</option>
                <option value="2">Admin</option>
              </select>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" id="btnSave" class="btn btn-qibul" onclick="update_data()">Simpan</button>
          <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
        </div>
      </form>
    </div>
  </div>
</div>
<!---->

<script language="JavaScript" src="<?php echo base_url();?>assets/library/md5.js"></script>
<script type="text/javascript">
  var url = "pengaturan-user/ambil-data";
</script>
<script src="<?php echo base_url(); ?>services/UserService.js"></script>
<?php  
  $this->view('users/layout/footer');
?>