<?php 
$this->view('users/layout/header');
$this->view('users/layout/navbar');
?>

<div class="content-wrapper" style="border-top: 2px solid #f8c300; border-bottom: 2px solid #f8c300;">
	<!-- Begin Page Content -->

	<section class="content-header">
		<div class="container-fluid">
			<div class="row mb-2">
				<div class="col-sm-12">
					<ol class="breadcrumb float-sm-left">
						<li class="breadcrumb-item"><a href="<?php echo base_url(); ?>admin">Dashboard</a></li>
						<li class="breadcrumb-item active">Laporan Data COVID-19</li>
					</ol>
				</div>
			</div>
		</div><!-- /.container-fluid -->
	</section>

	<!-- Main content -->
	<section class="content">
		<div class="row">
			<?php
			// $conn=$this->load->database('covid19',TRUE);
			// $otg = $conn->query('SELECT nama FROM `covid` WHERE otg IS NOT NULL ');
			// $odp = $conn->query('SELECT nama FROM `covid` WHERE odp IS NOT NULL');
			// $pdp = $conn->query('SELECT nama FROM `covid` WHERE pdp IS NOT NULL');
			// $dirawat = $conn->query('SELECT nama FROM `covid` WHERE masih_dirawat IS NOT NULL');
			// $sembuh = $conn->query('SELECT nama FROM `covid` WHERE sembuh IS NOT NULL');
			// $meninggal = $conn->query('SELECT nama FROM `covid` WHERE meninggal IS NOT NULL');
			?>

			<!-- TOTAL OTG -->

			<div class="col-lg-4 col-6">
				<!-- small box -->
				<div class="small-box bg-warning">
					<div class="inner">
						<h5>TOTAL OTG</h5>
						<h3><?php echo count($otg) ;?></h3>
					</div>
					<div class="icon">
						<i class="fa fa-user"></i>
					</div>
				</div>
			</div>
			<!-- END TOTAL OTG -->

			<!-- TOTAL ODP -->
			<div class="col-lg-4 col-6">
				<!-- small box -->
				<div class="small-box bg-warning">
					<div class="inner">
						<h5>TOTAL ODP</h5>
						<h3><?php echo count($odp) ;?></h3>
					</div>
					<div class="icon">
						<i class="fa fa-user"></i>
					</div>
				</div>
			</div>
			<!-- END TOTAL ODP -->

			<!-- TOTAL PDP -->
			<div class="col-lg-4 col-6">
				<!-- small box -->
				<div class="small-box bg-warning">
					<div class="inner">
						<h5>TOTAL PDP</h5>
						<h3><?php echo count($pdp) ;?></h3>
					</div>
					<div class="icon">
						<i class="fa fa-user"></i>
					</div>
				</div>
			</div>
			<!-- END TOTAL PDP -->

			<!-- TOTAL DIRAWAT -->
			<div class="col-lg-4 col-6">
				<!-- small box -->
				<div class="small-box bg-info">
					<div class="inner">
						<h5>TOTAL DIRAWAT</h5>
						<h3><?php echo count($dirawat) ;?></h3>
					</div>
					<div class="icon">
						<i class="fa fa-user"></i>
					</div>
				</div>
			</div>
			<!-- END TOTAL DIRAWAT -->

			<!-- TOTAL SEMBUH-->
			<div class="col-lg-4 col-6">
				<!-- small box -->
				<div class="small-box bg-success">
					<div class="inner">
						<h5>TOTAL SEMBUH</h5>
						<h3><?php echo count($sembuh) ;?></h3>
					</div>
					<div class="icon">
						<i class="fa fa-user"></i>
					</div>
				</div>
			</div>
			<!-- END TOTAL SEMBUH -->

			<!-- TOTAL MENINGGAL-->
			<div class="col-lg-4 col-6">
				<!-- small box -->
				<div class="small-box bg-danger">
					<div class="inner">
						<h5>TOTAL MENINGGAL</h5>
						<h3><?php echo count($meninggal);?></h3>
					</div>
					<div class="icon">
						<i class="fa fa-user"></i>
					</div>
				</div>
			</div>
			<!-- END TOTAL MENINGGAL -->
		</div>
	</section>

	<!-- <div class="content-wrapper" style="border-top: 2px solid #2c3e50; border-bottom: 2px solid #2c3e50;"> -->
	<section class="content">

		<!-- DataTales Example -->
		<div class="card card-qibul card-outline">
			<div class="card-body">
				<div class="table-responsive">
					<table class="table table-bordered" id="tblCorona" width="100%" cellspacing="0" style="font-size: 10px">
						<thead>
							<tr>
								<th>No</th>
								<th>NIK</th>
								<th>Telepon</th>
								<th>Nama</th>
								<th>Umur (Tahun)</th>
								<th>Jenis Kelamin (L/P)</th>
								<th>Negara / Kota Asal Kedatangan</th>
								<th>Kecamatan</th>
								<th>Desa</th>
								<th>Alamat</th>
								<th>Kecamatan Tujuan</th>
								<th>Desa Tujuan</th>
								<th>Alamat Tujuan</th>
								<th>NIK Tujuan</th>
								<th>Nama Tujuan</th>
								<th>Suhu Tubuh</th>
								<th>Keluhan</th>
								<th>Dirawat RS</th>
								<th>Tidak Dirawat</th>
								<th>RS Merawat</th>
								<th>Faktor Resiko</th>
								<th>Diagnosis</th>
								<th>Kode ICDX</th>
								<th>Tanggal RDT</th>
								<th>Tanggal SWAB 1</th>
								<th>Tanggal SWAB 2</th>
								<th>Hasil Lab</th>
								<th>Hasil Rontgen</th>
								<th>Hasil Rapid Test</th>
								<th>Hasil SWAB</th>
							</tr>
						</thead>
						<tbody>
							<?php 
							foreach ($datas as $key => $value) { ?>
								<tr>
									<td><?= $key+1 ?></td>
									<td><?= $value['nik'] ?></td>
									<td><?= $value['telp'] ?></td>
									<td><?= $value['nama'] ?></td>
									<td><?= $value['umur'] ?></td>
									<td><?= $value['jenis_kelamin'] ?></td>
									<td><?= $value['asal_kedatangan'] ?></td>
									<td><?= $value['kecamatan'] ?></td>
									<td><?= $value['desa'] ?></td>
									<td><?= $value['alamat'] ?></td>
									<td><?= $value['kecamatan_tujuan'] ?></td>
									<td><?= $value['desa_tujuan'] ?></td>
									<td><?= $value['alamat_tujuan'] ?></td>
									<td><?= $value['nik_tujuan'] ?></td>
									<td><?= $value['nama_tujuan'] ?></td>
									<td><?= $value['suhu_tubuh'] ?></td>
									<td><?= $value['keluhan'] ?></td>
									<td><?= $value['rs_ya'] ?></td>
									<td><?= $value['rs_tidak'] ?></td>
									<td><?= $value['rs_merawat'] ?></td>
									<td><?= $value['faktor_resiko'] ?></td>
									<td><?= $value['diagnosis'] ?></td>
									<td><?= $value['kode_icdx'] ?></td>
									<td><?= $value['rdt'] ?></td>
									<td><?= $value['swab1'] ?></td>
									<td><?= $value['swab2'] ?></td>
									<td><?= $value['lab'] ?></td>
									<td><?= $value['rontgen'] ?></td>
									<td><?= $value['rapid_test'] ?></td>
									<td><?= $value['swab'] ?></td>
								</tr>
								<?php }
								?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</section>
	</div>
	<!-- /.container-fluid -->
	<!-- End of Main Content -->
	<?php  
	$this->view('users/layout/footer');
	?>
	<script type="text/javascript">
		$( document ).ready(function() {
			console.log('abc');
			$('#tblCorona').DataTable();
		});
	</script>