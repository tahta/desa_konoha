<?php 
  $this->view('users/layout/header');
  $this->view('users/layout/navbar');
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper" style="border-top: 2px solid #10a522; border-bottom: 2px solid #10a522;">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0 dinsos-color">Daftar Pengumuman</h1>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>admin">Home</a></li>
            <li class="breadcrumb-item active">Pengumuman</li>
          </ol>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>

 <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-12">
        <div class="card card-qibul card-outline">
          <div class="card-header">
            <div class="col-sm-2 float-left">
              <button type="button" data-toggle="modal" data-target="#insert-modal" class="btn btn-qibul btn-flat"><i class="fa fa-plus-square-o"></i> &nbsp; Tambah Data</button>
            </div>
          </div>
          <div class="card-body">
            <table id="example2" class="table table-bordered table-hover">
              <thead>
              <tr>
                <th style="width: 5%; vertical-align">No</th>
                <th style="vertical-align">Judul</th>
                <th style="width: 15%; vertical-align">File</th>
                <th style="width: 10%; vertical-align">Status</th>
                <th style="width: 15%; vertical-align">Action</th>
              </tr>
              </thead>
              <tbody>
              </tbody>
            </table>
          </div>
        </div>
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<!--insert modal-->
<div class="modal fade" id="insert-modal" role="dialog">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header modal-header-qibul">
        <h3 class="modal-title">Form Pengumuman</h3>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      </div>
      <form action="" method="post" id="newsform">
        <div class="modal-body form">
          <div class="row mb-2">
            <div class="col-sm-3"><label class="control-label" for="title">Judul Pengumuman</label></div>
            <div class="col-sm-9"><input class="form-control" type="text" placeholder="Pengumuman" name="judul" id="judul"></div>
          </div>
          <div class="row mb-2">
            <div class="col-sm-3"><label class="control-label" for="title">Lampiran File</label></div>
            <div class="col-sm-9 float-right"><input type="file" class="form-control" name="file" id="file">
             <span><i style="color: red; font-size: 8pt">* File : jpg/png/jpeg/pdf/doc/docx</i></span></div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" id="btnSave" class="btn btn-qibul" onclick="insert()">Simpan</button>
          <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
        </div>
      </form>
    </div>
  </div>
</div>
<!---->

<!--insert modal-->
<div class="modal fade" id="update-modal" role="dialog">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header modal-header-qibul">
        <h3 class="modal-title">Form Pengumuman</h3>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      </div>
      <form action="" method="post" id="newsform">
        <div class="modal-body form">
          <div class="row mb-2">
            <div class="col-sm-3"><label class="control-label" for="title">Judul Pengumuman</label></div>
            <div class="col-sm-9">
              <input class="form-control" type="text" placeholder="Pengumuman" name="judul" id="judul">
              <input class="form-control" type="hidden" name="id" id="id">
            </div>
          </div>
          <div class="row mb-2">
            <div class="col-sm-3"><label class="control-label" for="title">Lampiran File</label></div>
            <div class="col-sm-9 float-right"><input type="file" class="form-control" name="file" id="file2">
             <span><i style="color: red; font-size: 8pt">* File : jpg/png/jpeg/pdf/doc/docx</i></span></div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" id="btnSave" class="btn btn-qibul" onclick="update_data()">Simpan</button>
          <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
        </div>
      </form>
    </div>
  </div>
</div>
<!---->


<script src="<?php echo base_url(); ?>services/PengumumanService.js"></script>
<?php  
  $this->view('users/layout/footer');
?>