  <style>
    .menusamping{
      background-color: #ffffff;
      color: #000000;
    }

    .kec-color{
      color: #000000;
    }

  </style>
  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-ligth-success elevation-4 ">
    <!-- Brand Logo -->
    <div style="border-bottom: 2px solid #f8c300;">
      <?php
      $get_data = $this->db->get('kontak')->row();
      ?>
      <a class="brand-link">
        <img src="<?php echo base_url();?>assets/frontend/images/log.png" alt="" class="brand-image img-circle elevation-3"
        style="opacity: .8">
        <span class="brand-text font-weight-light" style="font-family: 'Squada One'; font-size: 15pt">
          <strong><span>
            <?php 
            if($this->session->userdata('desa') != ""){
              echo $this->session->userdata('desa');
            }else{
              echo "SUPER ADMIN";
            } 
            ?>
          </span></strong>
        </span>
      </a>
    </div>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
            with font-awesome or any other icon font library -->
            <li class="nav-item has-treeview menu-open">
              <a href="<?php echo base_url(); ?>admin" class="nav-link active" style="background-color: #f8c300;">
                <i class="nav-icon fa fa-dashboard"></i>
                <p>
                  Dashboard
                </p>
              </a>
            </li>

            <?php if($this->session->userdata('role')==1 || $this->session->userdata('role')==2) {?>
              <li class="nav-item has-treeview menusamping">
                <a href="#" class="nav-link">
                  <i class="fa fa-university kec-color"></i>
                  <p>
                    Profil
                    <i class="right fa fa-angle-left"></i>
                  </p>
                </a>
                <ul class="nav nav-treeview menusamping">
                  <li class="nav-item menusamping">
                    <a href="<?php echo base_url(); ?>admin/pemerintah_desa" class="nav-link">
                      <i class="fa fa-circle-o nav-icon kec-color"></i>
                      <p>Pemerintah Desa</p>
                    </a>
                  </li>
                  <li class="nav-item menusamping">
                    <a href="<?php echo base_url(); ?>admin/profil-sejarah" class="nav-link">
                      <i class="fa fa-circle-o nav-icon kec-color"></i>
                      <p>Sejarah</p>
                    </a>
                  </li>
                  <li class="nav-item menusamping">
                    <a href="<?php echo base_url(); ?>admin/bpd" class="nav-link">
                      <i class="fa fa-circle-o nav-icon kec-color"></i>
                      <p>BPD</p>
                    </a>
                  </li>
                  <li class="nav-item menusamping">
                    <a href="<?php echo base_url(); ?>admin/lpmd" class="nav-link">
                      <i class="fa fa-circle-o nav-icon kec-color"></i>
                      <p>LPMD</p>
                    </a>
                  </li>
                  <li class="nav-item menusamping">
                    <a href="<?php echo base_url(); ?>admin/kartar" class="nav-link">
                      <i class="fa fa-circle-o nav-icon kec-color"></i>
                      <p>Karang Taruna</p>
                    </a>
                  </li>
                  <li class="nav-item menusamping">
                    <a href="<?php echo base_url(); ?>admin/pkkdesa" class="nav-link">
                      <i class="fa fa-circle-o nav-icon kec-color"></i>
                      <p>PKK Desa</p>
                    </a>
                  </li>
                  <li class="nav-item menusamping">
                    <a href="<?php echo base_url(); ?>admin/kaderdesa" class="nav-link">
                      <i class="fa fa-circle-o nav-icon kec-color"></i>
                      <p>Kader Desa</p>
                    </a>
                  </li>
                  <li class="nav-item menusamping">
                    <a href="<?php echo base_url(); ?>admin/rtrw" class="nav-link">
                      <i class="fa fa-circle-o nav-icon kec-color"></i>
                      <p>RT-RW</p>
                    </a>
                  </li>
                </ul>
              </li>
            <?php } ?>

            <?php if($this->session->userdata('role')==1 || $this->session->userdata('role')==2) {?>
              <li class="nav-item menusamping">
                <a href="<?php echo base_url(); ?>admin/berita" class="nav-link">
                  <i class="fa fa-newspaper-o kec-color"></i>
                  <p>
                    Berita
                  </p>
                </a>
              </li>

              <li class="nav-item menusamping">
                <a href="<?php echo base_url(); ?>admin/daftar-komentar" class="nav-link">
                  <i class="fa fa-comments-o kec-color"></i>
                  <p>
                    Komentar
                  </p>
                </a>
              </li>
              
              <li class="nav-item menusamping">
                <a href="<?php echo base_url(); ?>admin/agenda" class="nav-link">
                  <i class="fa fa-pencil-square-o kec-color"></i>
                  <p>
                    Agenda
                  </p>
                </a>
              </li>

              <li class="nav-item menusamping">
                <a href="<?php echo base_url(); ?>admin/pengumuman" class="nav-link">
                  <i class="fa fa-clipboard kec-color"></i>
                  <p>
                    Pengumuman
                  </p>
                </a>
              </li>

              
            <?php } ?>

            <?php if($this->session->userdata('role')==1 || $this->session->userdata('role')==2) {?>

              <li class="nav-item menusamping">
                <a href="<?php echo base_url(); ?>admin/galeri" class="nav-link">
                  <i class="fa fa-file-image-o kec-color"></i>
                  <p>
                    Galeri Kegiatan
                  </p>
                </a>
              </li>


              <li class="nav-item menusamping">
                <a href="<?php echo base_url(); ?>admin/gambar-header" class="nav-link">
                  <i class="fa fa-picture-o kec-color"></i>
                  <p>
                    Gambar Header
                  </p>
                </a>
              </li>


              <li class="nav-item has-treeview menusamping">
                <a href="#" class="nav-link">
                  <i class="fa fa-info kec-color"></i>
                  <p>
                    Informasi
                    <i class="right fa fa-angle-left"></i>
                  </p>
                </a>
                <ul class="nav nav-treeview menusamping">
                  <li class="nav-item menusamping">
                    <a href="<?php echo base_url(); ?>admin/transparansi_desa" class="nav-link">
                      <i class="fa fa-angle-right nav-icon kec-color"></i>
                      <p>Transparansi Keuangan</p>
                    </a>
                  </li>

                  <li class="nav-item menusamping">
                    <a href="<?php echo base_url(); ?>admin/penerima_bantuan" class="nav-link">
                      <i class="fa fa-angle-right nav-icon kec-color"></i>
                      <p>Data Penerima Bantuan</p>
                    </a>
                  </li>

                  <li class="nav-item menusamping">
                    <a href="<?php echo base_url(); ?>admin/prestasi_desa" class="nav-link">
                      <i class="fa fa-angle-right nav-icon kec-color"></i>
                      <p>Prestasi Desa</p>
                    </a>
                  </li>
                </ul>
              </li>

              <li class="nav-item has-treeview menusamping">
                <a href="#" class="nav-link">
                  <i class="fa fa-street-view kec-color"></i>
                  <p>
                    PPID Desa
                    <i class="right fa fa-angle-left"></i>
                  </p>
                </a>
                <ul class="nav nav-treeview menusamping">
                  <li class="nav-item menusamping">
                    <a href="<?php echo base_url(); ?>admin/profilppid" class="nav-link">
                      <i class="fa fa-angle-right nav-icon kec-color"></i>
                      <p>Profil PPID Desa</p>
                    </a>
                  </li>

                  <li class="nav-item menusamping">
                    <a href="<?php echo base_url(); ?>admin/ppid" class="nav-link">
                      <i class="fa fa-angle-right nav-icon kec-color"></i>
                      <p>Informasi Berkala</p>
                    </a>
                  </li>

                  <li class="nav-item menusamping">
                    <a href="<?php echo base_url(); ?>admin/serta-merta" class="nav-link">
                      <i class="fa fa-angle-right nav-icon kec-color"></i>
                      <p>Informasi Serta-Merta</p>
                    </a>
                  </li>

                  <li class="nav-item menusamping">
                    <a href="<?php echo base_url(); ?>admin/dip_online" class="nav-link">
                      <i class="fa fa-angle-right nav-icon kec-color"></i>
                      <p>DIP Online</p>
                    </a>
                  </li>
                    <li class="nav-item menusamping">
                    <a href="<?php echo base_url(); ?>admin/sk_ppid" class="nav-link">
                      <i class="fa fa-angle-right nav-icon kec-color"></i>
                      <p>SK PPID</p>
                    </a>
                  </li>
                </ul>
              </li>

              <li class="nav-item menusamping">
                <a href="<?php echo base_url(); ?>admin/resetPassword" class="nav-link">
                  <i class="fa fa-lock kec-color"></i>
                  <p>
                    Reset Password
                  </p>
                </a>
              </li>

            <?php } ?>

            <?php if($this->session->userdata('role')==3) {?>
              <li class="nav-item menusamping">
                <a href="<?php echo base_url(); ?>admin/importcovid19" class="nav-link">
                  <i class="fa fa-medkit kec-color"></i>
                  <p>
                    Import Data Covid-19
                  </p>
                </a>
              </li>

              <li class="nav-item menusamping">
                <a href="<?php echo base_url(); ?>admin/laporancovid19" class="nav-link">
                  <i class="fa fa-newspaper-o kec-color"></i>
                  <p>
                    Laporan Data Covid-19
                  </p>
                </a>
              </li>

            <?php } ?>

            <?php if($this->session->userdata('role')==1) {?>
              <li class="nav-item menusamping">
                <a href="<?php echo base_url(); ?>admin/pengaturan-user" class="nav-link">
                  <i class="fa fa-user kec-color"></i>
                  <p>
                    Pengaturan User
                  </p>
                </a>
              </li>

            <?php } ?>
          </ul>
        </nav>
        <!-- /.sidebar-menu -->
      </div>
      <!-- /.sidebar -->
    </aside>