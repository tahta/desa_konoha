<?php
  class Model_prestasi_desa extends CI_Model {

    var $table = 'informasi';
    var $col_search = array('judul','isi');

    private function get_query_datatable()
    {
      $this->db->select('informasi.id as idinformasi, informasi.tanggal as tanggal, informasi.judul as judul, informasi.isi as isi, informasi.filepath as filepath, informasi.created_by as created_by, informasi.updated_by as updated_by, informasi.created_at as created_at, informasi.updated_at as updated_at, informasi.isActive as isActive, informasi.status as status ,users.nama as author');
      $this->db->where('informasi.status','Prestasi Desa');
      $this->db->from($this->table);
      $this->db->join('users','informasi.created_by = users.id','INNER');
      $i=0;

      foreach ($this->col_search as $item) {
        if($_POST['search']['value']) // if datatable send POST for search
        {
             
          if($i===0) // first loop
          {
              $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
              $this->db->like($item, $_POST['search']['value']);
          }
          else
          {
              $this->db->or_like($item, $_POST['search']['value']);
          }

          if(count($this->col_search) - 1 == $i) //last loop
              $this->db->group_end(); //close bracket
        }
        $i++;
      }
    }

    function get_datatables()
    {
        $this->get_query_datatable();
        if($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);
        if($this->session->userdata('role') != 1)
          $this->db->where(array(
            //'informasi.isActive'   => 1,
            'informasi.status'     => 'Prestasi Desa',
            'informasi.created_by' => $this->session->userdata('userid')
          ));
        $this->db->order_by("informasi.id", "desc");
        $query = $this->db->get();
        return $query->result();
    }
 
    function count_filtered()
    {
        $this->get_query_datatable();
        if($this->session->userdata('role') != 1)
          $this->db->where(array(
            //'informasi.isActive'   => 1,
            'informasi.status'     => 'Prestasi Desa',
            'informasi.created_by' => $this->session->userdata('userid')
          ));
        $query = $this->db->get();
        return $query->num_rows();
    }
 
    public function count_all()
    {
        $this->db->from($this->table);
        if($this->session->userdata('role') != 1)
          $this->db->where(array(
            //'informasi.isActive'   => 1,
            'informasi.status'     => 'Prestasi Desa',
            'informasi.created_by' => $this->session->userdata('userid')
          ));
        return $this->db->count_all_results();
    }

    public function __construct()
    {
      parent::__construct();
      $this->load->database();
    }

    public function insert_file($tanggal, $judul, $isi, $filepath, $created_by, $updated_by, $created_at, $updated_at, $isActive, $status)
    {
        $data = array(
            'tanggal'      => $tanggal,
            'judul'        => $judul,
            'isi'          => $isi,
            'filepath'     => $filepath,
            'created_by'   => $created_by,
            'updated_by'   => $updated_by,
            'created_at'   => $created_at,
            'updated_at'   => $updated_at,
            'isActive'     => $isActive,
            'status'       => $status

        );
        $this->db->insert('informasi', $data);
        return $this->db->insert_id();
    }

    function update_data($where,$data){
      $this->db->where($where);
      $this->db->update('informasi',$data);
      return true;
    }

    function delete_data($where){
      $this->db->where($where);
      $this->db->delete('informasi');
      return true;
      
    }


  }