<?php
  class Model_komentar extends CI_Model {

 
	function input_data($idberita,$nama,$email,$isi){
		$hasil=$this->db->query("INSERT INTO komentar (id_berita,nama_kom,email_kom,isi_kom,created_at,isActive)VALUES('$idberita','$nama','$email','$isi',now(),0)");
        return $hasil;
	}

	var $table = 'komentar';
    var $col_search = array('isi_kom');

    private function get_query_datatable()
    {
      $this->db->select('komentar.id_kom as id_kom, komentar.nama_kom as nama_kom, komentar.email_kom as email_kom, komentar.isi_kom as isi_kom, komentar.isActive as isActive, berita.judul as judul');
      $this->db->from($this->table);
      $this->db->join('berita','komentar.id_berita = berita.id','INNER');
      $i=0;

      foreach ($this->col_search as $item) {
        if($_POST['search']['value']) // if datatable send POST for search
        {
             
          if($i===0) // first loop
          {
              $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
              $this->db->like($item, $_POST['search']['value']);
          }
          else
          {
              $this->db->or_like($item, $_POST['search']['value']);
          }

          if(count($this->col_search) - 1 == $i) //last loop
              $this->db->group_end(); //close bracket
        }
        $i++;
      }
    }

    function get_datatables()
    {
        $this->get_query_datatable();
        if($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);
        if($this->session->userdata('role') != 1)
        $this->db->order_by("komentar.id_kom", "desc");
        $query = $this->db->get();
        return $query->result();
    }
 
    function count_filtered()
    {
        $this->get_query_datatable();
        if($this->session->userdata('role') != 1)
        $query = $this->db->get();
        return $query->num_rows();
    }
 
    public function count_all()
    {
        $this->db->from($this->table);
        if($this->session->userdata('role') != 1)
        return $this->db->count_all_results();
    }

    function update_data($where,$data){
      $this->db->where($where);
      $this->db->update('komentar',$data);
      return true;
    }

  }