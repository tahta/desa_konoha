<?php
  class Model_ppid extends CI_Model {

    var $table = 'ppid';
    var $col_search = array('judul','isi');

    private function get_query_datatable()
    {
      $this->db->select('ppid.id as idppid, ppid.tanggal as tanggal, ppid.judul as judul, ppid.isi as isi, ppid.filepath as filepath, ppid.created_by as created_by, ppid.updated_by as updated_by, ppid.created_at as created_at, ppid.updated_at as updated_at, ppid.isActive as isActive, kategori_ppid.nama_kategori as nama_kategori,users.nama as author');
      //$this->db->where(array('ppid.created_by' => $this->session->userdata('view_id')));
      $this->db->from($this->table);
      $this->db->join('users','ppid.created_by = users.id','INNER');
      $this->db->join('kategori_ppid','ppid.id_kategori = kategori_ppid.id_kategori','INNER');
      $i=0;

      foreach ($this->col_search as $item) {
        if($_POST['search']['value']) // if datatable send POST for search
        {
             
          if($i===0) // first loop
          {
              $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
              $this->db->like($item, $_POST['search']['value']);
          }
          else
          {
              $this->db->or_like($item, $_POST['search']['value']);
          }

          if(count($this->col_search) - 1 == $i) //last loop
              $this->db->group_end(); //close bracket
        }
        $i++;
      }
    }

    function get_datatables()
    {
        $this->get_query_datatable();
        if($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);
        if($this->session->userdata('role') != 1)
          $this->db->where(array(
            //'ppid.isActive'   => 1,
            'ppid.created_by' => $this->session->userdata('userid')
          ));
        $this->db->order_by("ppid.id", "desc");
        $query = $this->db->get();
        return $query->result();
    }
 
    function count_filtered()
    {
        $this->get_query_datatable();
        if($this->session->userdata('role') != 1)
          $this->db->where(array(
            //'ppid.isActive'   => 1,
            'ppid.created_by' => $this->session->userdata('userid')
          ));
        $query = $this->db->get();
        return $query->num_rows();
    }
 
    public function count_all()
    {
        $this->db->from($this->table);
        if($this->session->userdata('role') != 1)
          $this->db->where(array(
            //'ppid.isActive'   => 1,
            'ppid.created_by' => $this->session->userdata('userid')
          ));
        return $this->db->count_all_results();
    }

    public function __construct()
    {
      parent::__construct();
      $this->load->database();
    }

    public function insert_file($tanggal, $judul, $isi, $filepath, $created_by, $updated_by, $created_at, $updated_at, $isActive, $nama_kategori)
    {
        $data = array(
            'tanggal'      => $tanggal,
            'judul'        => $judul,
            'isi'          => $isi,
            'filepath'     => $filepath,
            'created_by'   => $created_by,
            'updated_by'   => $updated_by,
            'created_at'   => $created_at,
            'updated_at'   => $updated_at,
            'isActive'     => $isActive,
            'id_kategori' => $nama_kategori

        );
        $this->db->insert('ppid', $data);
        return $this->db->insert_id();
    }

    function update_data($where,$data){
      $this->db->where($where);
      $this->db->update('ppid',$data);
      return true;
    }

    function delete_data($where){
      $this->db->where($where);
      $this->db->delete('ppid');
      return true;
    }

    public function getKategori(){
        $query = $this->db->query("SELECT id_kategori, nama_kategori FROM kategori_ppid  ORDER BY id_kategori ASC");
        return $query->result();
    }

  }