<?php
  class Model_agenda extends CI_Model {

    var $table = 'agenda';
    var $col_search = array('judul','tanggal');

    private function get_query_datatable()
    {
      $this->db->select('agenda.id as idagenda, agenda.tanggal as tanggal, agenda.judul as judul, agenda.bagian as bagian, agenda.filepath as filepath, agenda.created_at as created_at, agenda.updated_at as updated_at, agenda.created_by as created_by, agenda.updated_by as updated_by, agenda.isActive as isActive, mst_bagian.namabagian as namabagian');
      $this->db->from($this->table);
      $this->db->join('mst_bagian','agenda.bagian = mst_bagian.id','INNER');
      $i=0;

      foreach ($this->col_search as $item) {
        if($_POST['search']['value']) // if datatable send POST for search
        {
             
          if($i===0) // first loop
          {
              $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
              $this->db->like($item, $_POST['search']['value']);
          }
          else
          {
              $this->db->or_like($item, $_POST['search']['value']);
          }

          if(count($this->col_search) - 1 == $i) //last loop
              $this->db->group_end(); //close bracket
        }
        $i++;
      }
    }

    function get_datatables()
    {
        $this->get_query_datatable();
        if($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);
        if($this->session->userdata('role') != 1)
          $this->db->where(array(
            'agenda.isActive' => 1,
            'agenda.created_by' => $this->session->userdata('userid')
          ));
        $this->db->order_by("agenda.id", "desc");
        $query = $this->db->get();
        return $query->result();
    }
 
    function count_filtered()
    {
        $this->get_query_datatable();
        if($this->session->userdata('role') != 1)
          $this->db->where(array(
            'agenda.isActive' => 1,
            'agenda.created_by' => $this->session->userdata('userid')
          ));
        $query = $this->db->get();
        return $query->num_rows();
    }
 
    public function count_all()
    {
        $this->db->from($this->table);
        if($this->session->userdata('role') != 1)
          $this->db->where(array(
            'agenda.isActive' => 1,
            'agenda.created_by' => $this->session->userdata('userid')
          ));
        return $this->db->count_all_results();
    }

    public function insert_file($data)
    {
        $this->db->insert('agenda', $data);
        return $this->db->insert_id();
    }

    function update_data($where,$data){
      $this->db->where($where);
      $this->db->update('agenda',$data);
      return true;
    } 
  }